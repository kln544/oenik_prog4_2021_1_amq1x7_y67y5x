var namespace_modnar =
[
    [ "GameLogic", "namespace_modnar_1_1_game_logic.html", "namespace_modnar_1_1_game_logic" ],
    [ "GameModel", "namespace_modnar_1_1_game_model.html", "namespace_modnar_1_1_game_model" ],
    [ "GameRenderer", "namespace_modnar_1_1_game_renderer.html", "namespace_modnar_1_1_game_renderer" ],
    [ "Repository", "namespace_modnar_1_1_repository.html", "namespace_modnar_1_1_repository" ],
    [ "WpfApp", "namespace_modnar_1_1_wpf_app.html", "namespace_modnar_1_1_wpf_app" ]
];