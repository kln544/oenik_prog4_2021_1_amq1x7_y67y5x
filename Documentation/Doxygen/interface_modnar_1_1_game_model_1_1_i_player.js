var interface_modnar_1_1_game_model_1_1_i_player =
[
    [ "Armour", "interface_modnar_1_1_game_model_1_1_i_player.html#a30e0e28776aa57f52d4a55f9137e72bc", null ],
    [ "Armoury", "interface_modnar_1_1_game_model_1_1_i_player.html#af6ab5c001b40861a20b800b9f688e033", null ],
    [ "Dex", "interface_modnar_1_1_game_model_1_1_i_player.html#aea6cef618297482469a037993c1ed844", null ],
    [ "Inventory", "interface_modnar_1_1_game_model_1_1_i_player.html#a4b3a2dd01853336c964208e178b4e8b7", null ],
    [ "Mana", "interface_modnar_1_1_game_model_1_1_i_player.html#ac50019f18a2f2b3337158ee0dbf7e05c", null ],
    [ "MaxMana", "interface_modnar_1_1_game_model_1_1_i_player.html#af3755c6b16dcfb94ed61796fdd47d698", null ],
    [ "MaxStamina", "interface_modnar_1_1_game_model_1_1_i_player.html#a7ffe90aae22e8847cbb72f7c384c7881", null ],
    [ "Name", "interface_modnar_1_1_game_model_1_1_i_player.html#a454f7721fee0aaed3b61910c2f8bf85e", null ],
    [ "Stamina", "interface_modnar_1_1_game_model_1_1_i_player.html#a182baee2c867fbf76c55487f2cd29b1e", null ],
    [ "Str", "interface_modnar_1_1_game_model_1_1_i_player.html#aa26356f4c7ee01e6d70d78c573d9c7ba", null ],
    [ "Weapon", "interface_modnar_1_1_game_model_1_1_i_player.html#ae708ef17b74f49ca82f39396f737793c", null ],
    [ "Weaponry", "interface_modnar_1_1_game_model_1_1_i_player.html#aef0c0584f18b004b10156ad200b21407", null ],
    [ "Wis", "interface_modnar_1_1_game_model_1_1_i_player.html#a6878092bd2c481cee9cba9f2a7d3cad9", null ]
];