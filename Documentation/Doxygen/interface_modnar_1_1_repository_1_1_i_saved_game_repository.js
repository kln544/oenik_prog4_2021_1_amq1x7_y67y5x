var interface_modnar_1_1_repository_1_1_i_saved_game_repository =
[
    [ "LoadEntities", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#ab01756d015bd4cb6cf62e045d41ef3f2", null ],
    [ "LoadLoot", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#a03aa5792cbffee73e4a63462b37c4844", null ],
    [ "LoadMap", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#a54843b31fc74c037f8c07e7f982a929d", null ],
    [ "SaveEntities", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#ad5445c7bd1863e6e4a2bc64089395279", null ],
    [ "SaveLoot", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#a46aaa07ca5103c062011725768f584c7", null ],
    [ "SaveMap", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html#ac91fca940a9f878a6608751541b25eab", null ]
];