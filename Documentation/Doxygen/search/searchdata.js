var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwxy",
  1: "abcdefghilmprsw",
  2: "mx",
  3: "abcdefghilmoprstuvw",
  4: "cdmrw",
  5: "abceflnprsuw",
  6: "acdefghilmnprstuvwxy",
  7: "efpsw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Events"
};

