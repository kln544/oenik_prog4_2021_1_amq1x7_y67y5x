var class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model =
[
    [ "CopyFrom", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#ad03d2309f27c70a683ec1854f42437a7", null ],
    [ "Cold", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a6624301c94ce7c09065c5ae5c2d24953", null ],
    [ "Durability", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a6e7a1b71b31c0eddec40271f3b67c964", null ],
    [ "Fire", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a1e31f46d5519d9f7556b51158037eba5", null ],
    [ "Icon", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a73cef49d1f6d297589880a79ee8cd805", null ],
    [ "Lightning", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#ace8509c6da0c62ed833336e7c3cb1435", null ],
    [ "Maxdurability", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#ab875b1df403a00ab673f03235a479243", null ],
    [ "Name", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a417916a8e38d9ec1f8b5b6f2ad1ffe3b", null ],
    [ "Physical", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a6c46ca1939b151ed607a427275c81a18", null ],
    [ "Range", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#aaef67d576aabd0290598bd8ab11f0d0b", null ],
    [ "Rarity", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a6e32c38364018924dcbb59a54d479ee3", null ],
    [ "Tooltip", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a1fff479abe2172003bfdc4e130033b50", null ],
    [ "Type", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html#a09f8d8328a2be784fb2b0e9681b2eb01", null ]
];