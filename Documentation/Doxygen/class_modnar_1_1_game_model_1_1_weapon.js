var class_modnar_1_1_game_model_1_1_weapon =
[
    [ "Weapon", "class_modnar_1_1_game_model_1_1_weapon.html#a118c360bd0dfef46a19954a9566f8d90", null ],
    [ "Weapon", "class_modnar_1_1_game_model_1_1_weapon.html#a2124a18d1d99d3aacae4c5264695d5fa", null ],
    [ "DeepClone", "class_modnar_1_1_game_model_1_1_weapon.html#a1aec21d94e86d376a910f9d5a1c19f37", null ],
    [ "Damage", "class_modnar_1_1_game_model_1_1_weapon.html#ad53327dc479a7663cb86c6b0bd03b79d", null ],
    [ "Durability", "class_modnar_1_1_game_model_1_1_weapon.html#a9c2d8366b75c8a47aaf3666ddd446f82", null ],
    [ "Icon", "class_modnar_1_1_game_model_1_1_weapon.html#a298ea16b49d791ff6ea003776fb874f6", null ],
    [ "Maxdurability", "class_modnar_1_1_game_model_1_1_weapon.html#a6666e8d7aab3d0f7eb81bc591778ceec", null ],
    [ "Name", "class_modnar_1_1_game_model_1_1_weapon.html#acd94d91484084a0499f1cc24169d59c4", null ],
    [ "Range", "class_modnar_1_1_game_model_1_1_weapon.html#ac9230032dd70409d73cb40da2ef713f2", null ],
    [ "Rarity", "class_modnar_1_1_game_model_1_1_weapon.html#a36e5c50f9a2aece397ccfe626914b2dc", null ],
    [ "Tooltip", "class_modnar_1_1_game_model_1_1_weapon.html#a10711d90afe8b10d1e0bf9dd4283e60e", null ],
    [ "Type", "class_modnar_1_1_game_model_1_1_weapon.html#a71f8a7dcae8845dd6e16e7f8d8360425", null ]
];