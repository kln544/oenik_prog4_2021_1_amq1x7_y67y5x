var interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic =
[
    [ "GetHunter", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#a98f15ccabded8ea15868a0a429ddb683", null ],
    [ "GetKnight", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#af23ab09da08d4c8d27111933ae2fe201", null ],
    [ "GetMage", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#acf5cc027989738451d2cbf5639ce58e7", null ],
    [ "LoadSavedGame", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#a8bf0315e051d0497bb52a11e94fb5147", null ],
    [ "StartNewGame", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#aa9c0024cbe4144886af69d2c07393505", null ],
    [ "ViewHighScore", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html#aaa8c30cc910fcfe6af70af1f8dbfc981", null ]
];