var class_modnar_1_1_wpf_app_1_1_data_1_1_character_model =
[
    [ "Armour", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#acd7c78fbe4788bd52e3fefe61f00cc6f", null ],
    [ "Dex", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#ad5ef2be7963a2ba8ef36c354679ecab9", null ],
    [ "Health", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#ab7011a52590459428f469dd18a3cdba1", null ],
    [ "Mana", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#aacde31d55dcaa3ee0df4f80ed20a70d2", null ],
    [ "Name", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#af738f7050a2b45d3a70478c07f0c1b3c", null ],
    [ "Points", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#ab71143ca0dbc75056de7c91abd845fd8", null ],
    [ "Stamina", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#ab6c9e51c781b8d3b9d295f3d10ac400d", null ],
    [ "Str", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#a4195c730bbddc2fa52f7afdbe40fab0d", null ],
    [ "Weaponr", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#a878a201c947311a40794226fca0534ba", null ],
    [ "Wis", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html#a02cd3b8820581646bfd5efdc6531f249", null ]
];