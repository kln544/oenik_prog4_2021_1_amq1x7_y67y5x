var annotated_dup =
[
    [ "Modnar", "namespace_modnar.html", [
      [ "GameLogic", "namespace_modnar_1_1_game_logic.html", [
        [ "Tests", "namespace_modnar_1_1_game_logic_1_1_tests.html", [
          [ "EntityActionTest", "class_modnar_1_1_game_logic_1_1_tests_1_1_entity_action_test.html", "class_modnar_1_1_game_logic_1_1_tests_1_1_entity_action_test" ]
        ] ],
        [ "DungeonLogic", "class_modnar_1_1_game_logic_1_1_dungeon_logic.html", "class_modnar_1_1_game_logic_1_1_dungeon_logic" ],
        [ "GameMaster", "class_modnar_1_1_game_logic_1_1_game_master.html", "class_modnar_1_1_game_logic_1_1_game_master" ],
        [ "IDungeonLogic", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic.html", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic" ],
        [ "IDungeonLogicEntityActions", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic_entity_actions.html", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic_entity_actions" ],
        [ "IGameMaster", "interface_modnar_1_1_game_logic_1_1_i_game_master.html", "interface_modnar_1_1_game_logic_1_1_i_game_master" ],
        [ "EnemyLogic", "class_modnar_1_1_game_logic_1_1_enemy_logic.html", "class_modnar_1_1_game_logic_1_1_enemy_logic" ],
        [ "IAiLogic", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html", "interface_modnar_1_1_game_logic_1_1_i_ai_logic" ],
        [ "IEntityLogic", "interface_modnar_1_1_game_logic_1_1_i_entity_logic.html", "interface_modnar_1_1_game_logic_1_1_i_entity_logic" ],
        [ "IPlayerLogic", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html", "interface_modnar_1_1_game_logic_1_1_i_player_logic" ],
        [ "ISkillLogic", "interface_modnar_1_1_game_logic_1_1_i_skill_logic.html", "interface_modnar_1_1_game_logic_1_1_i_skill_logic" ],
        [ "PlayerLogic", "class_modnar_1_1_game_logic_1_1_player_logic.html", "class_modnar_1_1_game_logic_1_1_player_logic" ],
        [ "SkillLogic", "class_modnar_1_1_game_logic_1_1_skill_logic.html", "class_modnar_1_1_game_logic_1_1_skill_logic" ],
        [ "IMapGenLogic", "interface_modnar_1_1_game_logic_1_1_i_map_gen_logic.html", "interface_modnar_1_1_game_logic_1_1_i_map_gen_logic" ],
        [ "MapGenLogic", "class_modnar_1_1_game_logic_1_1_map_gen_logic.html", "class_modnar_1_1_game_logic_1_1_map_gen_logic" ],
        [ "CordPair", "class_modnar_1_1_game_logic_1_1_cord_pair.html", "class_modnar_1_1_game_logic_1_1_cord_pair" ],
        [ "DisplaySupport", "class_modnar_1_1_game_logic_1_1_display_support.html", "class_modnar_1_1_game_logic_1_1_display_support" ],
        [ "SoundEffectEventArgs", "class_modnar_1_1_game_logic_1_1_sound_effect_event_args.html", "class_modnar_1_1_game_logic_1_1_sound_effect_event_args" ]
      ] ],
      [ "GameModel", "namespace_modnar_1_1_game_model.html", [
        [ "Damage", "class_modnar_1_1_game_model_1_1_damage.html", "class_modnar_1_1_game_model_1_1_damage" ],
        [ "IDamage", "interface_modnar_1_1_game_model_1_1_i_damage.html", "interface_modnar_1_1_game_model_1_1_i_damage" ],
        [ "ILeaderboard", "interface_modnar_1_1_game_model_1_1_i_leaderboard.html", "interface_modnar_1_1_game_model_1_1_i_leaderboard" ],
        [ "Armour", "class_modnar_1_1_game_model_1_1_armour.html", "class_modnar_1_1_game_model_1_1_armour" ],
        [ "Consumable", "class_modnar_1_1_game_model_1_1_consumable.html", "class_modnar_1_1_game_model_1_1_consumable" ],
        [ "IArmour", "interface_modnar_1_1_game_model_1_1_i_armour.html", "interface_modnar_1_1_game_model_1_1_i_armour" ],
        [ "IConsumable", "interface_modnar_1_1_game_model_1_1_i_consumable.html", "interface_modnar_1_1_game_model_1_1_i_consumable" ],
        [ "IItem", "interface_modnar_1_1_game_model_1_1_i_item.html", "interface_modnar_1_1_game_model_1_1_i_item" ],
        [ "ISkill", "interface_modnar_1_1_game_model_1_1_i_skill.html", "interface_modnar_1_1_game_model_1_1_i_skill" ],
        [ "IWeapon", "interface_modnar_1_1_game_model_1_1_i_weapon.html", "interface_modnar_1_1_game_model_1_1_i_weapon" ],
        [ "Skill", "class_modnar_1_1_game_model_1_1_skill.html", "class_modnar_1_1_game_model_1_1_skill" ],
        [ "Weapon", "class_modnar_1_1_game_model_1_1_weapon.html", "class_modnar_1_1_game_model_1_1_weapon" ],
        [ "Leaderboard", "class_modnar_1_1_game_model_1_1_leaderboard.html", "class_modnar_1_1_game_model_1_1_leaderboard" ],
        [ "IRoomModel", "interface_modnar_1_1_game_model_1_1_i_room_model.html", "interface_modnar_1_1_game_model_1_1_i_room_model" ],
        [ "IRoomNode", "interface_modnar_1_1_game_model_1_1_i_room_node.html", "interface_modnar_1_1_game_model_1_1_i_room_node" ],
        [ "RoomModel", "class_modnar_1_1_game_model_1_1_room_model.html", "class_modnar_1_1_game_model_1_1_room_model" ],
        [ "RoomNode", "class_modnar_1_1_game_model_1_1_room_node.html", "class_modnar_1_1_game_model_1_1_room_node" ],
        [ "Cell", "class_modnar_1_1_game_model_1_1_cell.html", "class_modnar_1_1_game_model_1_1_cell" ],
        [ "Enemy", "class_modnar_1_1_game_model_1_1_enemy.html", "class_modnar_1_1_game_model_1_1_enemy" ],
        [ "ICell", "interface_modnar_1_1_game_model_1_1_i_cell.html", "interface_modnar_1_1_game_model_1_1_i_cell" ],
        [ "IEnemy", "interface_modnar_1_1_game_model_1_1_i_enemy.html", "interface_modnar_1_1_game_model_1_1_i_enemy" ],
        [ "IEntity", "interface_modnar_1_1_game_model_1_1_i_entity.html", "interface_modnar_1_1_game_model_1_1_i_entity" ],
        [ "ILoot", "interface_modnar_1_1_game_model_1_1_i_loot.html", "interface_modnar_1_1_game_model_1_1_i_loot" ],
        [ "IObject", "interface_modnar_1_1_game_model_1_1_i_object.html", "interface_modnar_1_1_game_model_1_1_i_object" ],
        [ "IPlayer", "interface_modnar_1_1_game_model_1_1_i_player.html", "interface_modnar_1_1_game_model_1_1_i_player" ],
        [ "Loot", "class_modnar_1_1_game_model_1_1_loot.html", "class_modnar_1_1_game_model_1_1_loot" ],
        [ "Player", "class_modnar_1_1_game_model_1_1_player.html", "class_modnar_1_1_game_model_1_1_player" ]
      ] ],
      [ "GameRenderer", "namespace_modnar_1_1_game_renderer.html", [
        [ "BrushHelper", "class_modnar_1_1_game_renderer_1_1_brush_helper.html", "class_modnar_1_1_game_renderer_1_1_brush_helper" ],
        [ "FullMapRenderer", "class_modnar_1_1_game_renderer_1_1_full_map_renderer.html", "class_modnar_1_1_game_renderer_1_1_full_map_renderer" ],
        [ "MiniMapRenderer", "class_modnar_1_1_game_renderer_1_1_mini_map_renderer.html", "class_modnar_1_1_game_renderer_1_1_mini_map_renderer" ],
        [ "Renderer", "class_modnar_1_1_game_renderer_1_1_renderer.html", "class_modnar_1_1_game_renderer_1_1_renderer" ]
      ] ],
      [ "Repository", "namespace_modnar_1_1_repository.html", [
        [ "HighScoreRepository", "class_modnar_1_1_repository_1_1_high_score_repository.html", "class_modnar_1_1_repository_1_1_high_score_repository" ],
        [ "ResourceRepository", "class_modnar_1_1_repository_1_1_resource_repository.html", "class_modnar_1_1_repository_1_1_resource_repository" ],
        [ "SavedGameRepository", "class_modnar_1_1_repository_1_1_saved_game_repository.html", "class_modnar_1_1_repository_1_1_saved_game_repository" ],
        [ "IHighScoreRepository", "interface_modnar_1_1_repository_1_1_i_high_score_repository.html", "interface_modnar_1_1_repository_1_1_i_high_score_repository" ],
        [ "IResourceRepository", "interface_modnar_1_1_repository_1_1_i_resource_repository.html", "interface_modnar_1_1_repository_1_1_i_resource_repository" ],
        [ "ISavedGameRepository", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html", "interface_modnar_1_1_repository_1_1_i_saved_game_repository" ]
      ] ],
      [ "WpfApp", "namespace_modnar_1_1_wpf_app.html", [
        [ "BL", "namespace_modnar_1_1_wpf_app_1_1_b_l.html", [
          [ "GameBusinessLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_game_business_logic.html", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_game_business_logic" ],
          [ "IDisplayCharacterCreatorService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_character_creator_service.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_character_creator_service" ],
          [ "IDisplayGameService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_game_service.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_game_service" ],
          [ "IDisplayLeadeboardService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_leadeboard_service.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_leadeboard_service" ],
          [ "IDisplayMenuService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_menu_service.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_menu_service" ],
          [ "IGameBusinessLogic", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_game_business_logic.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_game_business_logic" ],
          [ "IMainMenuLogic", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic" ],
          [ "MainMenuLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic" ]
        ] ],
        [ "Data", "namespace_modnar_1_1_wpf_app_1_1_data.html", [
          [ "ArmourModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model" ],
          [ "CharacterModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model" ],
          [ "ConsumableModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model" ],
          [ "ScoreModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_score_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_score_model" ],
          [ "SkillModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_skill_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_skill_model" ],
          [ "WeaponModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model" ]
        ] ],
        [ "UI", "namespace_modnar_1_1_wpf_app_1_1_u_i.html", [
          [ "FullMapControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_control.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_control" ],
          [ "GameControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_control.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_control" ],
          [ "MiniMapControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_mini_map_control.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_mini_map_control" ],
          [ "FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window" ],
          [ "MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window" ],
          [ "GameWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_window.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_window" ],
          [ "DisplayCharacterCreatorService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_character_creator_service.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_character_creator_service" ],
          [ "DisplayGameService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_game_service.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_game_service" ],
          [ "DisplayLeadeboardService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_leadeboard_service.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_leadeboard_service" ],
          [ "DisplayMenuService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_menu_service.html", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_menu_service" ]
        ] ],
        [ "VM", "namespace_modnar_1_1_wpf_app_1_1_v_m.html", [
          [ "CharacterCreatorViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_character_creator_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_character_creator_view_model" ],
          [ "GameViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_game_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_game_view_model" ],
          [ "HighScoreViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_high_score_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_high_score_view_model" ],
          [ "MainViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_main_view_model" ],
          [ "MapViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_map_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_map_view_model" ],
          [ "MenuViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_menu_view_model.html", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_menu_view_model" ]
        ] ],
        [ "App", "class_modnar_1_1_wpf_app_1_1_app.html", "class_modnar_1_1_wpf_app_1_1_app" ],
        [ "CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", "class_modnar_1_1_wpf_app_1_1_character_creator_window" ],
        [ "GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", "class_modnar_1_1_wpf_app_1_1_game_window" ],
        [ "HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", "class_modnar_1_1_wpf_app_1_1_high_score_window" ],
        [ "MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", "class_modnar_1_1_wpf_app_1_1_main_window" ],
        [ "MyIOC", "class_modnar_1_1_wpf_app_1_1_my_i_o_c.html", "class_modnar_1_1_wpf_app_1_1_my_i_o_c" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];