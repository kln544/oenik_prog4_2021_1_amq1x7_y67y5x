var class_modnar_1_1_game_model_1_1_consumable =
[
    [ "Consumable", "class_modnar_1_1_game_model_1_1_consumable.html#a6ac769bd68b27ae47d43c595ef8bed2c", null ],
    [ "DeepClone", "class_modnar_1_1_game_model_1_1_consumable.html#a50f51bc6b0b3d9c7233bd08e6d4e1c2d", null ],
    [ "Health", "class_modnar_1_1_game_model_1_1_consumable.html#ae9f8c4ef40c376cd2a4336888abc1a9f", null ],
    [ "Icon", "class_modnar_1_1_game_model_1_1_consumable.html#a49f6f81a3e9da742667ae9aaeced6be2", null ],
    [ "Mana", "class_modnar_1_1_game_model_1_1_consumable.html#a8fc3670f6a9f781a04be8ae22540b169", null ],
    [ "Name", "class_modnar_1_1_game_model_1_1_consumable.html#ad91290df26ec957f09bdd21cc2a967b9", null ],
    [ "Rarity", "class_modnar_1_1_game_model_1_1_consumable.html#ab6c3350c181791eac86c44893fc1f880", null ],
    [ "Stack", "class_modnar_1_1_game_model_1_1_consumable.html#acd34c627913ba7462d1af42dbd465deb", null ],
    [ "Stamina", "class_modnar_1_1_game_model_1_1_consumable.html#ad67e99db20627e9ec24abbc4e127f5da", null ],
    [ "Tooltip", "class_modnar_1_1_game_model_1_1_consumable.html#a825b0ae6f86458eacfdfaf6cee8c6d4e", null ]
];