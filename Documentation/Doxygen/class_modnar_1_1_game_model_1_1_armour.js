var class_modnar_1_1_game_model_1_1_armour =
[
    [ "Armour", "class_modnar_1_1_game_model_1_1_armour.html#ae9bf27535831fe109143bc7b7c720f88", null ],
    [ "Armour", "class_modnar_1_1_game_model_1_1_armour.html#a4395e6198fb655ba6d3b4b2763c07e30", null ],
    [ "DeepClone", "class_modnar_1_1_game_model_1_1_armour.html#a65e177402710069d73766a3e31848e9c", null ],
    [ "Defense", "class_modnar_1_1_game_model_1_1_armour.html#a0ff119388705883893994120c4ad6511", null ],
    [ "Durability", "class_modnar_1_1_game_model_1_1_armour.html#ab7783d98261ebc3308b1eac6091012eb", null ],
    [ "Icon", "class_modnar_1_1_game_model_1_1_armour.html#a8b14c9f714d2dc1fba675ab48f4bd9e3", null ],
    [ "Maxdurability", "class_modnar_1_1_game_model_1_1_armour.html#ac3adbe5381e66fc9c6e0643df08847d9", null ],
    [ "Name", "class_modnar_1_1_game_model_1_1_armour.html#aa3b02f625917c771a316ad41418676cb", null ],
    [ "Rarity", "class_modnar_1_1_game_model_1_1_armour.html#a925e2595b57fc4e7d422019687d96ac7", null ],
    [ "Tooltip", "class_modnar_1_1_game_model_1_1_armour.html#a96fc025c53c38bcd2e4f36cb822b242e", null ]
];