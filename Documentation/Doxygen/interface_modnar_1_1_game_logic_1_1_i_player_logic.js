var interface_modnar_1_1_game_logic_1_1_i_player_logic =
[
    [ "ChangeArmour", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#ab13b709a9c940ca6e84e3830ad9e2b30", null ],
    [ "ChangeWeapon", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#a6590f293b9f44b4b1251235414d9b1c5", null ],
    [ "Face", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#a81e53a35bac9c33b61977d27295f8a7d", null ],
    [ "Interact", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#ab605b7afe38b699443937c1575183636", null ],
    [ "Move", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#ae579976c7f14e92768bcb8866ead7fab", null ],
    [ "TakeDamage", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#ac0e171feedff9c569329b11376e41c54", null ],
    [ "UseConsumable", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#a1c795942ed7bbbc30490dc7216509286", null ],
    [ "WeaponChanged", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html#a62aa346d424e480ef1debd0923b10933", null ]
];