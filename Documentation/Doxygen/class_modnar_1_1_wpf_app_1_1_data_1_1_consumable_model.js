var class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model =
[
    [ "Health", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#ac5ae7884b858dec4e7172e94316628bf", null ],
    [ "Icon", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#a5c98a9f36916f4487f8eb71d91406ec4", null ],
    [ "Mana", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#a8e4e1caecb2b8d379b61e54153561301", null ],
    [ "Name", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#a1a1cdaba299fb0472ae71c3d991550bf", null ],
    [ "Rarity", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#ab40dc5c7f1786a688c7fc801e36eff8c", null ],
    [ "Stack", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#ad077680a07ae7fd1ecf0b0471871175f", null ],
    [ "Stamina", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#acb5e26290637e5a673abe5eb9ad8793c", null ],
    [ "Tooltip", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html#a26b78f3f39afe21a0a7f283b91531978", null ]
];