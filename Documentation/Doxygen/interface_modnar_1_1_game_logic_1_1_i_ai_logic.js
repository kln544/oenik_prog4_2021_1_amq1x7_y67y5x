var interface_modnar_1_1_game_logic_1_1_i_ai_logic =
[
    [ "DoCurrentTurn", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#ae8fb8dec23d218aaaccd99a48ff44dc0", null ],
    [ "FaceTarget", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a35f76bf2aec50319201f4ed989ad63fe", null ],
    [ "FindTarget", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a4d837dd5c000f7b520c9fc6d872dd627", null ],
    [ "IdleAction", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a4efc807fbd7b8ecc3967b19d27e05d3f", null ],
    [ "Move", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a07d76b53ea29bf778d3f743a8f752515", null ],
    [ "PathOrder", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a665c70f16a209752c8ec6c83009f0cbb", null ],
    [ "PathToTarget", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a239fa78e0a118043095cd01431c6eb06", null ],
    [ "TakeDamage", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a2d11a31c81160f537067a1e442bf6b56", null ],
    [ "Moves", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a00d6b6b7fd04609f50e94280795f9167", null ],
    [ "Targetx", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#af97faf519f7be9a5291fa809dca99e22", null ],
    [ "Targety", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html#a3fa185ffb7f19ea828c5962c002e6ec6", null ]
];