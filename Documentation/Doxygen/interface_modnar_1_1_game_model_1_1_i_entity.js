var interface_modnar_1_1_game_model_1_1_i_entity =
[
    [ "CanFly", "interface_modnar_1_1_game_model_1_1_i_entity.html#a090ed592285553277c6645febb0bb318", null ],
    [ "Facing", "interface_modnar_1_1_game_model_1_1_i_entity.html#a42540d318a131900045ef9a22b60a00f", null ],
    [ "Health", "interface_modnar_1_1_game_model_1_1_i_entity.html#a73ab28dc4bf411b9e5d43a835863f72e", null ],
    [ "MaxHealth", "interface_modnar_1_1_game_model_1_1_i_entity.html#aed48255ab2b6f95aad229ee92244920d", null ],
    [ "Resistance", "interface_modnar_1_1_game_model_1_1_i_entity.html#af14c8563b51c5ff7df77d4ed7ed68106", null ],
    [ "Sight", "interface_modnar_1_1_game_model_1_1_i_entity.html#a37676639f0eecf5b38b67f9f4062741d", null ]
];