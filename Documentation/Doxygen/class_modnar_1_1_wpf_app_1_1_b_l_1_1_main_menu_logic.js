var class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic =
[
    [ "MainMenuLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#ac6494fe030add1a882f62b3145829d35", null ],
    [ "MainMenuLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#a28dce9d43e2ac0459fb5572a38c85c73", null ],
    [ "GetHunter", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#a16b90f482ac27db6c796e34c8631cb5b", null ],
    [ "GetKnight", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#ae49f154e79273a395b91670d90002e0b", null ],
    [ "GetMage", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#ac7e4b5749abbc451888720cec26e1f83", null ],
    [ "LoadSavedGame", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#a17fc789b7c6dda03cd8ccf7c28139b60", null ],
    [ "StartNewGame", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#a7c55b0ebe7fa810b47e58e02b40d5c9d", null ],
    [ "ViewHighScore", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html#acce13ebafc8ddfee67f440f58d5bc050", null ]
];