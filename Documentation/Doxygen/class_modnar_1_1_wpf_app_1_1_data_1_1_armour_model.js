var class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model =
[
    [ "CopyFrom", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#a0bf3223a9c0cdd000c0b7a83bdce2186", null ],
    [ "Cold", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aa56b1f640eee6b2c4e80357dfe88b9e6", null ],
    [ "Durability", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#ad68804fcecc870053bcafcc976065f2f", null ],
    [ "Fire", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aa68d1ba9d40508d5d72fc69a10ff8d0a", null ],
    [ "Icon", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aef1b36e90d5ec34eef923885309a2675", null ],
    [ "Lightning", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aef77c83515451ddd1428a2c48026c8b8", null ],
    [ "Maxdurability", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aa8503286a480bb74f5beca38a54c85ad", null ],
    [ "Name", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#aa5181a4ae66a2cfc39e1d65cc55ec828", null ],
    [ "Physical", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#a9e7f0b52f4bd982cab115e93abba4fb7", null ],
    [ "Rarity", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#a1cd754277b73519b4771526994fca711", null ],
    [ "Tooltip", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html#a481d72a06240acae8e18bd88d62cee5e", null ]
];