var hierarchy =
[
    [ "Application", null, [
      [ "Modnar.WpfApp.App", "class_modnar_1_1_wpf_app_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "Modnar.WpfApp.App", "class_modnar_1_1_wpf_app_1_1_app.html", null ],
      [ "Modnar.WpfApp.App", "class_modnar_1_1_wpf_app_1_1_app.html", null ]
    ] ],
    [ "Modnar.GameRenderer.BrushHelper", "class_modnar_1_1_game_renderer_1_1_brush_helper.html", null ],
    [ "Modnar.GameLogic.CordPair", "class_modnar_1_1_game_logic_1_1_cord_pair.html", null ],
    [ "Modnar.GameLogic.DisplaySupport", "class_modnar_1_1_game_logic_1_1_display_support.html", null ],
    [ "Modnar.GameLogic.Tests.EntityActionTest", "class_modnar_1_1_game_logic_1_1_tests_1_1_entity_action_test.html", null ],
    [ "EventArgs", null, [
      [ "Modnar.GameLogic.SoundEffectEventArgs", "class_modnar_1_1_game_logic_1_1_sound_effect_event_args.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Modnar.WpfApp.UI.FullMapControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_control.html", null ],
      [ "Modnar.WpfApp.UI.GameControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_control.html", null ],
      [ "Modnar.WpfApp.UI.MiniMapControl", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_mini_map_control.html", null ]
    ] ],
    [ "Modnar.GameRenderer.FullMapRenderer", "class_modnar_1_1_game_renderer_1_1_full_map_renderer.html", null ],
    [ "Modnar.GameLogic.IAiLogic", "interface_modnar_1_1_game_logic_1_1_i_ai_logic.html", [
      [ "Modnar.GameLogic.EnemyLogic", "class_modnar_1_1_game_logic_1_1_enemy_logic.html", null ]
    ] ],
    [ "Modnar.GameModel.ICell", "interface_modnar_1_1_game_model_1_1_i_cell.html", [
      [ "Modnar.GameModel.Cell", "class_modnar_1_1_game_model_1_1_cell.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "Modnar.WpfApp.CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", null ],
      [ "Modnar.WpfApp.CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", null ],
      [ "Modnar.WpfApp.GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", null ],
      [ "Modnar.WpfApp.HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", null ],
      [ "Modnar.WpfApp.MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", null ],
      [ "Modnar.WpfApp.MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", null ],
      [ "Modnar.WpfApp.UI.FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", null ],
      [ "Modnar.WpfApp.UI.FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", null ],
      [ "Modnar.WpfApp.UI.GameWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ]
    ] ],
    [ "Modnar.GameModel.IDamage", "interface_modnar_1_1_game_model_1_1_i_damage.html", [
      [ "Modnar.GameModel.Damage", "class_modnar_1_1_game_model_1_1_damage.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IDisplayCharacterCreatorService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_character_creator_service.html", [
      [ "Modnar.WpfApp.UI.DisplayCharacterCreatorService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_character_creator_service.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IDisplayGameService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_game_service.html", [
      [ "Modnar.WpfApp.UI.DisplayGameService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_game_service.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IDisplayLeadeboardService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_leadeboard_service.html", [
      [ "Modnar.WpfApp.UI.DisplayLeadeboardService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_leadeboard_service.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IDisplayMenuService", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_display_menu_service.html", [
      [ "Modnar.WpfApp.UI.DisplayMenuService", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_display_menu_service.html", null ]
    ] ],
    [ "Modnar.GameLogic.IDungeonLogicEntityActions", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic_entity_actions.html", [
      [ "Modnar.GameLogic.IDungeonLogic", "interface_modnar_1_1_game_logic_1_1_i_dungeon_logic.html", [
        [ "Modnar.GameLogic.DungeonLogic", "class_modnar_1_1_game_logic_1_1_dungeon_logic.html", null ]
      ] ]
    ] ],
    [ "Modnar.GameLogic.IEntityLogic", "interface_modnar_1_1_game_logic_1_1_i_entity_logic.html", [
      [ "Modnar.GameLogic.EnemyLogic", "class_modnar_1_1_game_logic_1_1_enemy_logic.html", null ],
      [ "Modnar.GameLogic.PlayerLogic", "class_modnar_1_1_game_logic_1_1_player_logic.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IGameBusinessLogic", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_game_business_logic.html", [
      [ "Modnar.WpfApp.BL.GameBusinessLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_game_business_logic.html", null ]
    ] ],
    [ "Modnar.GameLogic.IGameMaster", "interface_modnar_1_1_game_logic_1_1_i_game_master.html", [
      [ "Modnar.GameLogic.GameMaster", "class_modnar_1_1_game_logic_1_1_game_master.html", null ]
    ] ],
    [ "Modnar.Repository.IHighScoreRepository", "interface_modnar_1_1_repository_1_1_i_high_score_repository.html", [
      [ "Modnar.Repository.HighScoreRepository", "class_modnar_1_1_repository_1_1_high_score_repository.html", null ]
    ] ],
    [ "Modnar.GameModel.IItem", "interface_modnar_1_1_game_model_1_1_i_item.html", [
      [ "Modnar.GameModel.IArmour", "interface_modnar_1_1_game_model_1_1_i_armour.html", [
        [ "Modnar.GameModel.Armour", "class_modnar_1_1_game_model_1_1_armour.html", null ]
      ] ],
      [ "Modnar.GameModel.IConsumable", "interface_modnar_1_1_game_model_1_1_i_consumable.html", [
        [ "Modnar.GameModel.Consumable", "class_modnar_1_1_game_model_1_1_consumable.html", null ]
      ] ],
      [ "Modnar.GameModel.IWeapon", "interface_modnar_1_1_game_model_1_1_i_weapon.html", [
        [ "Modnar.GameModel.Weapon", "class_modnar_1_1_game_model_1_1_weapon.html", null ]
      ] ]
    ] ],
    [ "Modnar.GameModel.ILeaderboard", "interface_modnar_1_1_game_model_1_1_i_leaderboard.html", [
      [ "Modnar.GameModel.Leaderboard", "class_modnar_1_1_game_model_1_1_leaderboard.html", null ]
    ] ],
    [ "Modnar.WpfApp.BL.IMainMenuLogic", "interface_modnar_1_1_wpf_app_1_1_b_l_1_1_i_main_menu_logic.html", [
      [ "Modnar.WpfApp.BL.MainMenuLogic", "class_modnar_1_1_wpf_app_1_1_b_l_1_1_main_menu_logic.html", null ]
    ] ],
    [ "Modnar.GameLogic.IMapGenLogic", "interface_modnar_1_1_game_logic_1_1_i_map_gen_logic.html", [
      [ "Modnar.GameLogic.MapGenLogic", "class_modnar_1_1_game_logic_1_1_map_gen_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Modnar.GameModel.IObject", "interface_modnar_1_1_game_model_1_1_i_object.html", [
      [ "Modnar.GameModel.IEntity", "interface_modnar_1_1_game_model_1_1_i_entity.html", [
        [ "Modnar.GameModel.IEnemy", "interface_modnar_1_1_game_model_1_1_i_enemy.html", [
          [ "Modnar.GameModel.Enemy", "class_modnar_1_1_game_model_1_1_enemy.html", null ]
        ] ],
        [ "Modnar.GameModel.IPlayer", "interface_modnar_1_1_game_model_1_1_i_player.html", [
          [ "Modnar.GameModel.Player", "class_modnar_1_1_game_model_1_1_player.html", null ]
        ] ]
      ] ],
      [ "Modnar.GameModel.ILoot", "interface_modnar_1_1_game_model_1_1_i_loot.html", [
        [ "Modnar.GameModel.Loot", "class_modnar_1_1_game_model_1_1_loot.html", null ]
      ] ]
    ] ],
    [ "Modnar.GameLogic.IPlayerLogic", "interface_modnar_1_1_game_logic_1_1_i_player_logic.html", [
      [ "Modnar.GameLogic.PlayerLogic", "class_modnar_1_1_game_logic_1_1_player_logic.html", null ]
    ] ],
    [ "Modnar.Repository.IResourceRepository", "interface_modnar_1_1_repository_1_1_i_resource_repository.html", [
      [ "Modnar.Repository.ResourceRepository", "class_modnar_1_1_repository_1_1_resource_repository.html", null ]
    ] ],
    [ "Modnar.GameModel.IRoomModel", "interface_modnar_1_1_game_model_1_1_i_room_model.html", [
      [ "Modnar.GameModel.RoomModel", "class_modnar_1_1_game_model_1_1_room_model.html", null ]
    ] ],
    [ "Modnar.GameModel.IRoomNode", "interface_modnar_1_1_game_model_1_1_i_room_node.html", [
      [ "Modnar.GameModel.RoomNode", "class_modnar_1_1_game_model_1_1_room_node.html", null ]
    ] ],
    [ "Modnar.Repository.ISavedGameRepository", "interface_modnar_1_1_repository_1_1_i_saved_game_repository.html", [
      [ "Modnar.Repository.SavedGameRepository", "class_modnar_1_1_repository_1_1_saved_game_repository.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "Modnar.WpfApp.MyIOC", "class_modnar_1_1_wpf_app_1_1_my_i_o_c.html", null ]
    ] ],
    [ "Modnar.GameModel.ISkill", "interface_modnar_1_1_game_model_1_1_i_skill.html", [
      [ "Modnar.GameModel.Skill", "class_modnar_1_1_game_model_1_1_skill.html", null ]
    ] ],
    [ "Modnar.GameLogic.ISkillLogic", "interface_modnar_1_1_game_logic_1_1_i_skill_logic.html", [
      [ "Modnar.GameLogic.SkillLogic", "class_modnar_1_1_game_logic_1_1_skill_logic.html", null ]
    ] ],
    [ "Modnar.GameRenderer.MiniMapRenderer", "class_modnar_1_1_game_renderer_1_1_mini_map_renderer.html", null ],
    [ "ObservableObject", null, [
      [ "Modnar.WpfApp.Data.ArmourModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_armour_model.html", null ],
      [ "Modnar.WpfApp.Data.CharacterModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_character_model.html", null ],
      [ "Modnar.WpfApp.Data.ConsumableModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_consumable_model.html", null ],
      [ "Modnar.WpfApp.Data.SkillModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_skill_model.html", null ],
      [ "Modnar.WpfApp.Data.WeaponModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_weapon_model.html", null ]
    ] ],
    [ "Modnar.GameRenderer.Renderer", "class_modnar_1_1_game_renderer_1_1_renderer.html", null ],
    [ "Modnar.WpfApp.Data.ScoreModel", "class_modnar_1_1_wpf_app_1_1_data_1_1_score_model.html", null ],
    [ "SimpleIoc", null, [
      [ "Modnar.WpfApp.MyIOC", "class_modnar_1_1_wpf_app_1_1_my_i_o_c.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "Modnar.WpfApp.VM.CharacterCreatorViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_character_creator_view_model.html", null ],
      [ "Modnar.WpfApp.VM.GameViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_game_view_model.html", null ],
      [ "Modnar.WpfApp.VM.HighScoreViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_high_score_view_model.html", null ],
      [ "Modnar.WpfApp.VM.MainViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_main_view_model.html", null ],
      [ "Modnar.WpfApp.VM.MapViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_map_view_model.html", null ],
      [ "Modnar.WpfApp.VM.MenuViewModel", "class_modnar_1_1_wpf_app_1_1_v_m_1_1_menu_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "Modnar.WpfApp.CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", null ],
      [ "Modnar.WpfApp.CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", null ],
      [ "Modnar.WpfApp.GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.GameWindow", "class_modnar_1_1_wpf_app_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", null ],
      [ "Modnar.WpfApp.HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", null ],
      [ "Modnar.WpfApp.HighScoreWindow", "class_modnar_1_1_wpf_app_1_1_high_score_window.html", null ],
      [ "Modnar.WpfApp.MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", null ],
      [ "Modnar.WpfApp.MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", null ],
      [ "Modnar.WpfApp.MainWindow", "class_modnar_1_1_wpf_app_1_1_main_window.html", null ],
      [ "Modnar.WpfApp.UI.FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", null ],
      [ "Modnar.WpfApp.UI.FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", null ],
      [ "Modnar.WpfApp.UI.FullMapWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_full_map_window.html", null ],
      [ "Modnar.WpfApp.UI.GameWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_game_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ],
      [ "Modnar.WpfApp.UI.MenuWindow", "class_modnar_1_1_wpf_app_1_1_u_i_1_1_menu_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Modnar.WpfApp.CharacterCreatorWindow", "class_modnar_1_1_wpf_app_1_1_character_creator_window.html", null ]
    ] ]
];