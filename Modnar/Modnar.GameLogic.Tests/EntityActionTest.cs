﻿// <copyright file="EntityActionTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using NUnit.Framework;

    /// <summary>
    /// Test for player actions.
    /// </summary>
    [TestFixture]
    internal static class EntityActionTest
    {
        private static DungeonLogic dl;
        private static Player player;
        private static Enemy enemy;
        private static ICell[][] map;
        private static IList<IEntity> entities;
        private static IList<Loot> maploot;

        /// <summary>
        /// Setup for the test.
        /// </summary>
        [SetUp]
        public static void Setup()
        {
            player = new Player(100, 100, 100, 2, 2);

            enemy = new Enemy("test", 100, new List<Damage> { new Damage(Damagetype.Physical, 10) }, 3, 3, 100) { X = 2, Y = 3 };

            map = new ICell[5][];

            for (int i = 0; i < 5; i++)
            {
                map[i] = new ICell[5];
                for (int j = 0; j < 5; j++)
                {
                    if (i == 0 || i == 5 || j == 0 || j == 5)
                    {
                        map[i][j] = new Cell(CellType.Wall);
                    }
                    else
                    {
                        map[i][j] = new Cell(CellType.Floor);
                    }
                }
            }

            entities = new List<IEntity>();

            entities.Add(player);
            entities.Add(enemy);

            maploot = new List<Loot>();

            IList<IItem> itempool = new List<IItem>();

            dl = new DungeonLogic(map, entities, maploot, itempool);
        }

        /// <summary>
        /// Testing the movement of the player.
        /// </summary>
        [Test]
        public static void TestPlayerMovement()
        {
            bool expected = true;
            bool result = dl.Player.Move(Movedirection.Forwards);

            Assert.AreEqual(expected, result);

            expected = false;
            result = dl.Player.Move(Movedirection.Forwards);

            Assert.AreEqual(expected, result);

            expected = true;
            result = dl.Player.Move(Movedirection.Left);

            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing facing the player.
        /// </summary>
        [Test]
        public static void TestPlayerFacing()
        {
            Direction expected = Direction.East;
            dl.Player.Face(Movedirection.Right);

            Direction result = player.Facing;

            Assert.AreEqual(expected, result);

            expected = Direction.North;
            dl.Player.Face(Movedirection.Left);

            result = player.Facing;

            Assert.AreEqual(expected, result);

            expected = Direction.West;
            dl.Player.Face(Movedirection.Left);

            result = player.Facing;

            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing player consuming to heal.
        /// </summary>
        [Test]
        public static void TestPlayerConsume()
        {
            Consumable testsnack = new Consumable() { Rarity = Rarity.Common, Name = "Snack", Health = 10, Icon = "snack", Stack = 1, Tooltip = "Snack for testing" };

            player.Inventory.Add(testsnack);

            player.Health -= 30;

            double expected = 80;

            dl.Player.UseConsumable(0);

            double result = player.Health;

            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing enemy movement.
        /// </summary>
        [Test]
        public static void TestEnemyMovement()
        {
            bool expected = false;

            EnemyLogic enemy = (EnemyLogic)dl.Entities.ToList().Find(e => e is EnemyLogic);

            bool result = enemy.Move(Direction.North);

            Assert.AreEqual(expected, result);

            expected = true;

            result = enemy.Move(Direction.South);

            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Testing enemy target finding.
        /// </summary>
        [Test]
        public static void TestEnemyFindTarget()
        {
            bool expected = true;

            EnemyLogic enemy = (EnemyLogic)dl.Entities.ToList().Find(e => e is EnemyLogic);

            bool result = enemy.FindTarget();

            Assert.AreEqual(expected, result);
        }
    }
}
