﻿// <copyright file="ILeaderboard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Support class for saving highscore.
    /// </summary>
    public interface ILeaderboard
    {
        /// <summary>
        /// Gets or sets the highest level reached.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get; set; }
    }
}
