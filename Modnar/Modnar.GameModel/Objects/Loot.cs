﻿// <copyright file="Loot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="ILoot"/> interface.
    /// </summary>
    [Serializable]
    public class Loot : ILoot
    {
        private List<Consumable> consumables;

        private List<Weapon> weapons;

        private List<Armour> armours;

        private int x;

        private int y;

        private string texture;

        /// <summary>
        /// Initializes a new instance of the <see cref="Loot"/> class.
        /// </summary>
        public Loot()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Loot"/> class.
        /// </summary>
        /// <param name="x">The x (horizontal) coordinate of where the loot is.</param>
        /// <param name="y">The y (vertical) coordinate of where the loot is.</param>
        /// <param name="texture">The appareance of the loot.</param>
        public Loot(int x, int y, string texture)
        {
            this.x = x;
            this.y = y;
            this.texture = texture;
            this.consumables = new List<Consumable>();
            this.weapons = new List<Weapon>();
            this.armours = new List<Armour>();
        }

        /// <inheritdoc/>
        public List<Consumable> Consumables { get => this.consumables; set => this.consumables = value; }

        /// <inheritdoc/>
        public List<Weapon> Weapons { get => this.weapons; set => this.weapons = value; }

        /// <inheritdoc/>
        public List<Armour> Armours { get => this.armours; set => this.armours = value; }

        /// <inheritdoc/>
        public int X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public int Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public string Texture { get => this.texture; set => this.texture = value; }
    }
}
