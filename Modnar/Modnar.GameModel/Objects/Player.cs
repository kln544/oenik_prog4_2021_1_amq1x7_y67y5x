﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IPlayer"/> interface.
    /// </summary>
    [Serializable]
    public class Player : IPlayer
    {
        private string name;
        private string texture = "player";
        private int x;
        private int y;
        private double maxhealth;
        private double health;
        private List<Damage> resistance;
        private int sight = 4;
        private bool canfly;
        private Direction facing = Direction.North;
        private int str;
        private int wis;
        private int dex;
        private double maxmana;
        private double mana;
        private double maxstamina;
        private double stamina;
        private List<Consumable> inventory;
        private List<Weapon> weaponry;
        private List<Armour> armoury;
        private Armour armour;
        private Weapon weapon;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="maxhealth">Maximum health of the player.</param>
        /// <param name="maxmana">Maximum mana of the player.</param>
        /// <param name="maxstamina">Maximum stamina of the player.</param>
        /// <param name="fly">Whether the player can fly.</param>
        /// <param name="facing">The direction the player is facing.</param>
        /// <param name="health">Current health of the player.</param>
        /// <param name="mana">Current mana of the player.</param>
        /// <param name="stamina">Current stamina of the player.</param>
        /// <param name="name">Name of the player.</param>
        /// <param name="resistance">The resitances of the player.</param>
        /// <param name="inventory">The player's inventory.</param>
        /// <param name="weaponry">The player's weapons.</param>
        /// <param name="weapon">The player's equiqed weapon. Default: fist.</param>
        /// <param name="armoury">The player's armours.</param>
        /// <param name="armour">The player's equiqed armour.</param>
        /// <param name="x">The x (horizontal) coordinate of the player.</param>
        /// <param name="y">The y (vertical) coordinate of the player.</param>
        public Player(double maxhealth, double maxmana, double maxstamina, int x, int y, bool fly = false, Direction facing = Direction.North, double health = -1, double mana = -1, double stamina = -1, string name = "player", List<Damage> resistance = null, List<Consumable> inventory = null, List<Weapon> weaponry = null, List<Armour> armoury = null, Armour armour = null, Weapon weapon = null)
        {
            this.name = name;
            this.canfly = fly;
            this.maxhealth = maxhealth;
            this.maxmana = maxmana;
            this.maxstamina = maxstamina;

            if (health < 0)
            {
                this.health = this.maxhealth;
            }
            else
            {
                this.Health = health;
            }

            if (mana < 0)
            {
                this.mana = this.maxmana;
            }
            else
            {
                this.Mana = mana;
            }

            if (stamina < 0)
            {
                this.stamina = this.maxstamina;
            }
            else
            {
                this.Stamina = stamina;
            }

            this.facing = facing;

            this.x = x;
            this.y = y;

            if (resistance != null)
            {
                this.resistance = resistance;
            }
            else
            {
                this.resistance = new List<Damage>();
                this.resistance.Add(new Damage(Damagetype.Physical));
                this.resistance.Add(new Damage(Damagetype.Fire));
                this.resistance.Add(new Damage(Damagetype.Cold));
                this.resistance.Add(new Damage(Damagetype.Lightning));
            }

            if (inventory != null)
            {
                this.inventory = inventory;
            }
            else
            {
                this.inventory = new List<Consumable>();
            }

            if (weaponry != null)
            {
                this.weaponry = weaponry;
            }
            else
            {
                this.weaponry = new List<Weapon>();
            }

            this.armoury = armoury;

            if (weapon != null)
            {
                this.weapon = weapon;
            }
            else
            {
                this.weapon = new Weapon(true);
            }

            if (armour != null)
            {
                this.armour = armour;
            }
            else
            {
                this.armour = new Armour(true);
            }
        }

        /// <inheritdoc/>
        public string Name { get => this.name; set => this.name = value; }

        /// <inheritdoc/>
        public Direction Facing { get => this.facing; set => this.facing = value; }

        /// <inheritdoc/>
        public int X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public int Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public string Texture { get => this.texture; set => this.texture = value; }

        /// <inheritdoc/>
        public double MaxHealth { get => this.maxhealth; set => this.maxhealth = value; }

        /// <inheritdoc/>
        public double Health
        {
            get => this.health; set
            {
                if (value > this.maxhealth)
                {
                    this.health = this.maxhealth;
                }
                else if (value < 0)
                {
                    this.health = 0;
                }
                else
                {
                    this.health = value;
                }
            }
        }

        /// <inheritdoc/>
        public List<Damage> Resistance { get => this.resistance; set => this.resistance = value; }

        /// <inheritdoc/>
        public int Sight { get => this.sight; set => this.sight = value; }

        /// <inheritdoc/>
        public bool CanFly { get => this.canfly; set => this.canfly = value; }

        /// <inheritdoc/>
        public int Str { get => this.str; set => this.str = value; }

        /// <inheritdoc/>
        public int Wis { get => this.wis; set => this.wis = value; }

        /// <inheritdoc/>
        public int Dex { get => this.dex; set => this.dex = value; }

        /// <inheritdoc/>
        public double MaxMana { get => this.maxmana; set => this.maxmana = value; }

        /// <inheritdoc/>
        public double Mana
        {
            get => this.mana; set
            {
                if (value > this.maxmana)
                {
                    this.mana = this.maxmana;
                }
                else if (value < 0)
                {
                    this.mana = 0;
                }
                else
                {
                    this.mana = value;
                }
            }
        }

        /// <inheritdoc/>
        public double MaxStamina { get => this.maxstamina; set => this.maxstamina = value; }

        /// <inheritdoc/>
        public double Stamina
        {
            get => this.stamina; set
            {
                if (value > this.maxstamina)
                {
                    this.stamina = this.maxstamina;
                }
                else if (value < 0)
                {
                    this.stamina = 0;
                }
                else
                {
                    this.stamina = value;
                }
            }
        }

        /// <inheritdoc/>
        public List<Consumable> Inventory { get => this.inventory; set => this.inventory = value; }

        /// <inheritdoc/>
        public List<Weapon> Weaponry { get => this.weaponry; set => this.weaponry = value; }

        /// <inheritdoc/>
        public List<Armour> Armoury { get => this.armoury; set => this.armoury = value; }

        /// <inheritdoc/>
        public Armour Armour
        {
            get => this.armour; set
            {
                if (value == null)
                {
                    this.armour = new Armour(true);
                }
                else
                {
                    this.armour = value;
                }
            }
        }

        /// <inheritdoc/>
        public Weapon Weapon
        {
            get => this.weapon; set
            {
                if (value == null)
                {
                    this.weapon = new Weapon(true);
                }
                else
                {
                    this.weapon = value;
                }
            }
        }
    }
}
