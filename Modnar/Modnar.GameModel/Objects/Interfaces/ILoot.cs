﻿// <copyright file="ILoot.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for in-game loot.
    /// </summary>
    public interface ILoot : IObject
    {
        /// <summary>
        /// Gets or sets the stored consumables.
        /// </summary>
        public List<Consumable> Consumables { get; set; }

        /// <summary>
        /// Gets or sets the stored weapons.
        /// </summary>
        public List<Weapon> Weapons { get; set; }

        /// <summary>
        /// Gets or sets the stored armours.
        /// </summary>
        public List<Armour> Armours { get; set; }
    }
}
