﻿// <copyright file="IPlayer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the player of the game.
    /// </summary>
    public interface IPlayer : IEntity
    {
        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the player's strenght.
        /// </summary>
        public int Str { get; set; }

        /// <summary>
        /// Gets or sets the player's Wisdom.
        /// </summary>
        public int Wis { get; set; }

        /// <summary>
        /// Gets or sets the player's dexterity.
        /// </summary>
        public int Dex { get; set; }

        /// <summary>
        /// Gets or sets the player's maximum mana.
        /// </summary>
        public double MaxMana { get; set; }

        /// <summary>
        /// Gets or sets the player's current mana.
        /// </summary>
        public double Mana { get; set; }

        /// <summary>
        /// Gets or sets the player's maximum stamina.
        /// </summary>
        public double MaxStamina { get; set; }

        /// <summary>
        /// Gets or sets the player's current mana.
        /// </summary>
        public double Stamina { get; set; }

        /// <summary>
        /// Gets or sets the player's inventory.
        /// </summary>
        public List<Consumable> Inventory { get; set; }

        /// <summary>
        /// Gets or sets or sets the player's collection of weapons.
        /// </summary>
        public List<Weapon> Weaponry { get; set; }

        /// <summary>
        /// Gets or sets the player's collection of armours.
        /// </summary>
        public List<Armour> Armoury { get; set; }

        /// <summary>
        /// Gets or sets the player's equiqed weapon.
        /// </summary>
        public Weapon Weapon { get; set; }

        /// <summary>
        /// Gets or sets the player's equiqed weapon.
        /// </summary>
        public Armour Armour { get; set; }
    }
}
