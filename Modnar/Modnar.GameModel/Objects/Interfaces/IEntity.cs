﻿// <copyright file="IEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the entities of the game.
    /// </summary>
    public interface IEntity : IObject
    {
        /// <summary>
        /// Gets or sets the maximum health of the entity.
        /// </summary>
        public double MaxHealth { get; set; }

        /// <summary>
        /// Gets or sets the health of the entity.
        /// </summary>
        public double Health { get; set; }

        /// <summary>
        /// Gets or sets the resistance values of the entity.
        /// </summary>
        public List<Damage> Resistance { get; set; }

        /// <summary>
        /// Gets or sets the sighting range of the entity.
        /// </summary>
        public int Sight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity can fly.
        /// </summary>
        public bool CanFly { get; set; }

        /// <summary>
        /// Gets or sets the direction the entity is facing.
        /// </summary>
        public Direction Facing { get; set; }
    }
}
