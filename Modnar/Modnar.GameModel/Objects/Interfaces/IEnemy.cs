﻿// <copyright file="IEnemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the enemies in the game.
    /// </summary>
    public interface IEnemy : IEntity
    {
        /// <summary>
        /// Gets or sets the damage values of the entity.
        /// </summary>
        public List<Damage> Damage { get; set; }

        /// <summary>
        /// Gets or sets the difficulty score of the enemy.
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// Gets or sets the attack range of the entity.
        /// </summary>
        public int Range { get; set; }

        /// <summary>
        /// Gets or sets the speed of the enemy.
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Gets or sets memory.
        /// </summary>
        public int Memory { get; set; }

        /// <summary>
        /// Gets or sets the sound of the enemy.
        /// </summary>
        public string Sound { get; set; }

        /// <summary>
        /// Method to make a deepclone of <see cref="IEnemy"/>.
        /// </summary>
        /// <returns>Clone of <see cref="IEnemy"/>.</returns>
        public IEnemy DeepClone();
    }
}
