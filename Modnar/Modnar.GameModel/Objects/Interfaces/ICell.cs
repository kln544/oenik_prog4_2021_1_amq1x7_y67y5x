﻿// <copyright file="ICell.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the cells of the map.
    /// </summary>
    public interface ICell
    {
        /// <summary>
        /// Gets or sets the texture of the cell.
        /// </summary>
        public string Texture { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether entities can fly over the cell.
        /// </summary>
        public bool Flyover { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether entities can walk over the cell.
        /// </summary>
        public bool Walkover { get; set; }
    }
}
