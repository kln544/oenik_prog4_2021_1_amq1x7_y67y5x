﻿// <copyright file="IObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the objects of the game.
    /// </summary>
    public interface IObject
    {
        /// <summary>
        /// Gets or sets the x (horizontal) coordinate of the entity.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets y (vertical) coordinate of the entity.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Gets or sets the texture of the entity.
        /// </summary>
        public string Texture { get; set; }
    }
}
