﻿// <copyright file="Cell.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="ICell"/> interface.
    /// </summary>
    public class Cell : ICell
    {
        private bool flyover;
        private bool walkover;
        private string texture;

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        public Cell()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        /// <param name="type">Type to change into.</param>
        public Cell(CellType type)
        {
            switch (type)
            {
                case CellType.Wall:
                    this.texture = "w";

                    this.walkover = false;

                    this.flyover = false;
                    break;
                case CellType.Floor:
                    this.texture = "f";

                    this.walkover = true;

                    this.flyover = true;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        /// <param name="texture">Mesh of the cell.</param>
        /// <param name="flyover">Whether it can be flown over.</param>
        /// <param name="walkover">Whether it can be walekd over.</param>
        public Cell(string texture, bool flyover, bool walkover)
        {
            this.texture = texture;
            this.flyover = flyover;
            this.walkover = walkover;
        }

        /// <inheritdoc/>
        public bool Flyover { get => this.flyover; set => this.flyover = value; }

        /// <inheritdoc/>
        public bool Walkover { get => this.walkover; set => this.walkover = value; }

        /// <inheritdoc/>
        public string Texture { get => this.texture; set => this.texture = value; }
    }
}