﻿// <copyright file="Enemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IEnemy"/> interface.
    /// </summary>
    [Serializable]
    public class Enemy : IEnemy
    {
        private string texture;
        private double maxhealth;
        private double health;
        private List<Damage> damage;
        private List<Damage> resistance;
        private int sight;
        private int range;
        private bool canfly;
        private Direction facing = Direction.North;
        private int x;
        private int y;
        private int difficulty;
        private int speed;
        private int memory;
        private string sound;

        /// <summary>
        /// Initializes a new instance of the <see cref="Enemy"/> class.
        /// </summary>
        public Enemy()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Enemy"/> class.
        /// </summary>
        /// <param name="texture">The sprite of the enemy.</param>
        /// <param name="maxhealth">Maximum health of the enemy.</param>
        /// <param name="damage">Base damage of the enemy.</param>
        /// <param name="sight">Base sighting range of the enemy.</param>
        /// <param name="range">Base attack range of the enemy.</param>
        /// <param name="resistance">Resistances of the enemy.</param>
        /// <param name="fly">Whether the entity can fly.</param>
        /// <param name="speed">Speed of the entity.</param>
        /// <param name="difficulty">The difficutly score of the enemy.</param>
        public Enemy(string texture, double maxhealth, List<Damage> damage, int sight, int speed, int difficulty, List<Damage> resistance = null, int range = 1, bool fly = false)
        {
            this.texture = texture;
            this.maxhealth = maxhealth;
            this.health = maxhealth;
            this.damage = damage ?? throw new ArgumentNullException(nameof(damage));
            this.sight = sight;
            this.speed = speed;
            this.difficulty = difficulty;
            if (resistance != null)
            {
                this.resistance = resistance;
            }
            else
            {
                this.resistance = new List<Damage>();
                this.resistance.Add(new Damage(Damagetype.Physical));
                this.resistance.Add(new Damage(Damagetype.Fire));
                this.resistance.Add(new Damage(Damagetype.Cold));
                this.resistance.Add(new Damage(Damagetype.Lightning));
            }

            this.range = range;
            this.canfly = fly;
        }

        /// <inheritdoc/>
        public Direction Facing { get => this.facing; set => this.facing = value; }

        /// <inheritdoc/>
        public int X { get => this.x; set => this.x = value; }

        /// <inheritdoc/>
        public int Y { get => this.y; set => this.y = value; }

        /// <inheritdoc/>
        public string Texture { get => this.texture; set => this.texture = value; }

        /// <inheritdoc/>
        public double MaxHealth { get => this.maxhealth; set => this.maxhealth = value; }

        /// <inheritdoc/>
        public double Health
        {
            get => this.health;
            set
            {
                if (value > this.maxhealth)
                {
                    this.health = this.maxhealth;
                }
                else
                {
                    this.health = value;
                }
            }
        }

        /// <inheritdoc/>
        public bool CanFly { get => this.canfly; set => this.canfly = value; }

        /// <inheritdoc/>
        public List<Damage> Damage { get => this.damage; set => this.damage = value; }

        /// <inheritdoc/>
        public List<Damage> Resistance { get => this.resistance; set => this.resistance = value; }

        /// <inheritdoc/>
        public int Sight { get => this.sight; set => this.sight = value; }

        /// <inheritdoc/>
        public int Range { get => this.range; set => this.range = value; }

        /// <inheritdoc/>
        public int Difficulty { get => this.difficulty; set => this.difficulty = value; }

        /// <inheritdoc/>
        public int Speed { get => this.speed; set => this.speed = value; }

        /// <inheritdoc/>
        public int Memory
        {
            get => this.memory;
            set
            {
                if (value > 0)
                {
                    this.memory = value;
                }
                else
                {
                    this.memory = 0;
                }
            }
        }

        /// <inheritdoc/>
        public string Sound { get => this.sound; set => this.sound = value; }

        /// <inheritdoc/>
        public IEnemy DeepClone()
        {
            return (Enemy)JsonSerializer.Deserialize(JsonSerializer.Serialize(this, typeof(Enemy)), typeof(Enemy));
        }
    }
}
