﻿// <copyright file="RoomModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IRoomModel"/> interface.
    /// </summary>
    public class RoomModel : IRoomModel
    {
        private bool north;

        private bool east;

        private bool south;

        private bool west;

        private bool spawn;

        private ICell[][] room;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoomModel"/> class.
        /// </summary>
        public RoomModel()
        {
            this.room = new ICell[13][];

            for (int i = 0; i < this.room.Length; i++)
            {
                this.room[i] = new ICell[13];
            }
        }

        /// <inheritdoc/>
        public bool North { get => this.north; set => this.north = value; }

        /// <inheritdoc/>
        public bool East { get => this.east; set => this.east = value; }

        /// <inheritdoc/>
        public bool South { get => this.south; set => this.south = value; }

        /// <inheritdoc/>
        public bool West { get => this.west; set => this.west = value; }

        /// <inheritdoc/>
        public ICell[][] Room { get => this.room; set => this.room = value; }

        /// <inheritdoc/>
        public bool Spawn { get => this.spawn; set => this.spawn = value; }
    }
}
