﻿// <copyright file="IRoomNode.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for RoomNode, an assisting class for map generation.
    /// </summary>
    internal interface IRoomNode
    {
        /// <summary>
        /// Gets or sets a value indicating whether it can have a northen neighbour.
        /// </summary>
        public bool North { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it can have an eastern neighbour.
        /// </summary>
        public bool East { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it can have a southern neighbour.
        /// </summary>
        public bool South { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it can have a western neighbour.
        /// </summary>
        public bool West { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is a spawn room (true), an exit room (false) or a normal room (null).
        /// </summary>
        public bool Spawn { get; set; }

        /// <summary>
        /// Gets the x (horizontal) coordinate of the node, it is the middle of the room.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// Gets the y (vertical) coordinate of the node, it is the middle of the room.
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// Gets or sets the room that will be generated.
        /// </summary>
        public RoomModel Room { get; set; }
    }
}
