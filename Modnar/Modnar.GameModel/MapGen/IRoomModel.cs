﻿// <copyright file="IRoomModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the room model.
    /// </summary>
    public interface IRoomModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether it has a northen exit.
        /// </summary>
        public bool North { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it has an eastern exit.
        /// </summary>
        public bool East { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it has a southern exit.
        /// </summary>
        public bool South { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it has a western exit.
        /// </summary>
        public bool West { get; set; }

        /// <summary>
        /// Gets or sets the actual cells the room is made out of.
        /// </summary>
        public ICell[][] Room { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is a spawn, an exit or just a normal room.
        /// </summary>
        public bool Spawn { get; set; }
    }
}
