﻿// <copyright file="RoomNode.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IRoomNode"/> interface.
    /// </summary>
    public class RoomNode : IRoomNode
    {
        private int x;

        private int y;

        private bool north;

        private bool east;

        private bool south;

        private bool west;

        private bool spawn;

        private RoomModel room;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoomNode"/> class.
        /// </summary>
        /// <param name="x">The x (horizontal) coordinate of the node.</param>
        /// <param name="y">The y (vertical) coordinate of the node.</param>
        public RoomNode(int x, int y)
        {
            this.x = x;

            this.y = y;
        }

        /// <inheritdoc/>
        public bool North { get => this.north; set => this.north = value; }

        /// <inheritdoc/>
        public bool East { get => this.east; set => this.east = value; }

        /// <inheritdoc/>
        public bool South { get => this.south; set => this.south = value; }

        /// <inheritdoc/>
        public bool West { get => this.west; set => this.west = value; }

        /// <inheritdoc/>
        public bool Spawn { get => this.spawn; set => this.spawn = value; }

        /// <inheritdoc/>
        public int X { get => this.x; }

        /// <inheritdoc/>
        public int Y { get => this.y; }

        /// <inheritdoc/>
        public RoomModel Room { get => this.room; set => this.room = value; }
}
}
