﻿// <copyright file="Damage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IDamage"/> interface.
    /// </summary>
    [Serializable]
    public class Damage : IDamage
    {
        private Damagetype type;

        private double value;

        /// <summary>
        /// Initializes a new instance of the <see cref="Damage"/> class.
        /// </summary>
        public Damage()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Damage"/> class.
        /// </summary>
        /// <param name="type">Type of the damage.</param>
        /// <param name="value">Value of the damage.</param>
        public Damage(Damagetype type, double value = 0)
        {
            this.type = type;

            this.value = value;
        }

        /// <inheritdoc/>
        public Damagetype Type { get => this.type; set => this.type = value; }

        /// <inheritdoc/>
        public double Value { get => this.value; set => this.value = value; }
    }
}
