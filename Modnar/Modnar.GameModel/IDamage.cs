﻿// <copyright file="IDamage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for weapon and skill damage.
    /// </summary>
    public interface IDamage
    {
        /// <summary>
        /// Gets or sets the type of the damage.
        /// </summary>
        public Damagetype Type { get; set; }

        /// <summary>
        /// Gets or sets value of the damage.
        /// </summary>
        public double Value { get; set; }
    }
}
