﻿// <copyright file="WeaponType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for weapon types.
    /// </summary>
    [Flags]
    [Serializable]
    public enum WeaponType
    {
        /// <summary>
        /// Any type.
        /// </summary>
        None = 0,

        /// <summary>
        /// Sword type.
        /// </summary>
        Sword = 1,

        /// <summary>
        /// Spear type.
        /// </summary>
        Spear = 2,

        /// <summary>
        /// Axe type.
        /// </summary>
        Axe = 4,

        /// <summary>
        /// Bow type.
        /// </summary>
        Bow = 8,

        /// <summary>
        /// Wand type.
        /// </summary>
        Wand = 16,

        /// <summary>
        /// Fist type.
        /// </summary>
        Fist = 32,
    }
}
