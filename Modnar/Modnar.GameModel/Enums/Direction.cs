﻿// <copyright file="Direction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for directions.
    /// </summary>
    [Serializable]
    public enum Direction
    {
        /// <summary>
        /// Facing north.
        /// </summary>
        North,

        /// <summary>
        /// Facing east.
        /// </summary>
        East,

        /// <summary>
        /// Facing south.
        /// </summary>
        South,

        /// <summary>
        /// Facing west.
        /// </summary>
        West,
    }
}
