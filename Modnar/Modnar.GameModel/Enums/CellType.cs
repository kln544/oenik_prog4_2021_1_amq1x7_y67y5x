﻿// <copyright file="CellType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for wall types.
    /// </summary>
    public enum CellType
    {
        /// <summary>
        /// Wall cell type.
        /// </summary>
        Wall,

        /// <summary>
        /// Floor cell type.
        /// </summary>
        Floor,
    }
}
