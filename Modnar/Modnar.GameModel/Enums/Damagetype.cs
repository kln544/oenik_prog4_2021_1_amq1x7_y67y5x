﻿// <copyright file="Damagetype.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for damage types.
    /// </summary>
    [Serializable]
    public enum Damagetype
    {
        /// <summary>
        /// Physical damage type.
        /// </summary>
        Physical,

        /// <summary>
        /// Fire damage type.
        /// </summary>
        Fire,

        /// <summary>
        /// Lightning damage type.
        /// </summary>
        Lightning,

        /// <summary>
        /// Cold damage type.
        /// </summary>
        Cold,
    }
}
