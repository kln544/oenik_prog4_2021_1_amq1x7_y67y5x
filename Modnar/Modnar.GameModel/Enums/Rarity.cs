﻿// <copyright file="Rarity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Rarity identifier for items.
    /// </summary>
    public enum Rarity
    {
        /// <summary>
        /// Of the lowest value.
        /// </summary>
        Common,

        /// <summary>
        /// A decent find.
        /// </summary>
        Uncommon,

        /// <summary>
        /// An intresting item.
        /// </summary>
        Rare,

        /// <summary>
        /// Great worth!
        /// </summary>
        Epic,

        /// <summary>
        /// Bounty of legends!
        /// </summary>
        Legendary,

        /// <summary>
        /// Imbued with eternal power!
        /// </summary>
        Relic,
    }
}
