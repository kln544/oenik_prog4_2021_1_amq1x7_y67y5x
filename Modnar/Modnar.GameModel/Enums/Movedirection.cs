// <copyright file="Movedirection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for moving directions.
    /// </summary>
    public enum Movedirection
    {
        /// <summary>
        /// Moving forwards.
        /// </summary>
        Forwards,

        /// <summary>
        /// Moving right.
        /// </summary>
        Right,

        /// <summary>
        /// Moving left.
        /// </summary>
        Left,

        /// <summary>
        /// Moving backwards.
        /// </summary>
        Backwards,
    }
}
