﻿// <copyright file="Leaderboard.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="ILeaderboard"/> interface.
    /// </summary>
    public class Leaderboard : ILeaderboard
    {
        private int level;

        private string name;

        /// <inheritdoc/>
        public int Level { get => this.level; set => this.level = value; }

        /// <inheritdoc/>
        public string Name { get => this.name; set => this.name = value; }
    }
}
