﻿// <copyright file="Weapon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IWeapon"/> interface.
    /// </summary>
    [Serializable]
    public class Weapon : IWeapon
    {
        private Rarity rarity;

        private string name;

        private WeaponType type;

        private List<Damage> damage;

        private int maxdurability;

        private int durability;

        private int range;

        private string icon;

        private string tooltip;

        /// <summary>
        /// Initializes a new instance of the <see cref="Weapon"/> class.
        /// </summary>
        public Weapon()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Weapon"/> class. Default "fist" variant. Must have this and an empty contructor.
        /// </summary>
        /// <param name="fist">Had to have a paramter.</param>
        public Weapon(bool fist)
        {
            if (fist)
            {
                this.name = "Fist";
                this.damage = new List<Damage>();
                this.damage.Add(new Damage { Type = Damagetype.Physical, Value = 10 });
                this.maxdurability = -999; // unbreakable
                this.durability = this.maxdurability;
                this.range = 1;
                this.type = WeaponType.Fist;
                this.icon = @"Images\Icons\fist.png";
                this.tooltip = "Smash some faces with your fist.";
            }
        }

        /// <inheritdoc/>
        public Rarity Rarity { get => this.rarity; set => this.rarity = value; }

        /// <inheritdoc/>
        public string Name { get => this.name; set => this.name = value; }

        /// <inheritdoc/>
        public List<Damage> Damage { get => this.damage;  set => this.damage = value; }

        /// <inheritdoc/>
        public WeaponType Type { get => this.type; set => this.type = value; }

        /// <inheritdoc/>
        public int Maxdurability { get => this.maxdurability; set => this.maxdurability = value; }

        /// <inheritdoc/>
        public int Durability
        {
            get => this.durability;
            set
            {
                if (value > this.maxdurability)
                {
                    this.durability = this.maxdurability;
                }
                else if (value >= 0)
                {
                    this.durability = value;
                }
            }
        }

        /// <inheritdoc/>
        public int Range { get => this.range; set => this.range = value; }

        /// <inheritdoc/>
        public string Icon { get => this.icon; set => this.icon = value; }

        /// <inheritdoc/>
        public string Tooltip { get => this.tooltip; set => this.tooltip = value; }

        /// <summary>
        /// Method to make a deepclone of <see cref="Weapon"/>.
        /// </summary>
        /// <returns>Clone of <see cref="Weapon"/>.</returns>
        public Weapon DeepClone()
        {
            return (Weapon)JsonSerializer.Deserialize(JsonSerializer.Serialize(this, typeof(Weapon)), typeof(Weapon));
        }
    }
}
