﻿// <copyright file="IConsumable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the consumables in the game. Implementing <see cref="IItem"/> interface.
    /// </summary>
    public interface IConsumable : IItem
    {
        /// <summary>
        /// Gets how much health the item restores.
        /// </summary>
        public double Health { get; }

        /// <summary>
        /// Gets how much mana the item restores.
        /// </summary>
        public double Mana { get; }

        /// <summary>
        /// Gets how much stamina the item restores.
        /// </summary>
        public double Stamina { get; }

        /// <summary>
        /// Gets or sets the current stack of the item.
        /// </summary>
        public int Stack { get; set; }
    }
}
