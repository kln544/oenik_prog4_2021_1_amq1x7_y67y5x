﻿// <copyright file="IArmour.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the armor in the game. Implementing <see cref="IItem"/> interface.
    /// </summary>
    public interface IArmour : IItem
    {
        /// <summary>
        /// Gets or sets the durability of the armour.
        /// </summary>
        public int Durability { get; set; }

        /// <summary>
        /// Gets the max durability of the armour.
        /// </summary>
        public int Maxdurability { get; }

        /// <summary>
        /// Gets the defense values of the armour.
        /// </summary>
        public List<Damage> Defense { get; }
    }
}
