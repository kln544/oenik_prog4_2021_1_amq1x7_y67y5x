﻿// <copyright file="IWeapon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the weapons in the game. Implementing <see cref="IItem"/> interface.
    /// </summary>
    public interface IWeapon : IItem
    {
        /// <summary>
        /// Gets or sets the type of the weapon.
        /// </summary>
        public WeaponType Type { get; set; }

        /// <summary>
        /// Gets or sets the durability of the weapon.
        /// </summary>
        public int Durability { get; set; }

        /// <summary>
        /// Gets or sets the max durability of the weapon.
        /// </summary>
        public int Maxdurability { get; set; }

        /// <summary>
        /// Gets or sets the damage of the weapon.
        /// </summary>
        public List<Damage> Damage { get; set; }

        /// <summary>
        /// Gets or sets the range of the weapon.
        /// </summary>
        public int Range { get; set; }
    }
}
