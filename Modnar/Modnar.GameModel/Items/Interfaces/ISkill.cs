﻿// <copyright file="ISkill.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the skills in the game.
    /// </summary>
    public interface ISkill
    {
        /// <summary>
        /// Gets the name of the skill.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the icon of the skill.
        /// </summary>
        public string Icon { get; }

        /// <summary>
        /// Gets the skill's description.
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Gets the type of weapon that the skill can be used with.
        /// </summary>
        public WeaponType Type { get; }

        /// <summary>
        /// Gets the skill.
        /// </summary>
        public Action Skillact { get; }
    }
}
