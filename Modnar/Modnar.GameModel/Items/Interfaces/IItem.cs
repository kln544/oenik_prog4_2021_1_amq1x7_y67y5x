﻿// <copyright file="IItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the items in the game.
    /// </summary>
    public interface IItem
    {
        /// <summary>
        /// Gets or sets the name of the item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the icon of the item.
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Gets or sets the item's tooltip.
        /// </summary>
        public string Tooltip { get; set; }

        /// <summary>
        /// Gets or sets the rarity of the item.
        /// </summary>
        public Rarity Rarity { get; set; }
    }
}
