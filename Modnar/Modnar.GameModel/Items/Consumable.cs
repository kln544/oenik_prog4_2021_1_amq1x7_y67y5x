﻿// <copyright file="Consumable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IConsumable"/> interface.
    /// </summary>
    [Serializable]
    public class Consumable : IConsumable
    {
        private Rarity rarity;

        private string name;

        private double health;

        private double mana;

        private double stamina;

        private int stack;

        private string icon;

        private string tooltip;

        /// <summary>
        /// Initializes a new instance of the <see cref="Consumable"/> class.
        /// </summary>
        public Consumable()
        {
        }

        /// <inheritdoc/>
        public Rarity Rarity { get => this.rarity; set => this.rarity = value; }

        /// <inheritdoc/>
        public string Name { get => this.name; set => this.name = value; }

        /// <inheritdoc/>
        public double Health { get => this.health; set => this.health = value; }

        /// <inheritdoc/>
        public double Mana { get => this.mana; set => this.mana = value; }

        /// <inheritdoc/>
        public double Stamina { get => this.stamina; set => this.stamina = value; }

        /// <inheritdoc/>
        public int Stack { get => this.stack; set => this.stack = value; }

        /// <inheritdoc/>
        public string Icon { get => this.icon; set => this.icon = value; }

        /// <inheritdoc/>
        public string Tooltip { get => this.tooltip; set => this.tooltip = value; }

        /// <summary>
        /// Method to make a deepclone of <see cref="Consumable"/>.
        /// </summary>
        /// <returns>Clone of <see cref="Consumable"/>.</returns>
        public Consumable DeepClone()
        {
            return (Consumable)JsonSerializer.Deserialize(JsonSerializer.Serialize(this, typeof(Consumable)), typeof(Consumable));
        }
    }
}
