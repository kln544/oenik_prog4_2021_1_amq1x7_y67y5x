﻿// <copyright file="Armour.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="IArmour"/> interface.
    /// </summary>
    [Serializable]
    public class Armour : IArmour
    {
        private Rarity rarity;

        private string name;

        private List<Damage> defense;

        private int maxdurability;

        private int durability;

        private string icon;

        private string tooltip;

        /// <summary>
        /// Initializes a new instance of the <see cref="Armour"/> class.
        /// </summary>
        public Armour()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Armour"/> class. Must have this and an empty constructor.
        /// </summary>
        /// <param name="none">Must have this bool.</param>
        public Armour(bool none)
        {
            if (none)
            {
                this.rarity = Rarity.Common;

                this.name = "None.";

                this.defense = new List<Damage>() { new Damage(Damagetype.Physical), new Damage(Damagetype.Fire), new Damage(Damagetype.Lightning), new Damage(Damagetype.Cold) };

                this.maxdurability = -999;

                this.durability = this.maxdurability;

                this.icon = "none";

                this.tooltip = "You have no armour on.";
            }
        }

        /// <inheritdoc/>
        public List<Damage> Defense { get => this.defense; set => this.defense = value; }

        /// <inheritdoc/>
        public Rarity Rarity { get => this.rarity; set => this.rarity = value; }

        /// <inheritdoc/>
        public string Name { get => this.name; set => this.name = value; }

        /// <inheritdoc/>
        public int Maxdurability { get => this.maxdurability; set => this.maxdurability = value; }

        /// <inheritdoc/>
        public int Durability
        {
            get => this.durability;
            set
            {
                if (value > this.maxdurability)
                {
                    this.durability = this.maxdurability;
                }
                else if (value >= 0)
                {
                    this.durability = value;
                }
            }
        }

        /// <inheritdoc/>
        public string Icon { get => this.icon; set => this.icon = value; }

        /// <inheritdoc/>
        public string Tooltip { get => this.tooltip; set => this.tooltip = value; }

        /// <summary>
        /// Method to make a deepclone of <see cref="Armour"/>.
        /// </summary>
        /// <returns>Clone of <see cref="Armour"/>.</returns>
        public Armour DeepClone()
        {
            return (Armour)JsonSerializer.Deserialize(JsonSerializer.Serialize(this, typeof(Armour)), typeof(Armour));
        }
    }
}
