﻿// <copyright file="Skill.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="ISkill"/> interface.
    /// </summary>
    [Serializable]
    public class Skill : ISkill
    {
        private WeaponType type;

        private Action skillact;

        private string name;

        private string icon;

        private string description;

        /// <summary>
        /// Initializes a new instance of the <see cref="Skill"/> class.
        /// </summary>
        public Skill()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Skill"/> class.
        /// </summary>
        /// <param name="type">The type of weapon that uses this skill.</param>
        /// <param name="skillact">The skills method.</param>
        /// <param name="name">Name of the skill.</param>
        /// <param name="icon">Icon of the skill.</param>
        /// <param name="tooltip">Tooltip of the skill.</param>
        public Skill(WeaponType type, Action skillact, string name, string icon, string tooltip)
        {
            this.type = type;
            this.skillact = skillact;
            this.name = name;
            this.icon = icon;
            this.description = tooltip;
        }

        /// <inheritdoc/>
        public WeaponType Type { get => this.type; }

        /// <inheritdoc/>
        public Action Skillact { get => this.skillact; }

        /// <inheritdoc/>
        public string Name { get => this.name; }

        /// <inheritdoc/>
        public string Icon { get => this.icon; }

        /// <inheritdoc/>
        public string Description { get => this.description; }
    }
}
