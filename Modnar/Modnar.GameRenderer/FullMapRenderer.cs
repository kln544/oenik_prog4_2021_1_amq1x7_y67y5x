﻿// <copyright file="FullMapRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameRenderer
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Modnar.GameLogic;
    using Modnar.GameModel;

    /// <summary>
    /// Renderer class for the mini map.
    /// </summary>
    public class FullMapRenderer
    {
        private int tilesize;
        private IGameMaster gm;
        private double width;
        private double height;
        private Drawing background;

        /// <summary>
        /// Initializes a new instance of the <see cref="FullMapRenderer"/> class.
        /// </summary>
        /// <param name="gm">Game master.</param>
        /// <param name="width">Width.</param>
        /// <param name="height">Hight.</param>
        public FullMapRenderer(IGameMaster gm, double width, double height)
        {
            this.tilesize = 2;
            this.height = height;
            this.width = width;
            this.gm = gm ?? throw new ArgumentNullException(nameof(gm));
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>Drawing to be displayed.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetCells());
            return dg;
        }

        private Drawing GetBackground()
        {
            if (this.background == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.width, this.height));
                this.background = new GeometryDrawing(Brushes.DarkGray, null, backgroundGeometry);
            }

            return this.background;
        }

        private Drawing GetCells()
        {
            DrawingGroup dg = new DrawingGroup();
            for (int i = 0; i < this.gm.DungeonLogic.Map.Length; i++)
            {
                for (int j = 0; j < this.gm.DungeonLogic.Map[i].Length; j++)
                {
                    if (this.gm.DungeonLogic.Map[i][j].Walkover)
                    {
                        Geometry tile = new RectangleGeometry(new Rect(j * this.tilesize, i * this.tilesize, this.tilesize, this.tilesize));
                        dg.Children.Add(new GeometryDrawing(Brushes.Black, null, tile));
                    }
                }
            }

            return dg;
        }
    }
}
