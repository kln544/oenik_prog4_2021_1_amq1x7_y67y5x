// <copyright file="BrushHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameRenderer
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Helper Class for brushes.
    /// </summary>
    public static class BrushHelper
    {
        /// <summary>
        /// Creates image brushes.
        /// </summary>
        /// <param name="fname">File name.</param>
        /// <returns>Brush.</returns>
        public static Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(@"Images\" + fname, UriKind.Relative)));
            return ib;
        }
    }
}
