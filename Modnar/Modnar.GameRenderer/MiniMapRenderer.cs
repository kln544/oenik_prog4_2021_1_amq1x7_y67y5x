﻿// <copyright file="MiniMapRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameRenderer
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Modnar.GameLogic;
    using Modnar.GameModel;

    /// <summary>
    /// Renderer class for the mini map.
    /// </summary>
    public class MiniMapRenderer
    {
        private int tilesize;
        private IGameMaster gm;
        private double width;
        private double height;
        private Drawing background;

        /// <summary>
        /// Initializes a new instance of the <see cref="MiniMapRenderer"/> class.
        /// </summary>
        /// <param name="gm">Game master.</param>
        /// <param name="width">Width.</param>
        /// <param name="height">Hight.</param>
        public MiniMapRenderer(IGameMaster gm, double width, double height)
        {
            this.tilesize = 32;
            this.height = height;
            this.width = width;
            this.gm = gm ?? throw new ArgumentNullException(nameof(gm));
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>Drawing to be displayed.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetCells());
            dg.Children.Add(this.GetLoot());
            dg.Children.Add(this.GetEntities());
            return dg;
        }

        private Drawing GetBackground()
        {
            if (this.background == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.width, this.height));
                this.background = new GeometryDrawing(Brushes.Black, null, backgroundGeometry);
            }

            return this.background;
        }

        private Drawing GetCells()
        {
            DrawingGroup dg = new DrawingGroup();
            for (int i = this.gm.Player.X - 4; i <= this.gm.Player.X + 4; i++)
            {
                for (int j = this.gm.Player.Y - 4; j <= this.gm.Player.Y + 4; j++)
                {
                    if ((i >= 0 && j >= 0) && (i < this.gm.DungeonLogic.Map.Length && j < this.gm.DungeonLogic.Map[i].Length && this.gm.DungeonLogic.Map[j][i] != null))
                    {
                        Geometry tile = new RectangleGeometry(new Rect((i - this.gm.Player.X + 4) * this.tilesize, (j - this.gm.Player.Y + 4) * this.tilesize, this.tilesize, this.tilesize));
                        dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/" + this.gm.DungeonLogic.Map[j][i].Texture + ".bmp"), null, tile));
                    }
                }
            }

            return dg;
        }

        private Drawing GetLoot()
        {
            DrawingGroup dg = new DrawingGroup();
            foreach (var loot in this.gm.DungeonLogic.Lootlist)
            {
                if ((loot.X >= this.gm.Player.X - 4 && loot.Y >= this.gm.Player.Y - 4) && (loot.X <= this.gm.Player.X + 4 && loot.Y <= this.gm.Player.Y + 4))
                {
                    Geometry tile = new RectangleGeometry(new Rect((loot.X - this.gm.Player.X + 4) * this.tilesize, (loot.Y - this.gm.Player.Y + 4) * this.tilesize, this.tilesize, this.tilesize));
                    dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/bag.bmp"), null, tile));
                }
            }

            return dg;
        }

        private Drawing GetEntities()
        {
            DrawingGroup dg = new DrawingGroup();
            foreach (IEntityLogic entityLogic in this.gm.DungeonLogic.Entities)
            {
                if ((entityLogic.Entity.X >= this.gm.Player.X - 4 && entityLogic.Entity.Y >= this.gm.Player.Y - 4) && (entityLogic.Entity.X <= this.gm.Player.X + 4 && entityLogic.Entity.Y <= this.gm.Player.Y + 4))
                {
                    Geometry tile = new RectangleGeometry(new Rect((entityLogic.Entity.X - this.gm.Player.X + 4) * this.tilesize, (entityLogic.Entity.Y - this.gm.Player.Y + 4) * this.tilesize, this.tilesize, this.tilesize));

                    if (entityLogic.Entity is Player)
                    {
                        switch (entityLogic.Entity.Facing)
                        {
                            case Direction.North:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/playerN.bmp"), null, tile));
                                break;
                            case Direction.East:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/playerE.bmp"), null, tile));
                                break;
                            case Direction.South:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/playerS.bmp"), null, tile));
                                break;
                            case Direction.West:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/playerW.bmp"), null, tile));
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (entityLogic.Entity.Facing)
                        {
                            case Direction.North:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/enemyN.bmp"), null, tile));
                                break;
                            case Direction.East:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/enemyE.bmp"), null, tile));
                                break;
                            case Direction.South:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/enemyS.bmp"), null, tile));
                                break;
                            case Direction.West:
                                dg.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Minimap/enemyW.bmp"), null, tile));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return dg;
        }
    }
}
