﻿// <copyright file="Renderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Modnar.GameLogic;
    using Modnar.GameModel;

    /// <summary>
    /// Renderer class.
    /// </summary>
    public class Renderer
    {
        private double gameWidth;
        private double gameHeight;
        private IGameMaster gm;
        private Drawing background;

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="gm">Game Master.</param>
        /// <param name="gameWidth">Game width.</param>
        /// <param name="gameHeight">Game hight.</param>
        public Renderer(IGameMaster gm, double gameWidth, double gameHeight)
        {
            this.gameHeight = gameHeight;
            this.gameWidth = gameWidth;
            this.gm = gm ?? throw new ArgumentNullException(nameof(gm));
        }

        /// <summary>
        /// Build drawing.
        /// </summary>
        /// <returns>Drawing to be displayed.</returns>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetForeground());
            dg.Children.Add(this.GetPlayerStats());
            dg.Children.Add(this.GetEnemyHealth());
            return dg;
        }

        private Drawing GetForeground()
        {
            DrawingGroup drawingGroup = new DrawingGroup();
            Geometry foregroundGeometry = new RectangleGeometry(new Rect(0, 0, this.gameWidth, this.gameHeight));
            IEntity entity;
            ICell cell;
            ILoot loot;

            for (int i = 0; i < this.gm.DungeonLogic.Display.Length; i++)
            {
                for (int j = 0; j < this.gm.DungeonLogic.Display[i].Length / 2; j++)
                {
                    foregroundGeometry = new RectangleGeometry(new Rect(0, 0, this.gameWidth, this.gameHeight));
                    entity = this.gm.DungeonLogic.Display[i][j].Entity;
                    cell = this.gm.DungeonLogic.Display[i][j].Cell;
                    loot = this.gm.DungeonLogic.Display[i][j].Loot;

                    if (cell != null)
                    {
                        drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Map/" + cell.Texture + @"/" + i + j + ".png"), null, foregroundGeometry));
                    }

                    if (loot != null)
                    {
                        if (i == this.gm.DungeonLogic.Display.Length - 1)
                        {
                        }
                        else
                        {
                            foregroundGeometry = new RectangleGeometry(this.GeometryCalculator(j, i, this.gm.DungeonLogic.Display[i].Length));
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Loot/" + loot.Texture + @"/bag.png"), null, foregroundGeometry));
                        }
                    }

                    if (entity != null && entity is not Player)
                    {
                        string face = this.Dir(entity.Facing);
                        if (i == this.gm.DungeonLogic.Display.Length - 1)
                        { // TODO!!!
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Entity/" + i + j + ".png"), null, foregroundGeometry));
                        }
                        else if (File.Exists(@"Images/Entity/" + entity.Texture + @"/" + face + ".png"))
                        {
                            foregroundGeometry = new RectangleGeometry(this.GeometryCalculator(j, i, this.gm.DungeonLogic.Display[i].Length));
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Entity/" + entity.Texture + @"/" + face + ".png"), null, foregroundGeometry));
                        }
                    }
                }

                for (int j = this.gm.DungeonLogic.Display[i].Length - 1; j >= this.gm.DungeonLogic.Display[i].Length / 2; j--)
                {
                    foregroundGeometry = new RectangleGeometry(new Rect(0, 0, this.gameWidth, this.gameHeight));
                    entity = this.gm.DungeonLogic.Display[i][j].Entity;
                    cell = this.gm.DungeonLogic.Display[i][j].Cell;
                    loot = this.gm.DungeonLogic.Display[i][j].Loot;

                    if (this.gm.DungeonLogic.Display[i][j].Cell != null)
                    {
                        drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Map/" + cell.Texture + @"/" + i + j + ".png"), null, foregroundGeometry));
                    }

                    if (this.gm.DungeonLogic.Display[i][j].Loot != null)
                    {
                        if (i == this.gm.DungeonLogic.Display.Length - 1)
                        {
                        }
                        else
                        {
                            foregroundGeometry = new RectangleGeometry(this.GeometryCalculator(j, i, this.gm.DungeonLogic.Display[i].Length));
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Loot/" + loot.Texture + @"/bag.png"), null, foregroundGeometry));
                        }
                    }

                    if (entity != null && entity is not Player)
                    {
                        string face = this.Dir(entity.Facing);
                        if (i == this.gm.DungeonLogic.Display.Length - 1)
                        {
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Entity/" + i + j + ".png"), null, foregroundGeometry));
                        }
                        else if (File.Exists(@"Images/Entity/" + entity.Texture + @"/" + face + ".png"))
                        {
                            foregroundGeometry = new RectangleGeometry(this.GeometryCalculator(j, i, this.gm.DungeonLogic.Display[i].Length));
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush(@"Entity/" + entity.Texture + @"/" + face + ".png"), null, foregroundGeometry));
                        }
                    }
                }
            }

            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("fog.png"), null, foregroundGeometry));
            return drawingGroup;
        }

        private Drawing GetBackground()
        {
            if (this.background == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.gameWidth, this.gameHeight));
                this.background = new GeometryDrawing(BrushHelper.GetBrush("background.png"), null, backgroundGeometry);
            }

            return this.background;
        }

        private Drawing GetPlayerStats()
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            Geometry geometry = new RectangleGeometry(new Rect(32, 32, this.gm.Player.MaxHealth * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("emptybar.png"), null, geometry));
            geometry = new RectangleGeometry(new Rect(32, 64, this.gm.Player.MaxStamina * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("emptybar.png"), null, geometry));
            geometry = new RectangleGeometry(new Rect(32, 96, this.gm.Player.MaxMana * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("emptybar.png"), null, geometry));

            geometry = new RectangleGeometry(new Rect(32, 32, this.gm.Player.Health * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("heathbar.png"), null, geometry));
            geometry = new RectangleGeometry(new Rect(32, 64, this.gm.Player.Stamina * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("staminabar.png"), null, geometry));
            geometry = new RectangleGeometry(new Rect(32, 96, this.gm.Player.Mana * 2, 32));
            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("manabar.png"), null, geometry));

            return drawingGroup;
        }

        private Drawing GetEnemyHealth()
        {
            DisplaySupport[][] ds = this.gm.DungeonLogic.Display;
            DrawingGroup drawingGroup = new DrawingGroup();
            for (int i = 0; i < ds.Length; i++)
            {
                for (int j = 0; j < ds[i].Length; j++)
                {
                    if (ds[i][j].Entity != null)
                    {
                        if (ds[i][j].Entity.Health > 0 && !(ds[i][j].Entity is Player))
                        {
                            Geometry geometry = new RectangleGeometry(new Rect(this.gameWidth - 700 + (i * 120), j * 24, ds[i][j].Entity.Health, 16));
                            drawingGroup.Children.Add(new GeometryDrawing(BrushHelper.GetBrush("heathbar.png"), null, geometry));
                        }
                    }
                }
            }

            return drawingGroup;
        }

        private Rect GeometryCalculator(double width, double height, double maxwidth)
        {
            double correction = 1;

            switch (height)
            {
                case 0:
                    correction = 0.75;
                    break;
                case 1:
                    correction = 1.65;
                    break;
                case 2:
                    correction = 1.45;
                    break;
                case 3:
                    correction = 1.15;
                    break;
                default:
                    break;
            }

            double diff = width - Math.Round(maxwidth / 2.0, MidpointRounding.ToNegativeInfinity);
            double size = (3 - height) * 1.6 / 10;
            double x = (this.gameWidth * (diff / maxwidth) * correction) + (this.gameWidth * size / 2);
            double y = (this.gameHeight * (height - 3) / 8.5) + (this.gameHeight * size);
            return new Rect(x, y, this.gameWidth * (1 - size), this.gameHeight * (1 - size));
        }

        private string Dir(Direction d)
        {
            switch (this.gm.Player.Facing)
            {
                case Direction.North:
                    switch (d)
                    {
                        case Direction.North:
                            return "b";
                        case Direction.East:
                            return "r";
                        case Direction.South:
                            return "f";
                        case Direction.West:
                            return "l";
                    }

                    break;

                case Direction.East:
                    switch (d)
                    {
                        case Direction.North:
                            return "l";
                        case Direction.East:
                            return "b";
                        case Direction.South:
                            return "r";
                        case Direction.West:
                            return "f";
                    }

                    break;

                case Direction.South:
                    switch (d)
                    {
                        case Direction.North:
                            return "f";
                        case Direction.East:
                            return "l";
                        case Direction.South:
                            return "b";
                        case Direction.West:
                            return "r";
                    }

                    break;

                case Direction.West:
                    switch (d)
                    {
                        case Direction.North:
                            return "r";
                        case Direction.East:
                            return "f";
                        case Direction.South:
                            return "l";
                        case Direction.West:
                            return "b";
                    }

                    break;
            }

            return "f";
        }
    }
}
