﻿// <copyright file="HighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="IHighScoreRepository"/> interface.
    /// </summary>
    public class HighScoreRepository
        : IHighScoreRepository
    {
        /// <inheritdoc/>
        public List<Leaderboard> ReadScores()
        {
            if (!File.Exists(@"Saves\Scores.xml"))
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Leaderboard>));
            TextReader reader = new StreamReader(@"Saves\Scores.xml");
            List<Leaderboard> leaderboard = (List<Leaderboard>)serializer.Deserialize(reader);
            reader.Dispose();

            if (leaderboard == null)
            {
                leaderboard = new List<Leaderboard>();
            }

            return leaderboard.OrderByDescending(p => p.Level).ToList();
        }

        /// <inheritdoc/>
        public void SaveScore(int level, string name)
        {
            List<Leaderboard> leaderboard = this.ReadScores();

            if (leaderboard == null)
            {
                leaderboard = new List<Leaderboard>();
            }

            if (name == null)
            {
                name = "Default";
            }

            Leaderboard player = leaderboard.Find(p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            if (player != null && player.Level < level)
            {
                player.Level = level;
            }
            else if (player == null)
            {
                leaderboard.Add(new Leaderboard() { Name = name, Level = level });
            }
            else
            {
                return;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Leaderboard>));
            TextWriter writer = new StreamWriter(@"Saves\Scores.xml");
            serializer.Serialize(writer, leaderboard);
            writer.Dispose();
        }
    }
}
