﻿// <copyright file="ResourceRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="IResourceRepository"/> interface.
    /// </summary>
    public class ResourceRepository : IResourceRepository
    {
        /// <inheritdoc/>
        public IList<Enemy> LoadEnemies()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Enemy>));
            TextReader reader = new StreamReader(@"Resources\enemy.xml");
            IList<Enemy> enemypool = (List<Enemy>)serializer.Deserialize(reader);
            reader.Close();

            return enemypool;
        }

        /// <inheritdoc/>
        public IList<IItem> LoadItems()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Weapon>));
            TextReader reader = new StreamReader(@"Resources\weapon.xml");
            IList<Weapon> weapons = (List<Weapon>)serializer.Deserialize(reader);
            reader.Dispose();

            serializer = new XmlSerializer(typeof(List<Armour>));
            reader = new StreamReader(@"Resources\armour.xml");
            IList<Armour> armours = (List<Armour>)serializer.Deserialize(reader);
            reader.Dispose();

            serializer = new XmlSerializer(typeof(List<Consumable>));
            reader = new StreamReader(@"Resources\consumable.xml");
            IList<Consumable> consumables = (List<Consumable>)serializer.Deserialize(reader);
            reader.Close();

            List<IItem> itempool = new List<IItem>();
            itempool.AddRange(weapons);
            itempool.AddRange(armours);
            itempool.AddRange(consumables);

            return itempool;
        }

        /// <inheritdoc/>
        public IList<IRoomModel> LoadRoom()
        {
            IList<IRoomModel> roompool = new List<IRoomModel>();
            int roomSize = 13;
            using (var sr = new StreamReader(@"Resources\rooms.txt"))
            {
                while (!sr.EndOfStream)
                {
                    RoomModel rm = new RoomModel();
                    string line = sr.ReadLine();
                    rm.North = line[0].Equals('1');
                    rm.East = line[1].Equals('1');
                    rm.South = line[2].Equals('1');
                    rm.West = line[3].Equals('1');
                    rm.Spawn = line[4].Equals('1');

                    for (int i = 0; i < roomSize; i++)
                    {
                        line = sr.ReadLine();
                        for (int j = 0; j < roomSize; j++)
                        {
                            if (line[j] == 'w')
                            {
                                rm.Room[i][j] = new Cell(CellType.Wall);
                            }
                            else if (line[j] == 't')
                            {
                                rm.Room[i][j] = new Cell("t", true, false);
                            }
                            else
                            {
                                rm.Room[i][j] = new Cell(CellType.Floor);
                            }
                        }
                    }

                    roompool.Add(rm);
                }
            }

            return roompool;
        }
    }
}
