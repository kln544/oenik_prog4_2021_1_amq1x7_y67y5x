﻿// <copyright file="SavedGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="ISavedGameRepository"/> interface.
    /// </summary>
    public class SavedGameRepository
        : ISavedGameRepository
    {
        /// <inheritdoc/>
        public void SaveEntities(IList<IEntity> entities)
        {
            if (entities is null)
            {
                throw new ArgumentNullException(nameof(entities));
            }

            XmlSerializer serializer = new XmlSerializer(typeof(Player));
            TextWriter writer = new StreamWriter(@"Saves\Player.xml");
            Player player = (Player)entities.ToList().Find(p => p is Player);
            serializer.Serialize(writer, player);
            writer.Dispose();

            serializer = new XmlSerializer(typeof(List<Enemy>));
            writer = new StreamWriter(@"Saves\Enemy.xml");
            IList<Enemy> enemies = new List<Enemy>();
            foreach (IEntity entity in entities)
            {
                if (entity is Enemy && entity.Health > 0)
                {
                    enemies.Add((Enemy)entity);
                }
            }

            serializer.Serialize(writer, enemies);
            writer.Close();
        }

        /// <inheritdoc/>
        public List<IEntity> LoadEntities()
        {
            if (!File.Exists(@"Saves\Player.xml"))
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(Player));
            TextReader reader = new StreamReader(@"Saves\Player.xml");

            Player player = (Player)serializer.Deserialize(reader);
            reader.Dispose();

            if (!File.Exists(@"Saves\Enemy.xml"))
            {
                return null;
            }

            serializer = new XmlSerializer(typeof(List<Enemy>));
            reader = new StreamReader(@"Saves\Enemy.xml");
            List<Enemy> enemies = (List<Enemy>)serializer.Deserialize(reader);
            reader.Close();

            List<IEntity> entities = new List<IEntity>();
            entities.Add(player);
            entities.AddRange(enemies);

            return entities;
        }

        /// <inheritdoc/>
        public void SaveMap(ICell[][] map, int lvl)
        {
            if (map is null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            using (StreamWriter sw = new StreamWriter(@"Saves\Map.txt"))
            {
                sw.WriteLine(lvl.ToString(CultureInfo.CurrentCulture));

                for (int i = 0; i < map.Length; i++)
                {
                    string line = string.Empty;
                    for (int j = 0; j < map[i].Length; j++)
                    {
                        if (map[i][j] != null)
                        {
                            line += map[i][j].Texture;
                        }
                        else
                        {
                            line += ' ';
                        }
                    }

                    sw.WriteLine(line);
                }
            }
        }

        /// <inheritdoc/>
        public Cell[][] LoadMap(out int lvl)
        {
            if (!File.Exists(@"Saves\Map.txt"))
            {
                lvl = 1;
                return null;
            }

            int mapSize = 390;
            Cell[][] map = new Cell[mapSize][];
            using (var sr = new StreamReader(@"Saves\Map.txt"))
            {
                string stringlvl = sr.ReadLine();

                if (!int.TryParse(stringlvl, out lvl))
                {
                    lvl = 1;
                }

                for (int i = 0; i < mapSize; i++)
                {
                    map[i] = new Cell[mapSize];
                    string line = sr.ReadLine();
                    if (line == null)
                    {
                        return null;
                    }

                    for (int j = 0; j < mapSize; j++)
                    {
                        if (line[j] == 'w')
                        {
                            map[i][j] = new Cell(CellType.Wall);
                        }
                        else if (line[j] == 'e')
                        {
                            map[i][j] = new Cell("e", true, true);
                        }
                        else if (line[j] == 't')
                        {
                            map[i][j] = new Cell("t", true, false);
                        }
                        else
                        {
                            map[i][j] = new Cell(CellType.Floor);
                        }
                    }
                }
            }

            return map;
        }

        /// <inheritdoc/>
        public void SaveLoot(IList<Loot> loot)
        {
            if (loot is null)
            {
                throw new ArgumentNullException(nameof(loot));
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Loot>));
            TextWriter writer = new StreamWriter(@"Saves\Loot.xml");
            serializer.Serialize(writer, loot);
            writer.Dispose();
        }

        /// <inheritdoc/>
        public IList<Loot> LoadLoot()
        {
            IList<Loot> loot;

            if (!File.Exists(@"Saves\Loot.xml"))
            {
                loot = new List<Loot>();
                return loot;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<Loot>));
            TextReader reader = new StreamReader(@"Saves\Loot.xml");

            loot = (List<Loot>)serializer.Deserialize(reader);
            reader.Dispose();

            return loot;
        }
    }
}
