﻿// <copyright file="IHighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Repository for the high score.
    /// </summary>
    public interface IHighScoreRepository
    {
        /// <summary>
        /// Read the leaderboard from an existing file.
        /// </summary>
        /// <returns>Score list.</returns>
        public List<Leaderboard> ReadScores();

        /// <summary>
        /// Saves the new score.
        /// </summary>
        /// <param name="level">The level reached.</param>
        /// <param name="name">The player's name.</param>
        public void SaveScore(int level, string name);
    }
}
