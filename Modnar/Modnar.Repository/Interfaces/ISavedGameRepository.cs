﻿// <copyright file="ISavedGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface for the saved game repository.
    /// </summary>
    public interface ISavedGameRepository
    {
        /// <summary>
        /// Loads saved game.
        /// </summary>
        /// <returns>Game data as string.</returns>
        public List<IEntity> LoadEntities();

        /// <summary>
        /// Saves current game.
        /// </summary>
        /// <param name="entities">The entities to be saved.</param>
        public void SaveEntities(IList<IEntity> entities);

        /// <summary>
        /// Loads saved map.
        /// </summary>
        /// <param name="lvl">The level of the dungeon.</param>
        /// <returns>Map of cells.</returns>
        public Cell[][] LoadMap(out int lvl);

        /// <summary>
        /// Saves current map.
        /// </summary>
        /// <param name="map">Current map.</param>
        /// <param name="lvl">The level of the dungeon.</param>
        public void SaveMap(ICell[][] map, int lvl);

        /// <summary>
        /// Loads saved loot on the map.
        /// </summary>
        /// <returns>The loot.</returns>
        public IList<Loot> LoadLoot();

        /// <summary>
        /// Saves the loot on the map.
        /// </summary>
        /// <param name="loot">Loot to be saved.</param>
        public void SaveLoot(IList<Loot> loot);
    }
}
