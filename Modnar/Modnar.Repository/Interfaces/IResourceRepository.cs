﻿// <copyright file="IResourceRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface for the rescource repository.
    /// </summary>
    public interface IResourceRepository
    {
        /// <summary>
        /// Load the entities.
        /// </summary>
        /// <returns>List of all the enemies.</returns>
        public IList<Enemy> LoadEnemies();

        /// <summary>
        /// Load all weapons.
        /// </summary>
        /// <returns>List of weapons.</returns>
        public IList<IItem> LoadItems();

        /// <summary>
        /// Load all the possible rooms.
        /// </summary>
        /// <returns>List of possible rooms.</returns>
        public IList<IRoomModel> LoadRoom();
    }
}
