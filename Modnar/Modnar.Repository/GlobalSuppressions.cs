﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA5369:Use XmlReader For Deserialize", Justification = "These files are safe.", Scope = "module")]
[assembly: SuppressMessage("Security", "CA5369:Use XmlReader For Deserialize", Justification = "These files are safe.", Scope = "module")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "For xml serialization we cannot use IList", Scope = "module")]