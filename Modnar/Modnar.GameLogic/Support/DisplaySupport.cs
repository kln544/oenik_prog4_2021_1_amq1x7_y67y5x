﻿// <copyright file="DisplaySupport.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Support class for FOV display.
    /// </summary>
    public class DisplaySupport
    {
        private IEntity entity;
        private ICell cell;
        private ILoot loot;

        /// <summary>
        /// Gets or sets an entity in FOV.
        /// </summary>
        public IEntity Entity { get => this.entity; set => this.entity = value; }

        /// <summary>
        /// Gets or sets a cell in FOV.
        /// </summary>
        public ICell Cell { get => this.cell; set => this.cell = value; }

        /// <summary>
        /// Gets or sets a loot in FOV.
        /// </summary>
        public ILoot Loot { get => this.loot; set => this.loot = value; }
    }
}
