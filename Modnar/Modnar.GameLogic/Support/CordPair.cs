﻿// <copyright file="CordPair.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Support class for AiLogic.
    /// </summary>
    public class CordPair
    {
        private int x;

        private int y;

        /// <summary>
        /// Initializes a new instance of the <see cref="CordPair"/> class.
        /// </summary>
        public CordPair()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CordPair"/> class.
        /// </summary>
        /// <param name="x">The x (horizontal) coordinate.</param>
        /// <param name="y">The y (vertical) coordinate.</param>
        public CordPair(int x, int y)
        {
            this.x = x;

            this.y = y;
        }

        /// <summary>
        /// Gets or sets an x (horizontal) coordinate.
        /// </summary>
        public int X { get => this.x; set => this.x = value; }

        /// <summary>
        /// Gets or sets an y (veritcal) coordinate.
        /// </summary>
        public int Y { get => this.y; set => this.y = value; }
    }
}
