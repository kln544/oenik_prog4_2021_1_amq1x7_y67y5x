﻿// <copyright file="SoundEffectEventArgs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Event arguments for soundeffect events.
    /// </summary>
    public class SoundEffectEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoundEffectEventArgs"/> class.
        /// </summary>
        /// <param name="effect">The sound effect.</param>
        public SoundEffectEventArgs(string effect)
        {
            this.Effect = effect;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoundEffectEventArgs"/> class.
        /// </summary>
        public SoundEffectEventArgs()
        {
        }

        /// <summary>
        /// Gets or sets the sound effect that must be player.
        /// </summary>
        public string Effect { get; set; }
    }
}
