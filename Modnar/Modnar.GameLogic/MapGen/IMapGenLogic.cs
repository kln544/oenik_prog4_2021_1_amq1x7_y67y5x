﻿// <copyright file="IMapGenLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface defining map generation.
    /// </summary>
    public interface IMapGenLogic
    {
        /// <summary>
        /// Starts a new map generation and ensures a map is properly created.
        /// </summary>
        /// <param name="lvl">The current level to be generated.</param>
        /// <param name="x">The x (horizontal) coordinate.</param>
        /// <param name="y">The y (vertical) coordinate.</param>
        public void GenInit(int lvl, out int x, out int y);
    }
}
