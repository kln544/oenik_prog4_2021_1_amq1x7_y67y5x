﻿// <copyright file="MapGenLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="IMapGenLogic"/> interface.
    /// </summary>
    public class MapGenLogic : IMapGenLogic
    {
        private const int N = 6; // It is actually the distance from the middle of the room, not the actuall widht and height.

        private static Random rndm;

        private ICell[][] map;

        private IList<IEntity> entities;

        private IList<IRoomModel> roomlist;

        private IList<Enemy> enemylist;

        private RoomNode furthest;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapGenLogic"/> class.
        /// </summary>
        /// <param name="roomlist">List of all existing room elements.</param>
        /// <param name="enemylist">List of all enemies.</param>
        /// <param name="map">The base map to be generated.</param>
        /// <param name="entities">The base entity list to be filled.</param>
        /// <param name="seed">Optional map seed.</param>
        public MapGenLogic(IList<IRoomModel> roomlist, IList<Enemy> enemylist, ref ICell[][] map, ref IList<IEntity> entities, string seed = null)
        {
            this.map = map;

            this.entities = entities;

            this.enemylist = enemylist;

            this.roomlist = roomlist;

            if (seed == null)
            {
                seed = DateTime.Now.ToString();
            }

            MapGenLogic.rndm = new Random(seed.GetHashCode(StringComparison.Ordinal));
        }

        /// <inheritdoc/>
        public void GenInit(int lvl, out int x, out int y)
        {
            int cntr;

            IList<RoomNode> nodes;

            this.furthest = null;

            do
            {
                x = MapGenLogic.rndm.Next(this.map.Length / 3, this.map.Length * 2 / 3);

                y = MapGenLogic.rndm.Next(this.map.Length / 3, this.map.Length * 2 / 3);

                cntr = (2 * lvl) + 5;
                if (cntr > 37)
                {
                    cntr = 36;
                }

                for (int i = 0; i < this.map.Length; i++)
                {
                    for (int j = 0; j < this.map[i].Length; j++)
                    {
                        this.map[i][j] = new Cell(CellType.Wall);
                    }
                }

                nodes = new List<RoomNode>() { new RoomNode(x, y) };

                this.CreateMap(ref nodes, ref cntr, 0);

                this.GenerateMap(nodes);
            }
            while (this.map[y][x] == null);

            this.SpawnEnemies(nodes, lvl);
        }

        private static void AddNeighbors(ref IList<RoomNode> nodes, ref RoomNode node)
        {
            int x = node.X;

            int y = node.Y;

            RoomNode nnorth = nodes.ToList().Find(nd => nd.X == x && nd.Y == y - (2 * N));
            if (nnorth != null)
            {
                node.North = true;
                nnorth.South = true;
            }

            RoomNode neast = nodes.ToList().Find(nd => nd.X == x + (2 * N) && nd.Y == y);
            if (neast != null)
            {
                node.East = true;
                neast.West = true;
            }

            RoomNode nsouth = nodes.ToList().Find(nd => nd.X == x && nd.Y == y + (2 * N));
            if (nsouth != null)
            {
                node.South = true;
                nsouth.North = true;
            }

            RoomNode nwest = nodes.ToList().Find(nd => nd.X == x - (2 * N) && nd.Y == y);
            if (nwest != null)
            {
                node.West = true;
                nwest.East = true;
            }
        }

        private static bool ValidRoom(RoomNode node, RoomModel room)
        {
            if (node.North && !room.North)
            {
                return false;
            }

            if (node.East && !room.East)
            {
                return false;
            }

            if (node.South && !room.South)
            {
                return false;
            }

            if (node.West && !room.West)
            {
                return false;
            }

            return true;
        }

        private void CreateMap(ref IList<RoomNode> nodes, ref int roomcnt, int depth)
        {
            RoomNode node;

            RoomModel room;

            if (depth == 0)
            {
                if (nodes == null)
                {
                    nodes = new List<RoomNode>();
                }

                do
                {
                    room = (RoomModel)this.roomlist.ElementAt(MapGenLogic.rndm.Next(this.roomlist.Count));
                }
                while (room.Spawn != true);

                node = nodes.First();

                node.Room = room;

                node.Spawn = true;
            }
            else
            {
                do
                {
                    node = nodes.ElementAt(MapGenLogic.rndm.Next(nodes.Count));
                }
                while (node.Room != null);

                do
                {
                    room = (RoomModel)this.roomlist.ElementAt(MapGenLogic.rndm.Next(this.roomlist.Count));
                }
                while (!MapGenLogic.ValidRoom(node, room));

                node.Room = room;
            }

            MapGenLogic.AddNeighbors(ref nodes, ref node);

            this.CalcFurthest(node, nodes.First().X, nodes.First().Y);

            if (!node.North && room.North && this.ValidPlacement(node.X, node.Y, Direction.North) && nodes.ToList().Find(nd => nd.X == node.X && nd.Y == node.Y - (2 * N)) == null)
            {
                nodes.Add(new RoomNode(node.X, node.Y - (2 * N)) { South = true });
            }

            if (!node.East && room.East && this.ValidPlacement(node.X, node.Y, Direction.East) && nodes.ToList().Find(nd => nd.X == node.X + (2 * N) && nd.Y == node.Y) == null)
            {
                nodes.Add(new RoomNode(node.X + (2 * N), node.Y) { West = true });
            }

            if (!node.South && room.South && this.ValidPlacement(node.X, node.Y, Direction.South) && nodes.ToList().Find(nd => nd.X == node.X && nd.Y == node.Y + (2 * N)) == null)
            {
                nodes.Add(new RoomNode(node.X, node.Y + (2 * N)) { North = true });
            }

            if (!node.West && room.West && this.ValidPlacement(node.X, node.Y, Direction.West) && nodes.ToList().Find(nd => nd.X == node.X - (2 * N) && nd.Y == node.Y) == null)
            {
                nodes.Add(new RoomNode(node.X - (2 * N), node.Y) { East = true });
            }

            roomcnt--;

            if (roomcnt == 0)
            {
                return;
            }

            this.CreateMap(ref nodes, ref roomcnt, depth + 1);
        }

        private void CreateExit()
        {
            int mx = this.furthest.X - (N - 1);

            int my = this.furthest.Y - (N - 1);

            int x;

            int y;

            do
            {
                x = mx + rndm.Next(2 * N);

                y = my + rndm.Next(2 * N);

                if (this.map[y][x].Walkover)
                {
                    this.map[y][x] = new Cell("e", true, true);
                }
            }
            while (!this.map[y][x].Texture.Equals("e", StringComparison.OrdinalIgnoreCase));
        }

        private void GenerateMap(IList<RoomNode> nodes)
        {
            foreach (RoomNode node in nodes)
            {
                if (node.Room != null)
                {
                    for (int i = 0; i < node.Room.Room.Length; i++)
                    {
                        for (int j = 0; j < node.Room.Room[i].Length; j++)
                        {
                            this.map[node.Y - N + i][node.X - N + j] = node.Room.Room[i][j];
                        }
                    }
                }
            }

            this.CreateExit();
        }

        private void SpawnEnemies(IList<RoomNode> nodes, int lvl)
        {
            int cnt = 1 + (int)Math.Round(Math.Sqrt(lvl), MidpointRounding.ToEven);
            if (cnt > 4)
            {
                cnt = 4;
            }

            int diff = (int)Math.Round(Math.Sqrt(400 * lvl) + (50 * lvl), MidpointRounding.ToPositiveInfinity);
            if (diff > 500)
            {
                diff = 500;
            }

            IList<IEnemy> tempenemies;

            int sx;

            int sy;

            foreach (RoomNode node in nodes)
            {
                if (node.Room != null && node.Spawn == false)
                {
                    int sum = 0;

                    tempenemies = new List<IEnemy>();

                    do
                    {
                        tempenemies.Clear();

                        IEnemy tempentity;

                        sum = 0;

                        int rand;

                        do
                        {
                            rand = MapGenLogic.rndm.Next(this.enemylist.Count);
                            tempentity = this.enemylist.ElementAt(rand);
                            tempenemies.Add(tempentity);
                            sum += tempentity.Difficulty;
                        }
                        while (tempenemies.Count < cnt);
                    }
                    while (sum > diff);

                    foreach (IEnemy e in tempenemies)
                    {
                        do
                        {
                            sx = MapGenLogic.rndm.Next(node.X - N, node.X + (N + 1));

                            sy = MapGenLogic.rndm.Next(node.Y - N, node.Y + (N + 1));
                        }
                        while (this.map[sy][sx] == null || !this.map[sy][sx].Walkover || this.entities.ToList().Find(enemy => enemy.X == sx && enemy.Y == sy) != null);

                        IEnemy enemy = e.DeepClone();

                        enemy.X = sx;

                        enemy.Y = sy;

                        this.entities.Add(enemy);
                    }
                }
            }
        }

        private bool ValidPlacement(int x, int y, Direction dir)
        {
            switch (dir)
            {
                case Direction.North:
                    if (y - (3 * N) - 1 >= 0)
                    {
                        return true;
                    }

                    break;
                case Direction.East:
                    if (x + (3 * N) + 1 < this.map[y].Length)
                    {
                        return true;
                    }

                    break;
                case Direction.South:
                    if (y + (3 * N) + 1 < this.map.Length)
                    {
                        return true;
                    }

                    break;
                case Direction.West:
                    if (x - (3 * N) - 1 >= 0)
                    {
                        return true;
                    }

                    break;
                default:
                    break;
            }

            return false;
        }

        private void CalcFurthest(RoomNode node, int x, int y)
        {
            if (this.furthest == null)
            {
                this.furthest = node;
                return;
            }

            double current = Math.Sqrt(Math.Pow(x - this.furthest.X, 2) + Math.Pow(y - this.furthest.Y, 2));

            double newnode = Math.Sqrt(Math.Pow(x - node.X, 2) + Math.Pow(y - node.Y, 2));

            if (newnode > current)
            {
                this.furthest = node;
            }
        }
    }
}
