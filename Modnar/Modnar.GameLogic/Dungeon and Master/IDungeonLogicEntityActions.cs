﻿// <copyright file="IDungeonLogicEntityActions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Logic interface for the playable map. Defines fields and methods that only return data.
    /// </summary>
    public interface IDungeonLogicEntityActions
    {
        /// <summary>
        /// Gets the current round.
        /// </summary>
        public int Round { get; }

        /// <summary>
        /// Determines whether the entity can move to this cell.
        /// </summary>
        /// <param name="x">Contains the x (horizontal) coordinate.</param>
        /// <param name="y">Contains the y (vertical) coordinate.</param>
        /// /// <param name="canfly">Whether the entity can fly.</param>
        /// <returns>Whether it is a valid move.</returns>
        public bool ValidMove(int x, int y, bool? canfly = null);

        /// <summary>
        /// Interaction with the elements of the game.
        /// </summary>
        /// <param name="player">Player as an input, as only the player can interact with things.</param>
        /// <returns>Whether any interaction was successful.</returns>
        public bool Interact(IPlayer player);

        /// <summary>
        /// Checks a range of cells to find an entity.
        /// </summary>
        /// <param name="x">Contains the x (horizontal) coordinate to start the search from.</param>
        /// <param name="y">Contains the y (vertical) coordinate to start the search from.</param>
        /// <param name="facing">The direction the searching entity is facing.</param>
        /// <param name="range">The searching entity's range.</param>
        /// <param name="type">Optional to return a specific type of entity.</param>
        /// <returns>An entityLogic in range or null.</returns>
        public IEntityLogic CheckRange(int x, int y, Direction facing, int range, Type type = null);

        /// <summary>
        /// Checks a cone shaped field for targets in front of the entity.
        /// </summary>
        /// <param name="x">Contains the x (horizontal)coordinate of the entity.</param>
        /// <param name="y">Contains the y (vertical) coordinate of the entity.</param>
        /// <param name="dir">The direction the entity is facing.</param>
        /// <param name="range">How far the entity can see.</param>
        /// <returns>An entityLogic or null.</returns>
        public IEntityLogic EntityFOV(int x, int y, Direction dir, int range);

        /// <summary>
        /// Updates an array with the entities and cells in front of the player in a cone shape.
        /// </summary>
        public void PlayerFOV();

        /// <summary>
        /// Ends a turn.
        /// </summary>
        /// <returns>Whether it was successful.</returns>
        public bool EndTurn();
    }
}
