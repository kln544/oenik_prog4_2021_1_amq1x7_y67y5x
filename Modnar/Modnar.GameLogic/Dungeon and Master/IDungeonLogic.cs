﻿// <copyright file="IDungeonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Logic interface for the playable map.
    /// </summary>
    public interface IDungeonLogic : IDungeonLogicEntityActions
    {
        /// <summary>
        /// Event when the exit is reached.
        /// </summary>
        public event EventHandler<EventArgs> ExitReached;

        /// <summary>
        /// Event when the FOV was updated.
        /// </summary>
        public event EventHandler<EventArgs> FOVUpdated;

        /// <summary>
        /// Event when the player died.
        /// </summary>
        public event EventHandler<EventArgs> PlayerDied;

        /// <summary>
        /// Event when soundeffect must be played.
        /// </summary>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <summary>
        /// Gets the game map.
        /// </summary>
        public ICell[][] Map { get; }

        /// <summary>
        /// Gets the list of enemies of entity models.
        /// </summary>
        public IList<IEntityLogic> Entities { get; }

        /// <summary>
        /// Gets the list of the loot on the map.
        /// </summary>
        public IList<Loot> Lootlist { get; }

        /// <summary>
        /// Gets the array containing the entities and cells in field of view.
        /// </summary>
        public DisplaySupport[][] Display { get; }

        /// <summary>
        /// Gets the player logic for the wpf layer.
        /// </summary>
        public PlayerLogic Player { get; }

        /// <summary>
        /// Gets the skills for the player.
        /// </summary>
        public SkillLogic Skills { get; }

        /// <summary>
        /// Adding an enemy into the game.
        /// </summary>
        /// <param name="entity">The entity to be added to the map.</param>
        /// <returns>Returns whether the add operation was successful.</returns>
        public bool AddEntity(IEntity entity);

        /// <summary>
        /// Adding loot to the game.
        /// </summary>
        /// <param name="score">The enemy's score.</param>
        /// <param name="x">The x (horizontal) coordinate of where the loot will spawn.</param>
        /// <param name="y">The y (vertical) coordinate of where the loot will spawn.</param>
        /// <returns>Whether it was successful.</returns>
        public bool AddLoot(int score, int x, int y);
    }
}
