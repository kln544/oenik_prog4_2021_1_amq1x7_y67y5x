﻿// <copyright file="DungeonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using static Modnar.GameLogic.IDungeonLogic;

    /// <summary>
    /// Implementation of <see cref="IDungeonLogic"/> interface.
    /// </summary>
    public class DungeonLogic : IDungeonLogic
    {
        private ICell[][] map;

        private IList<IEntityLogic> entities;

        private IList<Loot> maploot;

        private IList<IItem> itempool;

        private DisplaySupport[][] display;

        private PlayerLogic player;

        private SkillLogic skills;

        private int round;

        /// <summary>
        /// Initializes a new instance of the <see cref="DungeonLogic"/> class.
        /// </summary>
        /// <param name="map">A generated or loaded map.</param>
        /// <param name="entities">A generated or loaded list of entities.</param>
        /// <param name="maploot">A generated or loaded list of the loot on the map.</param>
        /// <param name="itempool">A list of all the items of the game.</param>
        public DungeonLogic(ICell[][] map, IList<IEntity> entities, IList<Loot> maploot, IList<IItem> itempool)
        {
            this.map = map;

            this.entities = new List<IEntityLogic>();

            this.itempool = itempool;

            this.maploot = maploot;

            if (entities != null)
            {
                foreach (IEntity entity in entities)
                {
                    this.AddEntity(entity);
                }
            }

            this.skills = new SkillLogic(this.player);

            this.PlayerFOV();
        }

        /// <inheritdoc/>
        public event EventHandler<EventArgs> ExitReached;

        /// <inheritdoc/>
        public event EventHandler<EventArgs> FOVUpdated;

        /// <inheritdoc/>
        public event EventHandler<EventArgs> PlayerDied;

        /// <inheritdoc/>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <inheritdoc/>
        public ICell[][] Map { get => this.map; }

        /// <inheritdoc/>
        public IList<IEntityLogic> Entities { get => this.entities; }

        /// <inheritdoc/>
        public IList<Loot> Lootlist { get => this.maploot; }

        /// <inheritdoc/>
        public DisplaySupport[][] Display
        {
            get => this.display; set { this.display = value; }
        }

        /// <inheritdoc/>
        public PlayerLogic Player { get => this.player; }

        /// <inheritdoc/>
        public SkillLogic Skills { get => this.skills; }

        /// <inheritdoc/>
        public int Round { get => this.round; }

        /// <inheritdoc/>
        public bool AddEntity(IEntity entity)
        {
            if (entity != null && this.ValidMove(entity.X, entity.Y, entity.CanFly))
            {
                if (entity is Player)
                {// Should be only one case at all time
                    PlayerLogic plogic = new PlayerLogic((Player)entity, this);

                    this.entities.Add(plogic);

                    plogic.EntityDied += this.OnEntityDied;

                    plogic.SoundEffect += this.OnSoundEffect;

                    this.player = plogic;
                }
                else if (entity is Enemy)
                {
                    EnemyLogic elogic = new EnemyLogic((Enemy)entity, this);

                    this.entities.Add(elogic);

                    elogic.EntityDied += this.OnEntityDied;

                    elogic.SoundEffect += this.OnSoundEffect;
                }

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool AddLoot(int score, int x, int y)
        {
            if (this.maploot == null)
            {
                return false;
            }

            Random rnd = new Random();

            Loot loot = new Loot(x, y, "bag");

            int times = DropAmount(score);

            int scorefloor = (int)Math.Round(score * 0.5, MidpointRounding.ToEven);

            int scoreroof = (int)Math.Round(score * 1.5, MidpointRounding.ToEven);

            int id = 0;

            for (int i = 0; i < times; i++)
            {
                id = rnd.Next(scorefloor, scoreroof);
                IItem roll = this.GetItem(id);

                switch (roll)
                {
                    case Weapon weapon:
                        loot.Weapons.Add((Weapon)weapon.DeepClone());
                        break;
                    case Armour armour:
                        loot.Armours.Add((Armour)armour.DeepClone());
                        break;
                    case Consumable consumable:
                        loot.Consumables.Add((Consumable)consumable.DeepClone());
                        break;
                    case null:
                        throw new ArgumentNullException(roll.ToString(), "Failed item roll.");
                    default:
                        break;
                }
            }

            this.maploot.Add(loot);

            return true;
        }

        /// <inheritdoc/>
        public bool ValidMove(int x, int y, bool? canfly = null)
        {
            if (x >= 0 && y >= 0 && y < this.map.Length && x < this.map[y].Length)
            {
                ICell cell = this.map[y][x];
                if (cell != null && (canfly == null ||
                    ((cell.Walkover || (cell.Flyover && canfly == true)) && this.entities.ToList().Find(e => e.Entity.X == x && e.Entity.Y == y) == null)))
                {
                    if (this.entities.ToList().Find(e => e.Entity.X == x && e.Entity.Y == y) == null || canfly == null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public IEntityLogic CheckRange(int x, int y, Direction facing, int range, Type type = null)
        {
            int diry = 0;

            int dirx = 0;

            switch (facing)
            {
                case Direction.North:
                    diry = -1;
                    break;
                case Direction.East:
                    dirx = 1;
                    break;
                case Direction.South:
                    diry = 1;
                    break;
                case Direction.West:
                    dirx = -1;
                    break;
                default:
                    break;
            }

            IEntityLogic e;

            for (int i = 0; i <= range; i++)
            {
                if (!this.map[y + (diry * i)][x + (dirx * i)].Flyover)
                {
                    return null;
                }

                e = this.entities.ToList().Find(e => e.Entity.X == x + (dirx * i) && e.Entity.Y == y + (diry * i));

                if (e != null && (type == null || e.GetType().Equals(type)))
                {
                    return e;
                }
            }

            return null;
        }

        /// <inheritdoc/>
        public bool Interact(IPlayer player)
        {
            if (player == null)
            {
                return false;
            }

            int x = player.X;

            int y = player.Y;

            switch (player.Facing)
            {
                case Direction.North:
                    y--;
                    break;
                case Direction.East:
                    x++;
                    break;
                case Direction.South:
                    y++;
                    break;
                case Direction.West:
                    x--;
                    break;
                default:
                    break;
            }

            Loot loot = this.maploot.ToList().Find(l => l.X == x && l.Y == y);

            if (loot != null)
            {
                player.Weaponry.AddRange(loot.Weapons);

                player.Armoury.AddRange(loot.Armours);

                foreach (Consumable item in loot.Consumables)
                {
                    Consumable stack = player.Inventory.Find(i => i.Name.Equals(item.Name, StringComparison.OrdinalIgnoreCase));
                    if (stack != null)
                    {
                        stack.Stack++;
                    }
                    else
                    {
                        player.Inventory.Add(item);
                    }
                }

                this.maploot.Remove(loot);

                return true;
            }
            else
            {
                if (this.map[y][x].Texture.Equals("e", StringComparison.OrdinalIgnoreCase))
                {
                    this.OnExitReached();
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public void PlayerFOV()
        {
            int vx = 0;

            int vy = 0;

            int nx = 0;

            int ny = 0;

            int px = this.player.Entity.X;

            int py = this.player.Entity.Y;

            int range = this.player.Entity.Sight;

            Direction dir = this.player.Entity.Facing;

            if (this.display == null)
            {
                this.display = new DisplaySupport[range + 1][];
            }

            DisplaySupport[] row;

            switch (dir)
            {
                case Direction.North:
                    vx = -1;
                    vy = -1;
                    nx = 1;
                    ny = -1;
                    break;
                case Direction.East:
                    vx = 1;
                    vy = -1;
                    nx = 1;
                    ny = 1;
                    break;
                case Direction.South:
                    vx = 1;
                    vy = 1;
                    nx = -1;
                    ny = 1;
                    break;
                case Direction.West:
                    vx = -1;
                    vy = 1;
                    nx = -1;
                    ny = -1;
                    break;
                default:
                    break;
            }

            int dis = 0;

            for (int i = 0; i <= range; i++)
            {
                if (i == range)
                {
                    dis = (int)Math.Round((double)(i / 2), MidpointRounding.ToEven);
                }
                else if (i == 0)
                {
                    dis = 1;
                }
                else
                {
                    dis = i;
                }

                row = new DisplaySupport[(2 * dis) + 1];

                int relx;

                int rely;

                for (int j = -dis; j <= dis; j++)
                {
                    if (dir == Direction.North || dir == Direction.South)
                    {
                        relx = j * nx;
                        rely = i * ny;
                    }
                    else
                    {
                        relx = i * nx;
                        rely = j * ny;
                    }

                    row[j + dis] = new DisplaySupport();

                    if (i == 0 || ((DungeonLogic.CWTest(true, vx, vy, relx, rely) && DungeonLogic.CWTest(false, nx, ny, relx, rely)) && DungeonLogic.InRadious(relx, rely, range) && this.ValidMove(px + relx, py + rely)))
                    {
                        row[j + dis].Cell = this.map[py + rely][px + relx];

                        row[j + dis].Entity = this.entities.ToList().Find(e => e.Entity.X == px + relx && e.Entity.Y == py + rely)?.Entity;

                        row[j + dis].Loot = this.maploot.ToList().Find(e => e.X == px + relx && e.Y == py + rely);
                    }
                }

                this.display[range - i] = row;
            }

            this.OnFOVUpdated();
        }

        /// <inheritdoc/>
        public IEntityLogic EntityFOV(int x, int y, Direction dir, int range)
        {
            int vx = 0;

            int vy = 0;

            int nx = 0;

            int ny = 0;

            switch (dir)
            {
                case Direction.North:
                    vx = -1;
                    vy = -1;
                    nx = 1;
                    ny = -1;
                    break;
                case Direction.East:
                    vx = 1;
                    vy = -1;
                    nx = 1;
                    ny = 1;
                    break;
                case Direction.South:
                    vx = 1;
                    vy = 1;
                    nx = -1;
                    ny = 1;
                    break;
                case Direction.West:
                    vx = -1;
                    vy = 1;
                    nx = -1;
                    ny = -1;
                    break;
                default:
                    break;
            }

            int dis = 0;

            for (int i = 1; i <= range; i++)
            {
                if (i == range)
                {
                    dis = (int)Math.Round((double)(i / 2), MidpointRounding.ToEven);
                }
                else
                {
                    dis = i;
                }

                int relx;

                int rely;

                for (int j = -dis; j <= dis; j++)
                {
                    if (dir == Direction.North || dir == Direction.South)
                    {
                        relx = j * nx;
                        rely = i * ny;
                    }
                    else
                    {
                        relx = i * nx;
                        rely = j * ny;
                    }

                    if (DungeonLogic.CWTest(true, vx, vy, relx, rely) && DungeonLogic.CWTest(false, nx, ny, relx, rely) && DungeonLogic.InRadious(relx, rely, range) && this.ValidMove(x + relx, y + rely))
                    {
                        IEntityLogic e = this.entities.ToList().Find(e => e.Entity.X == x + relx && e.Entity.Y == y + rely);

                        if (e != null && e is PlayerLogic && this.EntityLOS(x, y, e.Entity.X, e.Entity.Y))
                        {
                            return e;
                        }
                    }
                }
            }

            return null;
        }

        /// <inheritdoc/>
        public bool EndTurn()
        {
            bool ret = true;

            this.round++;

            foreach (EnemyLogic enemies in this.entities.ToList().Where(e => e is EnemyLogic))
            {
                if (!enemies.DoCurrentTurn())
                {
                    ret = false;
                }
            }

            this.PlayerFOV();

            return ret;
        }

        /// <summary>
        /// Method to throw an event on exit reached.
        /// </summary>
        protected virtual void OnExitReached()
        {
            if (this.ExitReached != null)
            {
                this.ExitReached(this, EventArgs.Empty);
                this.player = null;
            }
        }

        /// <summary>
        /// Method to throw an event on FOV update.
        /// </summary>
        protected virtual void OnFOVUpdated()
        {
            if (this.FOVUpdated != null)
            {
                this.FOVUpdated(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Method to throw an event on the player's death.
        /// </summary>
        protected virtual void OnPlayerDied()
        {
            if (this.PlayerDied != null)
            {
                this.PlayerDied(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Checks whether a vector is to the left or right of another.
        /// </summary>
        /// <param name="cw">Whether is it clockwise or versa.</param>
        /// <param name="x1">First vector's x coordinate.</param>
        /// <param name="y1">First vector's y coordinate.</param>
        /// <param name="x2">Second vector's x coordinate.</param>
        /// <param name="y2">Second vector's y coordinate.</param>
        /// <returns>Whether it is clockwise (to the right).</returns>
        private static bool CWTest(bool cw, int x1, int y1, int x2, int y2)
        {
            if (cw)
            {
                return (-x1 * y2) + (y1 * x2) <= 0;
            }

            return (-x1 * y2) + (y1 * x2) >= 0;
        }

        /// <summary>
        /// Checks whether a point is inside the given radious.
        /// </summary>
        /// <param name="x">The x (horizontal) coordinate.</param>
        /// <param name="y">The y (vertical) coordinate.</param>
        /// <param name="r">The radious.</param>
        /// <returns>Whether it was inside the radious.</returns>
        private static bool InRadious(int x, int y, int r)
        {
            return Math.Round(Math.Sqrt((x * x) + (y * y)), MidpointRounding.ToEven) <= r;
        }

        private static int DropAmount(int score)
        {
            switch (score)
            {
                case < 50:
                    return 1;
                case < 100:
                    return 2;
                case < 200:
                    return 3;
                case < 300:
                    return 2;
                case > 300:
                    return 1;
                default:
                    break;
            }

            return 0;
        }

        /// <summary>
        /// Draws a line between two points deciding whether they can see each other.
        /// </summary>
        /// <param name="x1">The x (horizontal) coordinate of the first point.</param>
        /// <param name="y1">The y (vertical) coordinate of the first point.</param>
        /// <param name="x2">The x (vertical) coordinate of the second point.</param>
        /// <param name="y2">The y (vertical) coordinate of the second point.</param>
        /// <returns>Whether there is a clear line of sight between the two points.</returns>
        private bool EntityLOS(int x1, int y1, int x2, int y2)
        {
            int w = x2 - x1;

            int h = y2 - y1;

            int dx1 = 0;

            int dy1 = 0;

            int dx2 = 0;

            int dy2 = 0;

            if (w < 0)
            {
                dx1 = -1;
                dx2 = -1;
            }
            else if (w > 0)
            {
                dx1 = 1;
                dx2 = 1;
            }

            if (h < 0)
            {
                dy1 = -1;
            }
            else if (h > 0)
            {
                dy1 = 1;
            }

            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);

            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);

                if (h < 0)
                {
                    dy2 = -1;
                }
                else if (h > 0)
                {
                    dy2 = 1;
                }

                dx2 = 0;
            }

            int num = longest >> 1;

            for (int i = 0; i < longest; i++)
            {
                num += shortest;

                if (!(num < longest))
                {
                    num -= longest;
                    x1 += dx1;
                    y1 += dy1;
                }
                else
                {
                    x1 += dx2;
                    y1 += dy2;
                }

                if (x1 == x2 && y1 == y2)
                {
                    return true;
                }
                else if (!this.ValidMove(x1, y1, true))
                {
                    return false;
                }
            }

            return true;
        }

        private IItem GetItem(int roll)
        {
            Rarity rarity = Rarity.Common;

            switch (roll)
            {
                case < 100:
                    rarity = Rarity.Common;
                    break;
                case < 200:
                    rarity = Rarity.Uncommon;
                    break;
                case < 300:
                    rarity = Rarity.Rare;
                    break;
                case < 400:
                    rarity = Rarity.Epic;
                    break;
                case < 500:
                    rarity = Rarity.Legendary;
                    break;
                case > 500:
                    rarity = Rarity.Relic;
                    break;
                default:
                    break;
            }

            Random r = new Random();

            int rollt = r.Next(18);

            Type type = typeof(Consumable);

            switch (rollt)
            {
                case < 15:
                    break;
                case < 17:
                    type = typeof(Weapon);
                    break;
                case < 18:
                    type = typeof(Armour);
                    break;
                default:
                    break;
            }

            IList<IItem> selected = this.itempool.Where(item => item.Rarity == rarity && item.GetType().Equals(type)).ToList();

            return selected.ElementAt(r.Next(selected.Count));
        }

        private void OnEntityDied(object source, EventArgs arg)
        {
            if (source is EnemyLogic elogic)
            {
                Enemy enemy = (Enemy)elogic.Entity;
                this.AddLoot(enemy.Difficulty, enemy.X, enemy.Y);
                this.entities.Remove(elogic);
            }
            else if (source is PlayerLogic plogic)
            {
                this.OnPlayerDied();
            }
        }

        private void OnSoundEffect(object source, SoundEffectEventArgs arg)
        {
            if (this.SoundEffect != null)
            {
                this.SoundEffect(source, arg);
            }
        }
    }
}
