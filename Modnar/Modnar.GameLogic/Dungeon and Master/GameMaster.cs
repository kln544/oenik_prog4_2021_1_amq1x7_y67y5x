﻿// <copyright file="GameMaster.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using Modnar.Repository;
    using static Modnar.GameLogic.IGameMaster;

    /// <summary>
    /// Implementation of <see cref="IGameMaster"/> interface.
    /// </summary>
    public class GameMaster : IGameMaster
    {
        private IDungeonLogic dungeonLogic;
        private Player player;
        private ICell[][] map;
        private IList<IEntity> entities;
        private IList<Loot> maploot;

        private ISavedGameRepository savedGameRepository;
        private IHighScoreRepository highScoreRepository;

        private MapGenLogic mapGen;

        private IList<IRoomModel> roompool;
        private IList<Enemy> enemypool;
        private IList<IItem> itempool;

        private int level;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameMaster"/> class.
        /// </summary>
        /// <param name="resourceRepository">Resource repository.</param>
        /// <param name="highScoreRepository">HighScore repository.</param>
        /// <param name="savedGameRepository">Saved game repository.</param>
        public GameMaster(IResourceRepository resourceRepository, IHighScoreRepository highScoreRepository, ISavedGameRepository savedGameRepository)
        {
            if (resourceRepository is null)
            {
                throw new ArgumentNullException(nameof(resourceRepository));
            }

            this.savedGameRepository = savedGameRepository ?? throw new ArgumentNullException(nameof(savedGameRepository));

            this.highScoreRepository = highScoreRepository ?? throw new ArgumentNullException(nameof(highScoreRepository));

            this.entities = new List<IEntity>();

            this.maploot = new List<Loot>();

            this.roompool = resourceRepository.LoadRoom();

            this.enemypool = resourceRepository.LoadEnemies();

            this.itempool = resourceRepository.LoadItems();
        }

        /// <inheritdoc/>
        public event EventHandler<EventArgs> FOVUpdated;

        /// <inheritdoc/>
        public event EventHandler<EventArgs> PlayerDied;

        /// <inheritdoc/>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <inheritdoc/>
        public IDungeonLogic DungeonLogic { get => this.dungeonLogic; set => this.dungeonLogic = value; }

        /// <inheritdoc/>
        public Player Player { get => this.player; set => this.player = value; }

        /// <inheritdoc/>
        public int Level { get => this.level; }

        /// <inheritdoc/>
        public void SaveGame()
        {
            this.savedGameRepository.SaveEntities(this.entities);
            this.savedGameRepository.SaveMap(this.DungeonLogic.Map, this.level);
            this.savedGameRepository.SaveLoot(this.maploot.ToList());
        }

        /// <inheritdoc/>
        public void LoadGame()
        {
            if (File.Exists(@"Saves\Player.xml") && File.Exists(@"Saves\Enemy.xml") && File.Exists(@"Saves\Map.txt"))
            {
                if (this.dungeonLogic != null)
                {
                    this.dungeonLogic.FOVUpdated -= this.OnFOVUpdated;
                    this.dungeonLogic.ExitReached -= this.OnExitReached;
                    this.dungeonLogic.PlayerDied -= this.OnPlayerDied;
                    this.dungeonLogic.SoundEffect -= this.OnSoundEffect;
                }

                this.entities.Clear();
                this.maploot.Clear();
                this.map = this.savedGameRepository.LoadMap(out this.level);
                this.entities = this.savedGameRepository.LoadEntities();
                this.maploot = this.savedGameRepository.LoadLoot();
                if (this.map == null || !this.map.Any() || this.entities == null || !this.entities.Any())
                {
                    this.CreatePlayer("Nameless", 100, 100, 100, 10, 10, 10, "deprived");
                    this.StartNewGame();
                }

                this.player = (Player)this.entities.ToList()?.Find(p => p is Player);

                this.dungeonLogic = null;
                this.dungeonLogic = new DungeonLogic(this.map, this.entities, this.maploot, this.itempool);

                this.dungeonLogic.FOVUpdated += this.OnFOVUpdated;
                this.dungeonLogic.ExitReached += this.OnExitReached;
                this.dungeonLogic.PlayerDied += this.OnPlayerDied;
                this.dungeonLogic.SoundEffect += this.OnSoundEffect;

                this.mapGen = new MapGenLogic(this.roompool, this.enemypool, ref this.map, ref this.entities);

                this.dungeonLogic.PlayerFOV();
            }
            else
            {
                this.CreatePlayer("Nameless", 100, 100, 100, 10, 10, 10, "deprived");
                this.StartNewGame();
            }
        }

        /// <inheritdoc/>
        public void GenNewMap()
        {
            if (this.player == null)
            {
                this.CreatePlayer("Nameless", 100, 100, 100, 10, 10, 10, "deprived");
            }

            if (this.dungeonLogic != null)
            {
                this.dungeonLogic.FOVUpdated -= this.OnFOVUpdated;
                this.dungeonLogic.ExitReached -= this.OnExitReached;
                this.dungeonLogic.PlayerDied -= this.OnPlayerDied;
            }

            int x;
            int y;

            this.level++;
            this.mapGen.GenInit(this.level, out x, out y);

            this.player.X = x;
            this.player.Y = y;
            this.entities.Add(this.Player);

            this.dungeonLogic = null;
            this.dungeonLogic = new DungeonLogic(this.map, this.entities, this.maploot, this.itempool);

            this.dungeonLogic.FOVUpdated += this.OnFOVUpdated;
            this.dungeonLogic.ExitReached += this.OnExitReached;
            this.dungeonLogic.PlayerDied += this.OnPlayerDied;
            this.dungeonLogic.SoundEffect += this.OnSoundEffect;
        }

        /// <inheritdoc/>
        public void StartNewGame()
        {
            this.map = new ICell[441][];

            for (int i = 0; i < this.map.Length; i++)
            {
                this.map[i] = new ICell[441];
            }

            this.maploot.Clear();
            this.entities.Clear();

            this.mapGen = new MapGenLogic(this.roompool, this.enemypool, ref this.map, ref this.entities);

            this.level = default;
            this.GenNewMap();
        }

        /// <inheritdoc/>
        public List<Leaderboard> ReadHighScore()
        {
            return this.highScoreRepository.ReadScores();
        }

        /// <inheritdoc/>
        public void CreatePlayer(string name, double health, double mana, double stamina, int str, int wis, int dex, string role)
        {
            Weapon weapon;
            Armour armour;
            Damage physres = new Damage(Damagetype.Physical);

            switch (role)
            {
                case "knight":
                    weapon = (Weapon)this.itempool.ToList().Find(w => w.Name.Equals("Bronze sword", StringComparison.OrdinalIgnoreCase));
                    weapon = weapon.DeepClone();
                    armour = (Armour)this.itempool.ToList().Find(a => a.Name.Equals("Chain mail", StringComparison.OrdinalIgnoreCase));
                    armour = armour.DeepClone();
                    physres.Value = 0.1;
                    break;
                case "hunter":
                    weapon = (Weapon)this.itempool.ToList().Find(w => w.Name.Equals("Pearwood Bow", StringComparison.OrdinalIgnoreCase));
                    weapon = weapon.DeepClone();
                    armour = (Armour)this.itempool.ToList().Find(a => a.Name.Equals("Leather Vest", StringComparison.OrdinalIgnoreCase));
                    armour = armour.DeepClone();
                    physres.Value = 0.05;
                    break;
                case "mage":
                    weapon = (Weapon)this.itempool.ToList().Find(w => w.Name.Equals("Novice wand", StringComparison.OrdinalIgnoreCase));
                    weapon = weapon.DeepClone();
                    armour = (Armour)this.itempool.ToList().Find(a => a.Name.Equals("Novice Robe", StringComparison.OrdinalIgnoreCase));
                    armour = armour.DeepClone();
                    break;
                default:
                    weapon = (Weapon)this.itempool.ToList().Find(w => w.Name.Equals("Training sword", StringComparison.OrdinalIgnoreCase));
                    weapon = weapon.DeepClone();
                    armour = null;
                    break;
            }

            this.player = new Player()
            {
                Name = name,

                MaxHealth = health,
                MaxMana = mana,
                MaxStamina = stamina,
                Health = health,
                Mana = mana,
                Stamina = stamina,

                Str = str,
                Dex = dex,
                Wis = wis,

                X = 3,
                Y = 3,
                Facing = Direction.North,
                Texture = "player",
                Sight = 4,
                CanFly = false,

                Weapon = weapon,
                Armour = armour,

                Weaponry = new List<Weapon>(),
                Armoury = new List<Armour>(),
                Inventory = new List<Consumable>(),
                Resistance = new List<Damage>()
                {
                    physres,
                    new Damage(Damagetype.Fire, 0.02),
                    new Damage(Damagetype.Cold, 0.02),
                    new Damage(Damagetype.Lightning, 0.02),
                },
            };
        }

        private void OnExitReached(object source, EventArgs arg)
        {
            this.GenNewMap();
        }

        private void OnPlayerDied(object source, EventArgs arg)
        {
            this.highScoreRepository.SaveScore(this.level, this.player.Name);

            if (this.PlayerDied != null)
            {
                this.PlayerDied(source, arg);
            }
        }

        private void OnFOVUpdated(object source, EventArgs arg)
        {
            if (this.FOVUpdated != null)
            {
                this.FOVUpdated(source, arg);
            }
        }

        private void OnSoundEffect(object source, SoundEffectEventArgs arg)
        {
            if (this.SoundEffect != null)
            {
                this.SoundEffect(source, arg);
            }
        }

        private void TestMap()
        {
            this.level = default;
            this.entities.Add(this.Player);

            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    if (j == 0 || i == 0 || i == 12 || j == 12)
                    {
                        this.map[i][j] = new Cell(CellType.Wall);
                    }
                    else if (i == j && i == 5)
                    {
                        this.map[i][j] = new Cell(CellType.Wall);
                    }
                    else
                    {
                        this.map[i][j] = new Cell(CellType.Floor);
                    }
                }
            }

            List<Damage> dmg = new List<Damage>() { new Damage(Damagetype.Physical, 10), new Damage(Damagetype.Fire), new Damage(Damagetype.Lightning), new Damage(Damagetype.Cold) };

            IEnemy enemy = new Enemy("goblin", 100, dmg, 3, 3, 100);

            enemy.X = 10;

            enemy.Y = 10;

            this.entities.Add(enemy);

            this.dungeonLogic = new DungeonLogic(this.map, this.entities, this.maploot, this.itempool);
        }
    }
}
