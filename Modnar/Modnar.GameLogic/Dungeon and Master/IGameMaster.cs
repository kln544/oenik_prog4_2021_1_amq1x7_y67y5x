﻿// <copyright file="IGameMaster.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using Modnar.Repository;

    /// <summary>
    /// Interface for managing the game progression.
    /// </summary>
    public interface IGameMaster
    {
        /// <summary>
        /// Event when the FOV was updated.
        /// </summary>
        public event EventHandler<EventArgs> FOVUpdated;

        /// <summary>
        /// Event when the player died.
        /// </summary>
        public event EventHandler<EventArgs> PlayerDied;

        /// <summary>
        /// Event when soundeffect must be played.
        /// </summary>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <summary>
        /// Gets the map logic.
        /// </summary>
        public IDungeonLogic DungeonLogic { get; }

        /// <summary>
        /// Gets the current level.
        /// </summary>
        public int Level { get;  }

        /// <summary>
        /// Gets the player entity model.
        /// </summary>
        public Player Player { get; }

        /// <summary>
        /// Saves the current game.
        /// </summary>
        public void SaveGame();

        /// <summary>
        /// Load a saved game.
        /// </summary>
        public void LoadGame();

        /// <summary>
        /// Generate a new dungeon.
        /// </summary>
        public void GenNewMap();

        /// <summary>
        /// Start a new game.
        /// </summary>
        public void StartNewGame();

        /// <summary>
        /// Reads the leaderboard.
        /// </summary>
        /// <returns>The leaderboard.</returns>
        public List<Leaderboard> ReadHighScore();

        /// <summary>
        /// Starting character creation.
        /// </summary>
        /// <param name="name">The name of the player.</param>
        /// <param name="health">The health of the player.</param>
        /// <param name="mana">The mana of the player.</param>
        /// <param name="stamina">The stamina of the player.</param>
        /// <param name="str">The strabgth of the player.</param>
        /// <param name="wis">The wisdom of the player.</param>
        /// <param name="dex">The dexterity of the player.</param>
        /// <param name="role">The class of the player.</param>
        public void CreatePlayer(string name, double health, double mana, double stamina, int str, int wis, int dex, string role);
    }
}
