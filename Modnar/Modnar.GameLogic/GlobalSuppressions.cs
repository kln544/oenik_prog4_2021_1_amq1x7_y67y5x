﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "It is mandatory in this project.", Scope = "module")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "It does not cause any security issues in this project.", Scope = "module")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "It is safe to suppress in this environment.", Scope = "module")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "For xml serialization we cannot use IList", Scope = "module")]
