﻿// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="IEntityLogic"/> interface.
    /// </summary>
    public class PlayerLogic : IEntityLogic, IPlayerLogic
    {
        /// <summary>
        /// IEntity model for the player.
        /// </summary>
        private IPlayer entity;

        private IDungeonLogicEntityActions methods;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="player">Initializing the player.</param>
        /// <param name="methods">Access to the MapLogic's read-only methods.</param>
        public PlayerLogic(IPlayer player, IDungeonLogicEntityActions methods)
        {
            this.entity = player;

            this.methods = methods;
        }

        /// <inheritdoc/>
        public event EventHandler<EventArgs> EntityDied;

        /// <inheritdoc/>
        public event EventHandler<EventArgs> WeaponChanged;

        /// <inheritdoc/>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <inheritdoc/>
        public IEntity Entity { get => this.entity; }

        /// <inheritdoc/>
        public bool Move(Movedirection d)
        {
            int xdif = 0;

            int ydif = 0;

            switch (this.entity.Facing)
            {
                case Direction.North:
                    if (d == Movedirection.Forwards)
                    {
                        --ydif;
                    }
                    else if (d == Movedirection.Backwards)
                    {
                        ++ydif;
                    }
                    else if (d == Movedirection.Left)
                    {
                        --xdif;
                    }
                    else if (d == Movedirection.Right)
                    {
                        ++xdif;
                    }

                    break;
                case Direction.East:
                    if (d == Movedirection.Forwards)
                    {
                        ++xdif;
                    }
                    else if (d == Movedirection.Backwards)
                    {
                        --xdif;
                    }
                    else if (d == Movedirection.Left)
                    {
                        --ydif;
                    }
                    else if (d == Movedirection.Right)
                    {
                        ++ydif;
                    }

                    break;
                case Direction.South:
                    if (d == Movedirection.Forwards)
                    {
                        ++ydif;
                    }
                    else if (d == Movedirection.Backwards)
                    {
                        --ydif;
                    }
                    else if (d == Movedirection.Left)
                    {
                        ++xdif;
                    }
                    else if (d == Movedirection.Right)
                    {
                        --xdif;
                    }

                    break;
                case Direction.West:
                    if (d == Movedirection.Forwards)
                    {
                        --xdif;
                    }
                    else if (d == Movedirection.Backwards)
                    {
                        ++xdif;
                    }
                    else if (d == Movedirection.Left)
                    {
                        ++ydif;
                    }
                    else if (d == Movedirection.Right)
                    {
                        --ydif;
                    }

                    break;
                default:
                    break;
            }

            if (this.methods.ValidMove(this.entity.X + xdif, this.entity.Y + ydif, this.entity.CanFly))
            {
                this.entity.X += xdif;

                this.entity.Y += ydif;

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool Face(Movedirection d)
        {
            switch (this.entity.Facing)
            {
                case Direction.North:
                    if (d == Movedirection.Left)
                    {
                        this.entity.Facing = Direction.West;
                    }
                    else if (d == Movedirection.Right)
                    {
                        this.entity.Facing = Direction.East;
                    }

                    break;
                case Direction.East:
                    if (d == Movedirection.Left)
                    {
                        this.entity.Facing = Direction.North;
                    }
                    else if (d == Movedirection.Right)
                    {
                        this.entity.Facing = Direction.South;
                    }

                    break;
                case Direction.South:
                    if (d == Movedirection.Left)
                    {
                        this.entity.Facing = Direction.East;
                    }
                    else if (d == Movedirection.Right)
                    {
                        this.entity.Facing = Direction.West;
                    }

                    break;
                case Direction.West:
                    if (d == Movedirection.Left)
                    {
                        this.entity.Facing = Direction.South;
                    }
                    else if (d == Movedirection.Right)
                    {
                        this.entity.Facing = Direction.North;
                    }

                    break;
                default:
                    break;
            }

            return true;
        }

        /// <inheritdoc/>
        public double? DealDamage(IList<Damage> damage)
        {
            EnemyLogic target = (EnemyLogic)this.methods.CheckRange(this.entity.X, this.entity.Y, this.entity.Facing, this.entity.Weapon.Range, typeof(EnemyLogic));

            this.OnSoundEffect(new SoundEffectEventArgs("attack"));

            if (target != null)
            {
                double? suffered = target.TakeDamage(damage, (Player)this.entity);

                if (suffered != null)
                {
                    if (--this.entity.Weapon.Durability == 0)
                    {
                        this.ChangeWeapon();
                    }
                }

                return suffered;
            }

            return null;
        }

        /// <inheritdoc/>
        public double? TakeDamage(IList<Damage> damage)
        {
            double dmgtaken = 0;

            if (damage == null)
            {
                return null;
            }

            double res = 0;

            double def = 0;

            foreach (Damage dmg in damage)
            {
                res = this.Entity.Resistance.ToList().Find(r => r.Type == dmg.Type).Value;

                def = this.entity.Armour?.Defense?.ToList().Find(d => d.Type == dmg.Type)?.Value ?? 0;

                if (dmg.Value > def)
                {
                    dmgtaken += (dmg.Value - def) * (1 - res);
                }
            }

            if (this.entity.Armour != null && --this.entity.Armour.Durability == 0)
            {
                this.ChangeArmour();
            }

            this.entity.Health -= dmgtaken;

            if (this.entity.Health <= 0)
            {
                this.OnSoundEffect(new SoundEffectEventArgs("dead"));
                this.Die();
            }
            else if (dmgtaken > 0)
            {
                this.OnSoundEffect(new SoundEffectEventArgs("hurt"));
            }

            return dmgtaken;
        }

        /// <inheritdoc/>
        public bool Die()
        {
            this.OnEntityDead();
            return true;
        }

        /// <inheritdoc/>
        public bool ChangeWeapon(int id = -1)
        {
            if (id < -1 || id >= this.entity.Weaponry.Count)
            {
                return false;
            }

            if (id == -1)
            {
                if (this.entity.Weapon.Type == WeaponType.Fist)
                {
                    return false;
                }
                else if (this.entity.Weapon.Durability > 0 || this.entity.Weapon.Durability == -999)
                {
                    this.entity.Weaponry.Add(this.entity.Weapon);
                }

                this.entity.Weapon = null;

                this.OnWeaponChanged();

                return true;
            }
            else if (this.entity.Weaponry?[id] != null)
            {
                if (this.entity.Weapon.Type != WeaponType.Fist)
                {
                    this.entity.Weaponry.Add(this.entity.Weapon);
                }

                this.entity.Weapon = this.entity.Weaponry[id];

                this.entity.Weaponry.RemoveAt(id);

                this.OnWeaponChanged();

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool ChangeArmour(int id = -1)
        {
            if (id < -1 || id >= this.entity.Armoury.Count)
            {
                return false;
            }

            if (id == -1)
            {
                if (this.entity.Armour.Durability > 0)
                {
                    this.entity.Armoury.Add(this.entity.Armour);
                    this.entity.Armour = null;
                }
                else if (this.entity.Armour.Durability == 0)
                {
                    this.entity.Armour = null;

                    return true;
                }

                return false;
            }
            else if (this.entity.Armoury[id] != null)
            {
                if (this.entity.Armour.Durability > 0)
                {
                    this.entity.Armoury.Add(this.entity.Armour);
                }

                this.entity.Armour = this.entity.Armoury[id];

                this.entity.Armoury.RemoveAt(id);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UseConsumable(int id)
        {
            if (id < 0 || id >= this.entity.Inventory.Count)
            {
                return false;
            }

            IConsumable item = this.entity.Inventory[id];

            if (item != null)
            {
                switch (item.Health)
                {
                    case double healing when healing > 0 && healing <= 1:
                        this.entity.Health += healing * this.entity.MaxHealth;
                        break;
                    case double healing when healing > 1: // 10 is the minimum flat healing amount an item can give (like bread or smthng).
                        this.entity.Health += healing;
                        break;
                    default:
                        break;
                }

                switch (item.Stamina)
                {
                    case double stamina when stamina > 0 && stamina <= 1:
                        this.entity.Stamina += stamina * this.entity.MaxStamina;
                        break;
                    case double stamina when stamina > 1:
                        this.entity.Stamina += stamina;
                        break;
                    default:
                        break;
                }

                switch (item.Mana)
                {
                    case double mana when mana > 0 && mana <= 1:
                        this.entity.Mana += mana * this.entity.MaxMana;
                        break;
                    case double mana when mana > 1:
                        this.entity.Mana += mana;
                        break;
                    default:
                        break;
                }

                switch (item.Icon)
                {
                    case "Goddess' blessing":
                        foreach (Damage dmg in this.entity.Resistance)
                        {
                            dmg.Value++;
                        }

                        break;
                    case "Power of the Ancients":
                        this.entity.Str += 5;
                        this.entity.Dex += 5;
                        this.entity.Wis += 5;
                        break;
                    default:
                        break;
                }

                if (--item.Stack == 0)
                {
                    this.entity.Inventory.RemoveAt(id);
                }

                this.OnSoundEffect(new SoundEffectEventArgs("consume"));

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public void Interact()
        {
            this.methods.Interact(this.entity);
        }

        /// <summary>
        /// Method to throw an event when the entity dies.
        /// </summary>
        protected virtual void OnEntityDead()
        {
            if (this.EntityDied != null)
            {
                this.EntityDied(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Method to throw an event when the weapon was changed.
        /// </summary>
        protected virtual void OnWeaponChanged()
        {
            if (this.WeaponChanged != null)
            {
                this.WeaponChanged(this.entity.Weapon.Type, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Method to throw an event when sound effect must be player.
        /// </summary>
        /// <param name="arg">Argument for the sound effect.</param>
        protected virtual void OnSoundEffect(SoundEffectEventArgs arg)
        {
            if (this.SoundEffect != null)
            {
                this.SoundEffect(this.entity, arg);
            }
        }
    }
}
