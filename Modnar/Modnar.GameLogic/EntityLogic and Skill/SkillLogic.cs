﻿// <copyright file="SkillLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Implementation of <see cref="ISkillLogic"/> interface.
    /// </summary>
    public class SkillLogic : ISkillLogic
    {
        private IList<ISkill> allskills;

        private IList<ISkill> available;

        /// <summary>
        /// Initializes a new instance of the <see cref="SkillLogic"/> class.
        /// </summary>
        /// <param name="p">Player logic.</param>
        public SkillLogic(PlayerLogic p)
        {
            if (p == null)
            {
               throw new ArgumentNullException(nameof(p));
            }

            this.allskills = new List<ISkill>();

            this.available = new List<ISkill>();

            this.allskills.Add(new Skill(WeaponType.None, () => SkillLogic.Rest(p), "Rest", @"Images\Icons\rest.png", "Restores some energy."));
            this.allskills.Add(new Skill(WeaponType.None, () => SkillLogic.Meditate(p), "Meditate", @"Images\Icons\meditate.png", "Cost: 10 - mana. Restores some health and energy at the cost of mana."));
            this.allskills.Add(new Skill(WeaponType.Fist, () => SkillLogic.Bash(p), "Bash", @"Images\Icons\bash.png", "Cost: 10 - stamina. Punch with your fist for low damage."));
            this.allskills.Add(new Skill(WeaponType.Axe, () => SkillLogic.Crush(p), "Crush", @"Images\Icons\crush.png", "Cost: 55 - stamina. Deal a huge amount of physical damage using the weight of your axe, gains extra damage from strength"));
            this.allskills.Add(new Skill(WeaponType.Sword | WeaponType.Axe, () => SkillLogic.Slash(p), "Slash", @"Images\Icons\slash.png", "Cost: 20 - stamina. Slash with any bladed weapon, gains extra damage from strength on physical attack."));
            this.allskills.Add(new Skill(WeaponType.Sword, () => SkillLogic.Lunge(p), "Lunge", @"Images\Icons\lunge.png", "Cost: 35 - stamina. Jump foward 2 tiles, dealing lowered damage."));
            this.allskills.Add(new Skill(WeaponType.Spear, () => SkillLogic.Retreat(p), "Retreat", @"Images\Icons\retreat.png", "Cost: 40 - stamina. Poke the enemy while moving back 2 tiles, dealing lowered damage."));
            this.allskills.Add(new Skill(WeaponType.Sword | WeaponType.Spear, () => SkillLogic.Stab(p), "Stab", @"Images\Icons\stab.png", "Cost: 15 - stamina. Stab the enemy with the point of your weapon, gains extra damage from strength on physical attack ."));
            this.allskills.Add(new Skill(WeaponType.Bow, () => SkillLogic.QuickShot(p), "Quick Shot", @"Images\Icons\quickshot.png", "Cost: 10 - stamina. Quickly shoot your bow dealind normal damage, gains extra damage from dexterity."));
            this.allskills.Add(new Skill(WeaponType.Bow, () => SkillLogic.BurningArrow(p), "Burning Arrow", @"Images\Icons\burningarrow.png", "Cost: 10 - stamina, 15 - mana. Physical damage converted to fire damage, lowers other elemental damage. Gains extra damage from dexterity on fire and physical attacks."));
            this.allskills.Add(new Skill(WeaponType.Bow, () => SkillLogic.IceShot(p), "Ice Shot", @"Images\Icons\iceshot.png", "Cost: 10 - stamina, 15 - mana. Physical damage converted to cold damage, lowers other elemental damage. Gains extra damage from dexterity on cold and physical attacks."));
            this.allskills.Add(new Skill(WeaponType.Bow, () => SkillLogic.GalvanicArrow(p), "Galvanic Arrow", @"Images\Icons\galvanicarrow.png", "Cost: 10 - stamina, 15 - mana. Physical damage converted to lightning damage, lowers other elemental damage. Gains extra damage from dexterity on lightning and physical attacks."));
            this.allskills.Add(new Skill(WeaponType.Wand, () => SkillLogic.MagicMissile(p), "Magic Missile", @"Images\Icons\missile.png", "Cost: 6 - mana. Shoot a magic missile using your wand, gains extra damage from wisdom. "));
            this.allskills.Add(new Skill(WeaponType.Wand, () => SkillLogic.MagicBurst(p), "Magic Burst", @"Images\Icons\burst.png", "Cost: 30 - mana. Barrage of magical missiles, 3 attacks at the cost of increased mana, gains extra damage from wisdom."));

            p.WeaponChanged += this.OnWeaponChanged;

            Player player = (Player)p.Entity;

            this.Refreshlist(player.Weapon.Type);
        }

        /// <inheritdoc/>
        public IList<ISkill> Available { get => this.available; }

        /// <inheritdoc/>
        public void Refreshlist(WeaponType type)
        {
            this.available = this.allskills.Where(s => s.Type.HasFlag(type) || s.Type.Equals(WeaponType.None)).ToList();
        }

        private static void Bash(PlayerLogic p)
        {
            const int stam = 10;

            Player player = (Player)p.Entity;

            if (player.Stamina >= stam)
            {
                player.Stamina -= stam;

                p.DealDamage(player.Weapon.Damage);
            }
        }

        private static void Crush(PlayerLogic p)
        {
            const int stam = 55;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (3 + (player.Str / 20));
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.DealDamage(dmg);
            }
        }

        private static void Slash(PlayerLogic p)
        {
            const int stam = 20;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (1.2 + (player.Str / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value = 1.1;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.DealDamage(dmg);
            }
        }

        private static void Lunge(PlayerLogic p)
        {
            const int stam = 35;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (0.8 + (player.Str / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * 0.8;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.Move(Movedirection.Forwards);
                p.Move(Movedirection.Forwards);

                p.DealDamage(dmg);
            }
        }

        private static void Retreat(PlayerLogic p)
        {
            const int stam = 40;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (0.7 + (player.Str / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * 0.6;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.DealDamage(dmg);

                p.Move(Movedirection.Backwards);
                p.Move(Movedirection.Backwards);
            }
        }

        private static void Stab(PlayerLogic p)
        {
            const int stam = 15;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (1.1 + (player.Str / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.DealDamage(dmg);
            }
        }

        private static void QuickShot(PlayerLogic p)
        {
            const int stam = 10;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    d.Type = damage.Type;
                    d.Value = damage.Value * (1 + (player.Dex / 50));

                    dmg.Add(d);
                }

                player.Stamina -= stam;

                p.DealDamage(dmg);
            }
        }

        private static void BurningArrow(PlayerLogic p)
        {
            const int stam = 10;

            const int mana = 15;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam && player.Mana >= mana)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Fire))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (1.5 + (player.Dex / 50));
                    }
                    else if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = Damagetype.Fire;
                        d.Value = damage.Value * (0.8 + (player.Dex / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * 0.5;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;
                player.Mana -= mana;

                p.DealDamage(dmg);
            }
        }

        private static void IceShot(PlayerLogic p)
        {
            const int stam = 10;

            const int mana = 15;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Stamina >= stam && player.Mana >= mana)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Cold))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (1.5 + (player.Dex / 50));
                    }
                    else if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = Damagetype.Cold;
                        d.Value = damage.Value * (0.8 + (player.Dex / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * 0.5;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;
                player.Mana -= mana;

                p.DealDamage(dmg);
            }
        }

        private static void GalvanicArrow(PlayerLogic p)
        {
            const int stam = 10;

            const int mana = 15;

            Player player = (Player)p.Entity;

            if (player.Stamina >= stam && player.Mana >= mana)
            {
                IList<Damage> dmg = new List<Damage>();

                Damage d = new Damage();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    if (damage.Type.Equals(Damagetype.Lightning))
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * (1.5 + (player.Dex / 50));
                    }
                    else if (damage.Type.Equals(Damagetype.Physical))
                    {
                        d.Type = Damagetype.Lightning;
                        d.Value = damage.Value * (0.8 + (player.Dex / 50));
                    }
                    else
                    {
                        d.Type = damage.Type;
                        d.Value = damage.Value * 0.5;
                    }

                    dmg.Add(d);
                }

                player.Stamina -= stam;
                player.Mana -= mana;

                p.DealDamage(dmg);
            }
        }

        private static void MagicMissile(PlayerLogic p)
        {
            const int mana = 6;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Mana >= mana)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    d.Type = damage.Type;
                    d.Value = damage.Value * (1.2 + (player.Wis / 50));

                    dmg.Add(damage);
                }

                player.Mana -= mana;

                p.DealDamage(dmg);
            }
        }

        private static void MagicBurst(PlayerLogic p)
        {
            const int mana = 30;

            Player player = (Player)p.Entity;

            Damage d = new Damage();

            if (player.Mana >= mana)
            {
                IList<Damage> dmg = new List<Damage>();

                foreach (Damage damage in player.Weapon.Damage)
                {
                    d.Type = damage.Type;
                    d.Value = damage.Value * (1.1 + (player.Wis / 50));

                    dmg.Add(damage);
                }

                player.Mana -= mana;

                p.DealDamage(dmg);
                p.DealDamage(dmg);
                p.DealDamage(dmg);
            }
        }

        private static void Rest(PlayerLogic p)
        {
            Player player = (Player)p.Entity;

            player.Stamina += player.Dex / 10;
        }

        private static void Meditate(PlayerLogic p)
        {
            Player player = (Player)p.Entity;

            const int mana = 10;

            if (player.Mana >= mana)
            {
                player.Stamina += player.Wis / 10;

                player.Health += player.Wis / 10;

                player.Mana -= mana;
            }
        }

        private void OnWeaponChanged(object source, EventArgs arg)
        {
            this.Refreshlist((WeaponType)source);
        }
    }
}
