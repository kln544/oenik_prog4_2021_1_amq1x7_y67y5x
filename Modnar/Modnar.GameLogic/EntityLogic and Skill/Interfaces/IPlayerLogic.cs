﻿// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface defining player specific behaviour.
    /// </summary>
    public interface IPlayerLogic
    {
        /// <summary>
        /// Event when weapon was changed.
        /// </summary>
        public event EventHandler<EventArgs> WeaponChanged;

        /// <summary>
        /// Method to change weapon.
        /// </summary>
        /// <param name="id">The id of the weapon.</param>
        /// <returns>Whether it was successful.</returns>
        public bool ChangeWeapon(int id);

        /// <summary>
        /// Method to change armour.
        /// </summary>
        /// <param name="id">The id of the armour.</param>
        /// <returns>Whether it was successful.</returns>
        public bool ChangeArmour(int id);

        /// <summary>
        /// Method to consume items.
        /// </summary>
        /// <param name="id">The id of the item.</param>
        /// <returns>Whether it was succsessful.</returns>
        public bool UseConsumable(int id);

        /// <summary>
        /// Method to move the player in the given direction depending on the direction of facing.
        /// </summary>
        /// <param name="d">The direction of the movement.</param>
        /// <returns>Whether it was successful.</returns>
        public bool Move(Movedirection d);

        /// <summary>
        /// Method to turn the entity.
        /// </summary>
        /// <param name="d">The direction to turn.</param>
        /// <returns>Whether it was successful.</returns>
        public bool Face(Movedirection d);

        /// <summary>
        /// Taking damage.
        /// </summary>
        /// <param name="damage">The damage values input.</param>
        /// <returns>How much damage was taken.</returns>
        public double? TakeDamage(IList<Damage> damage);

        /// <summary>
        /// A call for the DungeonLogic's interact method.
        /// </summary>
        public void Interact();
    }
}
