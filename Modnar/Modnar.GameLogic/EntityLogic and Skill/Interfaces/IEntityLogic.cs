﻿// <copyright file="IEntityLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface defining entity interactions.
    /// </summary>
    public interface IEntityLogic
    {
        /// <summary>
        /// Event when the entity died.
        /// </summary>
        public event EventHandler<EventArgs> EntityDied;

        /// <summary>
        /// Event when soundeffect must be played.
        /// </summary>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <summary>
        /// Gets the entity.
        /// </summary>
        public IEntity Entity { get; }

        /// <summary>
        /// Dealing damage to another entity.
        /// </summary>
        /// <param name="damage">The damage that will be dealt.</param>
        /// <returns>How much damage was dealt.</returns>
        public double? DealDamage(IList<Damage> damage);

        /// <summary>
        /// Dies. Just dies.
        /// </summary>
        /// <returns>Whether the entity died successfully.</returns>
        public bool Die();
    }
}
