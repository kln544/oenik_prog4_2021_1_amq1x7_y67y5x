﻿// <copyright file="IAiLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface defining entity behaviour.
    /// </summary>
    public interface IAiLogic
    {
        /// <summary>
        /// Gets targeted x (horizontal) coordinate.
        /// </summary>
        public int? Targetx { get; }

        /// <summary>
        /// Gets targeted y (vertical) coordinate.
        /// </summary>
        public int? Targety { get; }

        /// <summary>
        /// Gets the stack containing the directions of movement.
        /// </summary>
        public Stack<Direction> Moves { get; }

        /// <summary>
        /// Method to move the entity in the given direction.
        /// </summary>
        /// <param name="d">The direction of the movement.</param>
        /// <returns>Whether it was successful.</returns>
        public bool Move(Direction d);

        /// <summary>
        /// Turns the entity towards it's target.
        /// </summary>
        public void FaceTarget();

        /// <summary>
        /// Runs the methods in order for the current round.
        /// </summary>
        /// <returns>Returns whether the all the actions this turn was successful.</returns>
        public bool DoCurrentTurn();

        /// <summary>
        /// Idle action for the enemy.
        /// </summary>
        public void IdleAction();

        /// <summary>
        /// Finds target if there is one in FOV.
        /// </summary>
        /// <returns>Whether a target was found.</returns>
        public bool FindTarget();

        /// <summary>
        /// Tryes to find a path to the target.
        /// </summary>
        /// <param name="x">The current x (horizontal) coordinate.</param>
        /// <param name="y">The current y (vertical) coordinate.</param>
        /// <param name="visited">Previously visited coordinates.</param>
        /// <returns>Returns an array of the directions to move.</returns>
        public Stack<Direction> PathToTarget(int x, int y, ref IList<CordPair> visited);

        /// <summary>
        /// Support method for <see cref="PathToTarget"/>.
        /// </summary>
        /// <param name="x">The current x (horizontal) coordinate.</param>
        /// <param name="y">The current y (vertical) coordinate.</param>
        /// <returns>The directions in order to which way look first for a path.</returns>
        public IList<Direction> PathOrder(int x, int y);

        /// <summary>
        /// Taking damage and setting the sender to be the target.
        /// </summary>
        /// <param name="damage">The damage values input.</param>
        /// <param name="player">The darget the damage is coming from.</param>
        /// <returns>How much damage was taken.</returns>
        public double? TakeDamage(IList<Damage> damage, Player player);
    }
}
