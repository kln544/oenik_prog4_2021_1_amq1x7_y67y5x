﻿// <copyright file="ISkillLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;

    /// <summary>
    /// Interface defining skill behaviour.
    /// </summary>
    public interface ISkillLogic
    {
        /// <summary>
        /// Gets the list of available skills.
        /// </summary>
        public IList<ISkill> Available { get; }

        /// <summary>
        /// Refreshes the list of available skills for the corresponding weapon.
        /// </summary>
        /// <param name="type">The type of the weapon.</param>
        public void Refreshlist(WeaponType type);
    }
}
