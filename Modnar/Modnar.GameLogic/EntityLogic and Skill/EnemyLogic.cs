﻿// <copyright file="EnemyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using static Modnar.GameLogic.IEntityLogic;

    /// <summary>
    /// Implementation of <see cref="IEntityLogic"/> and <see cref="IAiLogic"/> interfaces.
    /// </summary>
    public class EnemyLogic : IEntityLogic, IAiLogic
    {
        private IEnemy entity;

        private int? targetx;

        private int? targety;

        private Stack<Direction> moves;

        private IDungeonLogicEntityActions methods;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyLogic"/> class.
        /// </summary>
        /// <param name="entity">The entity of the class.</param>
        /// <param name="methods">Access to the MapLogic's read-only methods.</param>
        public EnemyLogic(IEnemy entity, IDungeonLogicEntityActions methods)
        {
            this.entity = entity;

            this.methods = methods;

            this.moves = new Stack<Direction>();
        }

        /// <inheritdoc/>
        public event EventHandler<EventArgs> EntityDied;

        /// <inheritdoc/>
        public event EventHandler<SoundEffectEventArgs> SoundEffect;

        /// <inheritdoc/>
        public IEntity Entity { get => this.entity; }

        /// <inheritdoc/>
        public int? Targetx { get => this.targetx; }

        /// <inheritdoc/>
        public int? Targety { get => this.targety; }

        /// <inheritdoc/>
        public Stack<Direction> Moves { get => this.moves; }

        /// <inheritdoc/>
        public bool DoCurrentTurn()
        {
            bool ret = true;

            // the enemy has to catch it's breath every few turns (defined in speed).
            if (this.methods.Round % this.entity.Speed != 0)
            {
                if (this.DealDamage(this.entity.Damage) == null)
                {
                    this.FindTarget();
                    this.FaceTarget();
                    if (this.moves.Any())
                    {
                        this.Move(this.moves.Pop());
                    }

                    this.IdleAction();
                }
            }

            return ret;
        }

        /// <inheritdoc/>
        public bool Move(Direction d)
        {
            int x = 0;

            int y = 0;

            switch (d)
            {
                case Direction.North:
                    y--;
                    break;
                case Direction.East:
                    x++;
                    break;
                case Direction.South:
                    y++;
                    break;
                case Direction.West:
                    x--;
                    break;
                default:
                    return false;
            }

            if (this.methods.ValidMove(this.entity.X + x, this.entity.Y + y, this.entity.CanFly))
            {
                this.entity.X += x;
                this.entity.Y += y;
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool FindTarget()
        {
            PlayerLogic e;

            e = (PlayerLogic)this.methods.EntityFOV(this.entity.X, this.entity.Y, this.entity.Facing, this.entity.Sight);

            if (e != null)
            {
                this.entity.Memory = 6;
                if (this.targetx != e.Entity.X || this.targety != e.Entity.Y)
                {
                    this.targetx = e.Entity.X;
                    this.targety = e.Entity.Y;

                    this.moves.Clear();
                    IList<CordPair> visited = new List<CordPair>();
                    this.moves = this.PathToTarget(this.Entity.X, this.Entity.Y, ref visited);
                }

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public Stack<Direction> PathToTarget(int x, int y, ref IList<CordPair> visited)
        {
            if (visited == null)
            {
                visited = new List<CordPair>();
            }

            Stack<Direction> dirs = new Stack<Direction>();

            visited.Add(new CordPair() { X = x, Y = y });

            int dx = 0;

            int dy = 0;

            if (this.Distance(x, y) > 10)
            {
                return dirs;
            }

            foreach (Direction dir in this.PathOrder(x, y))
            {
                switch (dir)
                {
                    case Direction.North:
                        dx = 0;
                        dy = -1;
                        break;
                    case Direction.East:
                        dx = 1;
                        dy = 0;
                        break;
                    case Direction.South:
                        dx = 0;
                        dy = 1;
                        break;
                    case Direction.West:
                        dx = -1;
                        dy = 0;
                        break;
                    default:
                        break;
                }

                if (this.targetx == x + dx && this.targety == y + dy)
                {
                    dirs.Push(dir);

                    return dirs;
                }
                else
                {
                    if (this.methods.ValidMove(x + dx, y + dy, this.Entity.CanFly) && visited.ToList().Find(c => c.X == x + dx && c.Y == y + dy) == null)
                    {
                        dirs = this.PathToTarget(x + dx, y + dy, ref visited);
                        if (dirs.Any())
                        {
                            dirs.Push(dir);

                            return dirs;
                        }
                    }
                }
            }

            return dirs;
        }

        /// <inheritdoc/>
        public void FaceTarget()
        {
            if (this.targetx == null)
            {
                return;
            }

            int dx = (int)this.targetx - this.entity.X;

            int dy = (int)this.targety - this.entity.Y;

            if (Math.Abs(dx) == Math.Abs(dy) && dx == 0)
            {
                return;
            }
            else if (Math.Abs(dx) > Math.Abs(dy))
            {
                if (dx > 0)
                {
                    this.entity.Facing = Direction.East;
                }
                else
                {
                    this.entity.Facing = Direction.West;
                }
            }
            else
            {
                if (dy > 0)
                {
                    this.entity.Facing = Direction.South;
                }
                else
                {
                    this.entity.Facing = Direction.North;
                }
            }
        }

        /// <inheritdoc/>
        public IList<Direction> PathOrder(int x, int y)
        {
            IList<Direction> dir = new List<Direction>();

            int absx = Math.Abs((int)this.targetx - x);
            int absy = Math.Abs((int)this.targety - y);

            if (absx > absy)
            {
                if (this.targetx > x)
                { // East
                    dir.Add(Direction.East);

                    if (this.targety > y)
                    {// South
                        dir.Add(Direction.South);
                        dir.Add(Direction.North);
                    }
                    else
                    {// North
                        dir.Add(Direction.North);
                        dir.Add(Direction.South);
                    }

                    dir.Add(Direction.West);
                }
                else
                { // West
                    dir.Add(Direction.West);

                    if (this.targety > y)
                    {// South
                        dir.Add(Direction.South);
                        dir.Add(Direction.North);
                    }
                    else
                    {// North
                        dir.Add(Direction.North);
                        dir.Add(Direction.South);
                    }

                    dir.Add(Direction.East);
                }
            }
            else
            {
                if (this.targety > y)
                { // South
                    dir.Add(Direction.South);

                    if (this.targetx > x)
                    {// East
                        dir.Add(Direction.East);
                        dir.Add(Direction.West);
                    }
                    else
                    {// West
                        dir.Add(Direction.West);
                        dir.Add(Direction.East);
                    }

                    dir.Add(Direction.North);
                }
                else
                { // North
                    dir.Add(Direction.North);

                    if (this.targetx > x)
                    {// East
                        dir.Add(Direction.East);
                        dir.Add(Direction.West);
                    }
                    else
                    {// West
                        dir.Add(Direction.West);
                        dir.Add(Direction.East);
                    }

                    dir.Add(Direction.South);
                }
            }

            return dir;
        }

        /// <inheritdoc/>
        public double? DealDamage(IList<Damage> damage)
        {
            var target = (PlayerLogic)this.methods.CheckRange(this.entity.X, this.entity.Y, this.entity.Facing, this.entity.Range, typeof(PlayerLogic));

            if (target != null)
            {
                return target.TakeDamage(damage);
            }

            return null; // Verifying there were no target.
        }

        /// <inheritdoc/>
        public double? TakeDamage(IList<Damage> damage, Player player)
        {
            double dmgtaken = 0;

            if (damage == null || player == null)
            {
                return null;
            }

            double res = 0;

            foreach (Damage dmg in damage)
            {
                res = this.Entity.Resistance.ToList().Find(r => r.Type == dmg.Type).Value;

                dmgtaken += dmg.Value * (1 - res);
            }

            if (player.Facing == this.entity.Facing)
            {
                dmgtaken *= 1.5;
            }

            this.entity.Health -= dmgtaken;

            this.targetx = player.X;

            this.targety = player.Y;

            this.FaceTarget();

            if (this.entity.Health <= 0)
            {
                this.OnSoundEffect(new SoundEffectEventArgs("dead"));
                this.Die();
            }
            else if (dmgtaken > 0)
            {
                this.OnSoundEffect(new SoundEffectEventArgs("hurt"));
            }

            return dmgtaken;
        }

        /// <inheritdoc/>
        public void IdleAction()
        {
            --this.entity.Memory;
            if (this.entity.Memory == 0)
            {
                Random rnd = new Random();

                this.targetx = null;

                this.targety = null;

                if (rnd.Next(0, 10) < this.entity.Sight)
                {
                    Array dirs = Enum.GetValues(typeof(Direction));

                    Direction dir = (Direction)dirs.GetValue(rnd.Next(dirs.Length));

                    this.entity.Facing = dir;
                }

                if (rnd.Next(0, 10) < this.entity.Speed)
                {
                    this.Move(this.entity.Facing);
                }
            }
        }

        /// <inheritdoc/>
        public bool Die()
        {
            this.OnEntityDead();
            return true;
        }

        /// <summary>
        /// Method to throw an event on entity died.
        /// </summary>
        protected virtual void OnEntityDead()
        {
            if (this.EntityDied != null)
            {
                this.EntityDied(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Method to throw an event when sound effect must be player.
        /// </summary>
        /// <param name="arg">Argument for the sound effect.</param>
        protected virtual void OnSoundEffect(SoundEffectEventArgs arg)
        {
            if (this.SoundEffect != null)
            {
                this.SoundEffect(this.entity, arg);
            }
        }

        /// <summary>
        /// Calculates the given point's distance from the current target.
        /// </summary>
        /// <param name="x">The x (horizontal) coordinate of the point.</param>
        /// <param name="y">The y (vertical) coordinate of the point.</param>
        /// <returns>Returns the distance.</returns>
        private double Distance(int x, int y)
        {
            int absx = Math.Abs((int)this.targetx - x);
            int absy = Math.Abs((int)this.targety - y);

            return Math.Sqrt((absx * absx) + (absy * absy));
        }
    }
}
