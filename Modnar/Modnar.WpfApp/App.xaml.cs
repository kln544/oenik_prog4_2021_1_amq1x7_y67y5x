﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.Repository;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.UI;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIOC.Instance);
            MyIOC.Instance.Register<IDisplayGameService, DisplayGameService>();
            MyIOC.Instance.Register<IDisplayCharacterCreatorService, DisplayCharacterCreatorService>();
            MyIOC.Instance.Register<IDisplayMenuService, DisplayMenuService>();
            MyIOC.Instance.Register<IDisplayLeadeboardService, DisplayLeadeboardService>();

            MyIOC.Instance.Register<IMainMenuLogic>(() => new MainMenuLogic(ServiceLocator.Current.GetInstance<IGameMaster>()));
            MyIOC.Instance.Register<IGameBusinessLogic>(() => new GameBusinessLogic(ServiceLocator.Current.GetInstance<IGameMaster>()));

            MyIOC.Instance.Register<IResourceRepository, ResourceRepository>();
            MyIOC.Instance.Register<ISavedGameRepository, SavedGameRepository>();
            MyIOC.Instance.Register<IHighScoreRepository, HighScoreRepository>();

            MyIOC.Instance.Register<IGameMaster>(() => new GameMaster(ServiceLocator.Current.GetInstance<IResourceRepository>(), ServiceLocator.Current.GetInstance<IHighScoreRepository>(), ServiceLocator.Current.GetInstance<ISavedGameRepository>()));

            // MyIOC.Instance.Register<IGameMaster, GameMaster>();
        }
    }
}
