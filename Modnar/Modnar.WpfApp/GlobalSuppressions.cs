﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It is mandatory in this project.", Scope = "module")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "It does not cause any security issues in this project.", Scope = "module")]