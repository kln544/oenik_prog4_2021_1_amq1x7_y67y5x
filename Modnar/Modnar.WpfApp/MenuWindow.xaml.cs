﻿// <copyright file="MenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Windows;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.VM;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class MenuWindow : Window
    {
        private MenuViewModel vm;
        private IDisplayLeadeboardService displayLeadeboardService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// </summary>
        /// <param name="displayLeadeboardService">Display leaderboard service.</param>
        public MenuWindow(IDisplayLeadeboardService displayLeadeboardService)
        {
            this.displayLeadeboardService = displayLeadeboardService ?? throw new ArgumentNullException(nameof(displayLeadeboardService));
            this.InitializeComponent();
            this.vm = this.FindResource("VM") as MenuViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// </summary>
        public MenuWindow()
            : this(ServiceLocator.Current.GetInstance<IDisplayLeadeboardService>())
        {
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to quit?", "Exit", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        private void SaveGame_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("This will overwrite your previous save. Are you sure to continue?", "Save", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.vm.GameLogic.SaveGame();
            }
        }

        private void HighScore_Click(object sender, RoutedEventArgs e)
        {
            this.displayLeadeboardService.DisplayLeaderBoard();
        }

        private void Return_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Current game will be lost. Are you sure to continue?", "Load", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.vm.GameLogic.LoadSavedGame();
            }
        }

        private void Weaponchange_Click(object sender, RoutedEventArgs e)
        {
            this.vm.GameLogic.WeaponIdx = this.weaponlistbox.SelectedIndex;
        }

        private void Armourchange_Click(object sender, RoutedEventArgs e)
        {
            this.vm.GameLogic.ArmourIdx = this.armourlistbox.SelectedIndex;
        }
    }
}
