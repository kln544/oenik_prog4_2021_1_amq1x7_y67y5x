﻿// <copyright file="CharacterCreatorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Modnar.WpfApp.Data;
    using Modnar.WpfApp.VM;

    /// <summary>
    /// Interaction logic for CharacterCreatorWindow.xaml.
    /// </summary>
    public partial class CharacterCreatorWindow : Window
    {
        private CharacterCreatorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterCreatorWindow"/> class.
        /// </summary>
        public CharacterCreatorWindow()
        {
            this.InitializeComponent();
        }

        ///// <summary>
        ///// Gets character.
        ///// </summary>
        // public CharacterModel Character
        // {
        //    get { return this.vm.Character; }
        // }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vm = this.FindResource("VM") as CharacterCreatorViewModel;
        }

        private void Knight_Click(object sender, RoutedEventArgs e)
        {
            this.vm.MainMenuLogic.GetKnight(this.vm.Character.Name);
            this.Close();
        }

        private void Mage_Click(object sender, RoutedEventArgs e)
        {
            this.vm.MainMenuLogic.GetMage(this.vm.Character.Name);
            this.Close();
        }

        private void Hunter_Click(object sender, RoutedEventArgs e)
        {
            this.vm.MainMenuLogic.GetHunter(this.vm.Character.Name);
            this.Close();
        }
    }
}
