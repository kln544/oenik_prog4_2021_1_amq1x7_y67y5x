﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using CommonServiceLocator;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.UI;
    using Modnar.WpfApp.VM;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel vm;
        private IDisplayGameService displayGameService;
        private IDisplayCharacterCreatorService displayCharacterCreatorService;
        private IDisplayLeadeboardService displayLeadeboardService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="displayGameService">Display game service.</param>
        /// <param name="displayCharacterCreatorService">Display character service.</param>
        /// <param name="displayLeadeboardService">Display leaderboard service.</param>
        public MainWindow(IDisplayGameService displayGameService, IDisplayCharacterCreatorService displayCharacterCreatorService, IDisplayLeadeboardService displayLeadeboardService)
        {
            this.displayLeadeboardService = displayLeadeboardService;
            this.displayCharacterCreatorService = displayCharacterCreatorService;
            this.displayGameService = displayGameService;
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
            : this(ServiceLocator.Current.GetInstance<IDisplayGameService>(), ServiceLocator.Current.GetInstance<IDisplayCharacterCreatorService>(), ServiceLocator.Current.GetInstance<IDisplayLeadeboardService>())
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vm = this.FindResource("VM") as MainViewModel;
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            this.displayCharacterCreatorService.DisplayCharacterCreator();
            this.vm.MainMenuLogic.StartNewGame();
            this.displayGameService.DisplayGame();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            this.vm.MainMenuLogic.LoadSavedGame();
            this.displayGameService.DisplayGame();
        }

        private void Highscore_Click(object sender, RoutedEventArgs e)
        {
            this.displayLeadeboardService.DisplayLeaderBoard();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
