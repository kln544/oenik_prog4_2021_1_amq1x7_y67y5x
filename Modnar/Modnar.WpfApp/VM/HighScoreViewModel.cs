﻿// <copyright file="HighScoreViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// View model for the Higscore window.
    /// </summary>
    public class HighScoreViewModel : ViewModelBase
    {
        private IMainMenuLogic mainMenuLogic;
        private IList<ScoreModel> highScores;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreViewModel"/> class.
        /// </summary>
        /// <param name="mainMenuLogic">Main menu logic.</param>
        public HighScoreViewModel(IMainMenuLogic mainMenuLogic)
        {
            if (this.IsInDesignMode)
            {
                this.highScores = new List<ScoreModel>() { new ScoreModel() { Name = "Placeholder", Level = 42, } };
            }
            else
            {
                this.mainMenuLogic = mainMenuLogic;
                this.mainMenuLogic = mainMenuLogic ?? throw new ArgumentNullException(nameof(mainMenuLogic));
                this.highScores = this.mainMenuLogic.ViewHighScore();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreViewModel"/> class.
        /// </summary>
        public HighScoreViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainMenuLogic>())
        {
        }

        /// <summary>
        /// Gets leaderboard.
        /// </summary>
        public IList<ScoreModel> HighScores { get => this.highScores; }
    }
}
