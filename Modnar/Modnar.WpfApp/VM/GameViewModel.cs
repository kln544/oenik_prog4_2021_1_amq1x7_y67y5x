﻿// <copyright file="GameViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.Data;
    using Modnar.WpfApp.UI;

    /// <summary>
    /// Viewmodel for the game window.
    /// </summary>
    public class GameViewModel
        : ViewModelBase
    {
        private IGameBusinessLogic gameLogic;

        private ObservableCollection<SkillModel> skills;
        private ObservableCollection<ConsumableModel> consumables;

        private SkillModel skillSelected;
        private ConsumableModel itemSelected;
        private int level;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameViewModel"/> class.
        /// </summary>
        /// <param name="gameLogic">Game Logic.</param>
        public GameViewModel(IGameBusinessLogic gameLogic)
        {
            if (!this.IsInDesignMode)
            {
                this.gameLogic = gameLogic ?? throw new System.ArgumentNullException(nameof(gameLogic));
                this.skills = this.gameLogic.GetSkillList();
                this.consumables = this.gameLogic.GetConsumableList();
                this.CurrentGameControl = new GameControl();
                this.MiniMapControl = new MiniMapControl();
                this.level = this.gameLogic.GetLevel();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameViewModel"/> class.
        /// </summary>
        public GameViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IGameBusinessLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the skill list.
        /// </summary>
        public ObservableCollection<SkillModel> Skills { get => this.skills; set => this.skills = value; }

        /// <summary>
        /// Gets or sets the selected skill.
        /// </summary>
        public SkillModel SkillSelected { get => this.skillSelected; set => this.skillSelected = value; }

        /// <summary>
        /// Gets or sets the consumable item list.
        /// </summary>
        public ObservableCollection<ConsumableModel> Consumables { get => this.consumables; set => this.consumables = value; }

        /// <summary>
        /// Gets or sets the selected item.
        /// </summary>
        public ConsumableModel ItemSelected { get => this.itemSelected; set => this.itemSelected = value; }

        /// <summary>
        /// Gets or sets the game logic.
        /// </summary>
        public IGameBusinessLogic GameLogic { get => this.gameLogic; set => this.gameLogic = value; }

        /// <summary>
        /// Gets or Sets the current game control.
        /// </summary>
        public GameControl CurrentGameControl { get; set; }

        /// <summary>
        /// Gets or Sets the current Mini map control.
        /// </summary>
        public MiniMapControl MiniMapControl { get; set; }

        /// <summary>
        /// Gets use skill command.
        /// </summary>
        public ICommand UseSkillCmd { get; private set; }

        /// <summary>
        /// Gets use item command.
        /// </summary>
        public ICommand UseItemCmd { get; private set; }

        /// <summary>
        /// Gets the current level.
        /// </summary>
        public int Level { get => this.level; }
    }
}
