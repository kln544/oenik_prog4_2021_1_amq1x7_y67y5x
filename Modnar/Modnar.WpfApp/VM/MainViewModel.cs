﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// View Model for the main window.
    /// </summary>
    public class MainViewModel
        : ViewModelBase
    {
        private IMainMenuLogic mainMenuLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="mainMenuLogic">Main menu logic.</param>
        public MainViewModel(IMainMenuLogic mainMenuLogic)
        {
            if (!this.IsInDesignMode)
            {
                this.mainMenuLogic = mainMenuLogic ?? throw new ArgumentNullException(nameof(mainMenuLogic));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(
                IsInDesignModeStatic ? null :
                  ServiceLocator.Current.GetInstance<IMainMenuLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the logic.
        /// </summary>
        public IMainMenuLogic MainMenuLogic { get => this.mainMenuLogic; set => this.mainMenuLogic = value; }
    }
}
