﻿// <copyright file="MapViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Modnar.WpfApp.UI;

    /// <summary>
    /// View Model for the map.
    /// </summary>
    public class MapViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapViewModel"/> class.
        /// </summary>
        public MapViewModel()
        {
            if (!this.IsInDesignMode)
            {
                this.FullMapControl = new FullMapControl();
            }
        }

        /// <summary>
        /// Gets or Sets the current Full map control.
        /// </summary>
        public FullMapControl FullMapControl { get; set; }
    }
}
