﻿// <copyright file="MenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Viewmodel for the Menu.
    /// </summary>
    public class MenuViewModel : ViewModelBase
    {
        private IGameBusinessLogic gameLogic;
        private Player player;

        private ArmourModel armourSelected;
        private WeaponModel weaponSelected;
        private int level;

        private ObservableCollection<ConsumableModel> consumableList;
        private ObservableCollection<WeaponModel> weaponList;
        private ObservableCollection<SkillModel> skills;
        private ObservableCollection<ArmourModel> armourList;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        /// <param name="gameLogic">Game logic.</param>
        public MenuViewModel(IGameBusinessLogic gameLogic)
        {
            if (this.IsInDesignMode)
            {
                this.Player = new Player(120, 120, 120, 1, 6);

                this.consumableList = new ObservableCollection<ConsumableModel>() { new ConsumableModel() { Icon = "bsword", Name = "krumpli", Rarity = Rarity.Common, Stack = 5, Tooltip = "Eza aasdadwqedqweawdasdasdwqdqedasdsaadwdwqqw", Health = 10, Stamina = 10, Mana = 10 } };
                this.weaponList = new ObservableCollection<WeaponModel>() { new WeaponModel() { Icon = "bsword", Name = "krumpli", Rarity = Rarity.Common, Durability = 5, Tooltip = "Eza aasdadwqedqweawdasdasdwqdqedasdsaadwdwqqw" } };
            }
            else
            {
                this.gameLogic = gameLogic;

                this.player = this.gameLogic.GetPlayer();

                this.weaponSelected = this.gameLogic.GetWeapon();
                this.armourSelected = this.gameLogic.GetArmour();
                this.skills = this.gameLogic.GetSkillList();
                this.consumableList = this.gameLogic.GetConsumableList();
                this.weaponList = this.gameLogic.GetWeaponList();
                this.armourList = this.gameLogic.GetArmourList();
                this.level = this.gameLogic.GetLevel();

                this.WeaponChangeCmd = new RelayCommand(() => this.gameLogic.WeaponChange(this.WeaponSelected, this.WeaponList, this.Skills));
                this.WeaponDisarmCmd = new RelayCommand(() => this.gameLogic.DisarmWeapon(this.WeaponSelected, this.WeaponList, this.Skills));
                this.ArmourChangeCmd = new RelayCommand(() => this.gameLogic.ArmourChange(this.ArmourSelected, this.ArmourList));
                this.ArmourDisarmCmd = new RelayCommand(() => this.gameLogic.DisarmArmour(this.ArmourSelected, this.ArmourList));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        public MenuViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IGameBusinessLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        public Player Player { get => this.player; set => this.player = value; }

        /// <summary>
        /// Gets the observable collection of consumable items.
        /// </summary>
        public ObservableCollection<ConsumableModel> ConsumableList { get => this.consumableList; }

        /// <summary>
        /// Gets the observable collection of wepons.
        /// </summary>
        public ObservableCollection<WeaponModel> WeaponList { get => this.weaponList; private set => this.weaponList = value; }

        /// <summary>
        /// Gets the skills.
        /// </summary>
        public ObservableCollection<SkillModel> Skills { get => this.skills; private set => this.skills = value; }

        /// <summary>
        /// Gets the observable collection of Armours.
        /// </summary>
        public ObservableCollection<ArmourModel> ArmourList { get => this.armourList; private set => this.armourList = value; }

        /// <summary>
        /// Gets or sets the game logic.
        /// </summary>
        public IGameBusinessLogic GameLogic { get => this.gameLogic; set => this.gameLogic = value; }

        /// <summary>
        /// gets or sets the Armour.
        /// </summary>
        public ArmourModel ArmourSelected
        {
            get { return this.armourSelected; }
            set { this.Set(ref this.armourSelected, value); }
        }

        /// <summary>
        /// gets or sets the Waeaopen.
        /// </summary>
        public WeaponModel WeaponSelected
        {
            get { return this.weaponSelected; }
            set { this.Set(ref this.weaponSelected, value); }
        }

                /// <summary>
        /// Gets weapon change command.
        /// </summary>
        public ICommand WeaponChangeCmd { get; private set; }

        /// <summary>
        /// Gets weapon uneqip command.
        /// </summary>
        public ICommand WeaponDisarmCmd { get; private set; }

        /// <summary>
        /// Gets armour change command.
        /// </summary>
        public ICommand ArmourChangeCmd { get; private set; }

        /// <summary>
        /// Gets armour uneqip command.
        /// </summary>
        public ICommand ArmourDisarmCmd { get; private set; }

        /// <summary>
        /// Gets or sets current level.
        /// </summary>
        public int Level { get => this.level; set => this.level = value; }
    }
}
