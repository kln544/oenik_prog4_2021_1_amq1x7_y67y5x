﻿// <copyright file="CharacterCreatorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Character creator view model.
    /// </summary>
    public class CharacterCreatorViewModel : ViewModelBase
    {
        private CharacterModel character;
        private IMainMenuLogic mainMenuLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterCreatorViewModel"/> class.
        /// </summary>
        /// <param name="mainMenuLogic">Main menu logic.</param>
        public CharacterCreatorViewModel(IMainMenuLogic mainMenuLogic)
        {
            this.character = new CharacterModel();
            this.character.Name = "Modnar";
            if (!this.IsInDesignMode)
            {
                this.mainMenuLogic = mainMenuLogic ?? throw new ArgumentNullException(nameof(mainMenuLogic));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterCreatorViewModel"/> class.
        /// </summary>
        public CharacterCreatorViewModel()
            : this(IsInDesignModeStatic ? null :
                  ServiceLocator.Current.GetInstance<IMainMenuLogic>())
        {
        }

        /// <summary>
        /// Gets or sets character.
        /// </summary>
        public CharacterModel Character
        {
            get { return this.character; }
            set { this.Set(ref this.character, value); }
        }

        /// <summary>
        /// Gets or sets main menu logic.
        /// </summary>
        public IMainMenuLogic MainMenuLogic { get => this.mainMenuLogic; set => this.mainMenuLogic = value; }
    }
}
