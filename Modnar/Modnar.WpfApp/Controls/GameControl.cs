﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.GameRenderer;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Controll class.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private IGameMaster gameMaster;
        private Renderer renderer;
        private IDisplayMenuService displayMenuService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        /// <param name="gameMaster">Game master.</param>
        /// <param name="displayMenuService">Dispaly menu service.</param>
        public GameControl(IGameMaster gameMaster, IDisplayMenuService displayMenuService)
        {
            this.gameMaster = gameMaster;
            this.displayMenuService = displayMenuService;
            this.Loaded += this.Control_Loaded;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
            : this(ServiceLocator.Current.GetInstance<IGameMaster>(), ServiceLocator.Current.GetInstance<IDisplayMenuService>())
        {
        }

        /// <summary>
        /// Draws the drawing context object.
        /// </summary>
        /// <param name="drawingContext">drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext is null)
            {
                throw new ArgumentNullException(nameof(drawingContext));
            }

            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            this.renderer = new Renderer(this.gameMaster, this.ActualWidth, this.ActualHeight);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.KeyDown += this.Win_KeyDown;
                this.MouseDown += this.Win_MouseDown;
                this.gameMaster.FOVUpdated += this.OnFOVUpdated;
            }

            this.gameMaster.DungeonLogic.PlayerFOV();
            this.InvalidateVisual();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.W:
                    this.gameMaster.DungeonLogic.Player.Move(Movedirection.Forwards);
                    this.gameMaster.DungeonLogic.EndTurn();
                    break;
                case Key.S:
                    this.gameMaster.DungeonLogic.Player.Move(Movedirection.Backwards);
                    this.gameMaster.DungeonLogic.EndTurn();
                    break;
                case Key.A: this.gameMaster.DungeonLogic.Player.Move(Movedirection.Left);
                    this.gameMaster.DungeonLogic.EndTurn();
                    break;
                case Key.D: this.gameMaster.DungeonLogic.Player.Move(Movedirection.Right);
                    this.gameMaster.DungeonLogic.EndTurn();
                    break;
                case Key.Q: this.gameMaster.DungeonLogic.Player.Face(Movedirection.Left);
                    this.gameMaster.DungeonLogic.PlayerFOV();
                    break;
                case Key.E: this.gameMaster.DungeonLogic.Player.Face(Movedirection.Right);
                    this.gameMaster.DungeonLogic.PlayerFOV();
                    break;
                case Key.D0:
                    this.InvokeSkill(0);
                    break;
                case Key.D1:
                    this.InvokeSkill(1);
                    break;
                case Key.D2:
                    this.InvokeSkill(2);
                    break;
                case Key.D3:
                    this.InvokeSkill(3);
                    break;
                case Key.D4:
                    this.InvokeSkill(4);
                    break;
                case Key.D5:
                    this.InvokeSkill(5);
                    break;
                case Key.D6:
                    this.InvokeSkill(6);
                    break;
                case Key.D7:
                    this.InvokeSkill(7);
                    break;
                case Key.D8:
                    this.InvokeSkill(8);
                    break;
                case Key.D9:
                    this.InvokeSkill(9);
                    break;
                case Key.Escape: this.displayMenuService.DisplayMenu();
                    this.displayMenuService.RefreshSkillList();
                    break;
            }

            this.InvalidateVisual();
        }

        private void InvokeSkill(int id)
        {
            if (id < this.gameMaster.DungeonLogic.Skills.Available.Count)
            {
                this.gameMaster.DungeonLogic.Skills.Available[id].Skillact.Invoke();
                this.gameMaster.DungeonLogic.EndTurn();
                this.displayMenuService.RefreshSkillList();
            }
        }

        private void Win_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.InvalidateVisual();
        }

        private void OnFOVUpdated(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }
    }
}
