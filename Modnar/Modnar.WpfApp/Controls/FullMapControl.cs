﻿// <copyright file="FullMapControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.GameRenderer;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Mini Map Control class.
    /// </summary>
    public class FullMapControl : FrameworkElement
    {
        private IGameMaster gameMaster;
        private FullMapRenderer renderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="FullMapControl"/> class.
        /// </summary>
        /// <param name="gameMaster">Game master.</param>
        public FullMapControl(IGameMaster gameMaster)
        {
            this.gameMaster = gameMaster ?? throw new ArgumentNullException(nameof(gameMaster));
            this.Loaded += this.MiniMapControl_Loaded;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FullMapControl"/> class.
        /// </summary>
        public FullMapControl()
            : this(ServiceLocator.Current.GetInstance<IGameMaster>())
        {
        }

        /// <summary>
        /// Draws the drawing context object.
        /// </summary>
        /// <param name="drawingContext">drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext is null)
            {
                throw new ArgumentNullException(nameof(drawingContext));
            }

            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void MiniMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.renderer = new FullMapRenderer(this.gameMaster, this.ActualWidth, this.ActualHeight);
            Window win = Window.GetWindow(this);
            this.InvalidateVisual();
        }
    }
}
