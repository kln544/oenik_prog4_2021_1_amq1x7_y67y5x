﻿// <copyright file="MiniMapControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.GameRenderer;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Mini Map Control class.
    /// </summary>
    public class MiniMapControl : FrameworkElement
    {
        private IGameMaster gameMaster;
        private MiniMapRenderer renderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MiniMapControl"/> class.
        /// </summary>
        /// <param name="gameMaster">Game master.</param>
        public MiniMapControl(IGameMaster gameMaster)
        {
            this.gameMaster = gameMaster ?? throw new ArgumentNullException(nameof(gameMaster));
            this.Loaded += this.MiniMapControl_Loaded;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MiniMapControl"/> class.
        /// </summary>
        public MiniMapControl()
            : this(ServiceLocator.Current.GetInstance<IGameMaster>())
        {
        }

        /// <summary>
        /// Draws the drawing context object.
        /// </summary>
        /// <param name="drawingContext">drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext is null)
            {
                throw new ArgumentNullException(nameof(drawingContext));
            }

            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void MiniMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.renderer = new MiniMapRenderer(this.gameMaster, this.ActualWidth, this.ActualHeight);
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.KeyDown += this.Win_KeyDown;
                this.MouseDown += this.Win_MouseDown;
                this.gameMaster.FOVUpdated += this.OnFOVUpdated;
            }

            this.InvalidateVisual();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            this.InvalidateVisual();
        }

        private void Win_MouseDown(object sender, MouseButtonEventArgs e)
        {
            FullMapWindow win = new FullMapWindow();
            win.ShowDialog();
        }

        private void OnFOVUpdated(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }
    }
}
