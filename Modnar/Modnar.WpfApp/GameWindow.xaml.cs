﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using CommonServiceLocator;
    using Modnar.WpfApp.BL;
    using Modnar.WpfApp.VM;

    /// <summary>
    /// Interaction logic for GameWindow.xaml.
    /// </summary>
    public partial class GameWindow : Window
    {
        private IDisplayMenuService displayMenuService;
        private GameViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="displayMenuService">Display menu service.</param>
        public GameWindow(IDisplayMenuService displayMenuService)
        {
            this.displayMenuService = displayMenuService ?? throw new ArgumentNullException(nameof(displayMenuService));
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        public GameWindow()
            : this(ServiceLocator.Current.GetInstance<IDisplayMenuService>())
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vm = this.FindResource("VM") as GameViewModel;
            this.displayMenuService.SkillUsed += this.OnSkillUsed;
            this.vm.GameLogic.PlayerDied += this.OnPlayerDied;
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.displayMenuService.DisplayMenu();
            this.vm.Skills = this.vm.GameLogic.GetSkillList();
            this.skillistbox.ItemsSource = this.vm.Skills;
            this.vm.Consumables = this.vm.GameLogic.GetConsumableList();
            this.consumablelistbox.ItemsSource = this.vm.Consumables;
        }

        private void ChangeSkill_Click(object sender, RoutedEventArgs e)
        {
            this.vm.GameLogic.SkillIdx = this.skillistbox.SelectedIndex;
            this.vm.GameLogic.UseSkill();
            this.skillistbox.ItemsSource = this.vm.GameLogic.GetSkillList();
        }

        private void ChangeItem_Click(object sender, RoutedEventArgs e)
        {
            this.vm.GameLogic.ConsumableIdx = this.consumablelistbox.SelectedIndex;
            this.vm.GameLogic.UseConsumable();
            this.consumablelistbox.ItemsSource = this.vm.GameLogic.GetConsumableList();
        }

        private void Interact_Click(object sender, RoutedEventArgs e)
        {
            this.vm.GameLogic.Interact();
            this.consumablelistbox.ItemsSource = this.vm.GameLogic.GetConsumableList();
        }

        private void OnSkillUsed(object source, EventArgs arg)
        {
            this.skillistbox.ItemsSource = this.vm.GameLogic.GetSkillList();
            this.vm.Skills = this.vm.GameLogic.GetSkillList();
        }

        private void OnPlayerDied(object source, EventArgs arg)
        {
            this.Close();
        }
    }
}
