﻿// <copyright file="MainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Implementation of <see cref=" IMainMenuLogic"/> interface.
    /// </summary>
    public class MainMenuLogic : IMainMenuLogic
    {
        private IGameMaster gameMaster;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuLogic"/> class.
        /// </summary>
        /// <param name="gameMaster">Game Master.</param>
        public MainMenuLogic(IGameMaster gameMaster)
        {
            this.gameMaster = gameMaster;

            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"Resources\ambience.wav"))
            {
                using (SoundPlayer player = new SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + @"Resources\ambience.wav"))
                {
                    player.LoadAsync();
                    player.PlayLooping();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuLogic"/> class.
        /// </summary>
        public MainMenuLogic()
            : this(ServiceLocator.Current.GetInstance<IGameMaster>())
        {
        }

        /// <inheritdoc/>
        public void StartNewGame()
        {
            this.gameMaster.StartNewGame();
        }

        /// <inheritdoc/>
        public void LoadSavedGame()
        {
            this.gameMaster.LoadGame();
        }

        /// <inheritdoc/>
        public IList<ScoreModel> ViewHighScore()
        {
            IList<ScoreModel> leadeboard = new List<ScoreModel>();
            IList<Leaderboard> readscore = this.gameMaster.ReadHighScore();

            if (readscore != null)
            {
                foreach (var item in readscore)
                {
                    leadeboard.Add(ScoreToModel(item));
                }
            }

            return leadeboard;
        }

        /// <inheritdoc/>
        public void GetKnight(string name)
        {
            this.gameMaster.CreatePlayer(name, 300, 75, 170, 80, 20, 80, "knight");
        }

        /// <inheritdoc/>
        public void GetMage(string name)
        {
            this.gameMaster.CreatePlayer(name, 150, 300, 120, 20, 90, 50, "mage");
        }

        /// <inheritdoc/>
        public void GetHunter(string name)
        {
            this.gameMaster.CreatePlayer(name, 200, 180, 200, 50, 50, 120, "hunter");
        }

        private static ScoreModel ScoreToModel(Leaderboard leaderboard)
        {
            return new ScoreModel
            {
                Name = leaderboard.Name,
                Level = leaderboard.Level,
            };
        }
    }
}
