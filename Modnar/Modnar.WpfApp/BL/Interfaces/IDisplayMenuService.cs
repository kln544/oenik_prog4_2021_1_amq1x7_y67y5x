﻿// <copyright file="IDisplayMenuService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameLogic;

    /// <summary>
    /// Interface for the menu display service.
    /// </summary>
    public interface IDisplayMenuService
    {
        /// <summary>
        /// Event when a skill was used.
        /// </summary>
        public event EventHandler<EventArgs> SkillUsed;

        /// <summary>
        /// Diplays Menu.
        /// </summary>
        void DisplayMenu();

        /// <summary>
        /// Method for event handling.
        /// </summary>
        public void RefreshSkillList();
    }
}
