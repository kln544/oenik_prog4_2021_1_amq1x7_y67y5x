﻿// <copyright file="IDisplayLeadeboardService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameLogic;

    /// <summary>
    /// Interface for the menu display service.
    /// </summary>
    public interface IDisplayLeadeboardService
    {
        /// <summary>
        /// Diplays Leadeboard.
        /// </summary>
        void DisplayLeaderBoard();
    }
}
