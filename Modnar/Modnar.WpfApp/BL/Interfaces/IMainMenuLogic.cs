﻿// <copyright file="IMainMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Main Menu Methods.
    /// </summary>
    public interface IMainMenuLogic
    {
        /// <summary>
        /// Start New Game().
        /// </summary>
        public void StartNewGame();

        /// <summary>
        /// Load Saved Game.
        /// </summary>
        public void LoadSavedGame();

        /// <summary>
        /// View High Score.
        /// </summary>
        /// <returns>Leaderboard.</returns>
        public IList<ScoreModel> ViewHighScore();

        /// <summary>
        /// Get Knight stats.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        public void GetKnight(string name);

        /// <summary>
        /// Get Mage stats.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        public void GetMage(string name);

        /// <summary>
        /// Get hunter stats.
        /// </summary>
        /// <param name="name">Name of the character.</param>
        public void GetHunter(string name);
    }
}
