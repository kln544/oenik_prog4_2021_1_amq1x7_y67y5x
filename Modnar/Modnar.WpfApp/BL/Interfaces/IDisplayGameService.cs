﻿// <copyright file="IDisplayGameService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameLogic;

    /// <summary>
    /// Interface for the game display service.
    /// </summary>
    public interface IDisplayGameService
    {
        /// <summary>
        /// Diplays Game.
        /// </summary>
        void DisplayGame();
    }
}
