﻿// <copyright file="IDisplayCharacterCreatorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the chracter creator display service.
    /// </summary>
    public interface IDisplayCharacterCreatorService
    {
        /// <summary>
        /// Diplays Character Creator.
        /// </summary>
        void DisplayCharacterCreator();
    }
}
