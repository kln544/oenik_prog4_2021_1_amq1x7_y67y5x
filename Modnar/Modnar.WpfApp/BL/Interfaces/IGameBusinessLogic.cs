﻿// <copyright file="IGameBusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.GameModel;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Game logic interface.
    /// </summary>
    public interface IGameBusinessLogic
    {
        /// <summary>
        /// Player died event handler.
        /// </summary>
        public event EventHandler<EventArgs> PlayerDied;

        /// <summary>
        /// Gets or sets selected weapon index.
        /// </summary>
        public int WeaponIdx { get; set; }

        /// <summary>
        /// Gets or sets selected armour index.
        /// </summary>
        public int ArmourIdx { get; set; }

        /// <summary>
        /// Gets or sets selected skill index.
        /// </summary>
        public int SkillIdx { get; set; }

        /// <summary>
        /// Gets or sets selected consumable item index.
        /// </summary>
        public int ConsumableIdx { get; set; }

        /// <summary>
        /// Gets the player.
        /// </summary>
        /// <returns>Player.</returns>
        public Player GetPlayer();

        /// <summary>
        /// Gets current level.
        /// </summary>
        /// <returns>level.</returns>
        public int GetLevel();

        /// <summary>
        /// Get rhe skill list.
        /// </summary>
        /// <returns>Skill List.</returns>
        public ObservableCollection<SkillModel> GetSkillList();

        /// <summary>
        /// Get the consumable item lsit.
        /// </summary>
        /// <returns>Consumable list.</returns>
        public ObservableCollection<ConsumableModel> GetConsumableList();

        /// <summary>
        /// Get the armour list.
        /// </summary>
        /// <returns>ArmourList.</returns>
        public ObservableCollection<ArmourModel> GetArmourList();

        /// <summary>
        /// Get the wepon list.
        /// </summary>
        /// <returns>Weapon.</returns>
        public ObservableCollection<WeaponModel> GetWeaponList();

        /// <summary>
        /// Save game.
        /// </summary>
        public void SaveGame();

        /// <summary>
        /// Load saved game.
        /// </summary>
        public void LoadSavedGame();

        /// <summary>
        /// End turn.
        /// </summary>
        public void EndTurn();

        /// <summary>
        /// Use consumable item.
        /// </summary>
        public void UseConsumable();

        /// <summary>
        /// Interact.
        /// </summary>
        public void Interact();

        /// <summary>
        /// Change Weapon.
        /// </summary>
        /// <param name="weapon">Weapon.</param>
        /// <param name="weapons">Weapon arsenal.</param>
        /// <param name="skills">Skills.</param>
        public void WeaponChange(WeaponModel weapon, ObservableCollection<WeaponModel> weapons, ObservableCollection<SkillModel> skills);

        /// <summary>
        ///  Ueqiup weapon.
        /// </summary>
        /// <param name="weapon">Weapon.</param>
        /// <param name="weapons">Weapon arsenal.</param>
        /// <param name="skills">Skills.</param>
        public void DisarmWeapon(WeaponModel weapon, ObservableCollection<WeaponModel> weapons, ObservableCollection<SkillModel> skills);

        /// <summary>
        /// Change Armour.
        /// </summary>
        /// <param name="armour">Armour.</param>
        /// <param name="armours">Armour collection.</param>
        public void ArmourChange(ArmourModel armour, ObservableCollection<ArmourModel> armours);

        /// <summary>
        /// Uneqip Armour.
        /// </summary>
        /// <param name="armour">Armour.</param>
        /// <param name="armours">Armour collection.</param>
        public void DisarmArmour(ArmourModel armour, ObservableCollection<ArmourModel> armours);

        /// <summary>
        /// Uses selected skill.
        /// </summary>
        public void UseSkill();

        /// <summary>
        /// Get current weapon.
        /// </summary>
        /// <returns>Weapon.</returns>
        public WeaponModel GetWeapon();

        /// <summary>
        /// Get current armour.
        /// </summary>
        /// <returns>Armour.</returns>
        public ArmourModel GetArmour();
    }
}
