﻿// <copyright file="GameBusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.BL
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Media;
    using System.Windows.Media;
    using CommonServiceLocator;
    using Modnar.GameLogic;
    using Modnar.GameModel;
    using Modnar.WpfApp.Data;

    /// <summary>
    /// Implementation of <see cref="IGameBusinessLogic"/> interface.
    /// </summary>
    public class GameBusinessLogic : IGameBusinessLogic
    {
        private IGameMaster gameMaster;
        private int weaponIdx;
        private int armourIdx;
        private int skillIdx;
        private int consumableIdx;
        private MediaPlayer pplayer = new MediaPlayer();
        private MediaPlayer eplayer = new MediaPlayer();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameBusinessLogic"/> class.
        /// </summary>
        /// <param name="gameMaster">Game Master.</param>
        public GameBusinessLogic(IGameMaster gameMaster)
        {
            this.gameMaster = gameMaster;

            this.gameMaster.PlayerDied += this.OnPlayerDied;

            this.gameMaster.SoundEffect += this.OnSoundEffect;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameBusinessLogic"/> class.
        /// </summary>
        public GameBusinessLogic()
            : this(ServiceLocator.Current.GetInstance<IGameMaster>())
        {
        }

        /// <inheritdoc/>
        public event EventHandler<EventArgs> PlayerDied;

        /// <inheritdoc/>
        public int WeaponIdx { get => this.weaponIdx; set => this.weaponIdx = value; }

        /// <inheritdoc/>
        public int ArmourIdx { get => this.armourIdx; set => this.armourIdx = value; }

        /// <inheritdoc/>
        public int SkillIdx { get => this.skillIdx; set => this.skillIdx = value; }

        /// <inheritdoc/>
        public int ConsumableIdx { get => this.consumableIdx; set => this.consumableIdx = value; }

        /// <inheritdoc/>
        public Player GetPlayer()
        {
            return this.gameMaster.Player;
        }

        /// <inheritdoc/>
        public ObservableCollection<SkillModel> GetSkillList()
        {
            ObservableCollection<SkillModel> skills = new ObservableCollection<SkillModel>();
            foreach (var item in this.gameMaster.DungeonLogic.Skills.Available)
            {
                skills.Add(SkillToModel(item));
            }

            return skills;
        }

        /// <inheritdoc/>
        public ObservableCollection<ConsumableModel> GetConsumableList()
        {
            ObservableCollection<ConsumableModel> items = new ObservableCollection<ConsumableModel>();
            foreach (var item in this.gameMaster.Player.Inventory)
            {
                items.Add(ConsumableToModel(item));
            }

            return items;
        }

        /// <inheritdoc/>
        public void EndTurn()
        {
            this.gameMaster.DungeonLogic.EndTurn();
        }

        /// <inheritdoc/>
        public ObservableCollection<ArmourModel> GetArmourList()
        {
            ObservableCollection<ArmourModel> armourList = new ObservableCollection<ArmourModel>();
            foreach (var item in this.gameMaster.Player.Armoury)
            {
                armourList.Add(ArmourToModel(item));
            }

            return armourList;
        }

        /// <inheritdoc/>
        public ArmourModel GetArmour()
        {
            return ArmourToModel(this.gameMaster.Player.Armour);
        }

        /// <inheritdoc/>
        public void UseConsumable()
        {
            if (this.ConsumableIdx >= 0)
            {
                this.gameMaster.DungeonLogic.Player.UseConsumable(this.consumableIdx);
                this.gameMaster.DungeonLogic.EndTurn();
            }
        }

        /// <inheritdoc/>
        public void Interact()
        {
            this.gameMaster.DungeonLogic.Player.Interact();
            this.gameMaster.DungeonLogic.EndTurn();
        }

        /// <inheritdoc/>
        public ObservableCollection<WeaponModel> GetWeaponList()
        {
            ObservableCollection<WeaponModel> weaponList = new ObservableCollection<WeaponModel>();
            foreach (var item in this.gameMaster.Player.Weaponry)
            {
                weaponList.Add(WeaponToModel(item));
            }

            return weaponList;
        }

        /// <inheritdoc/>
        public WeaponModel GetWeapon()
        {
            return WeaponToModel(this.gameMaster.Player.Weapon);
        }

        /// <inheritdoc/>
        public void SaveGame()
        {
            this.gameMaster.SaveGame();
        }

        /// <inheritdoc/>
        public void LoadSavedGame()
        {
            this.gameMaster.LoadGame();
        }

        /// <inheritdoc/>
        public int GetLevel()
        {
            return this.gameMaster.Level;
        }

        /// <inheritdoc/>
        public void WeaponChange(WeaponModel weapon, ObservableCollection<WeaponModel> weapons, ObservableCollection<SkillModel> skills)
        {
            if (weapon is null)
            {
                throw new System.ArgumentNullException(nameof(weapon));
            }

            if (weapons is null)
            {
                throw new System.ArgumentNullException(nameof(weapons));
            }

            if (skills is null)
            {
                throw new System.ArgumentNullException(nameof(skills));
            }

            this.gameMaster.DungeonLogic.Player.ChangeWeapon(this.weaponIdx);
            weapon.CopyFrom(WeaponToModel(this.gameMaster.Player.Weapon));
            weapons.Clear();
            foreach (var otherweapon in this.gameMaster.Player.Weaponry)
            {
                weapons.Add(WeaponToModel(otherweapon));
            }

            skills.Clear();

            foreach (var gSkill in this.gameMaster.DungeonLogic.Skills.Available)
            {
                skills.Add(SkillToModel(gSkill));
            }
        }

        /// <inheritdoc/>
        public void DisarmWeapon(WeaponModel weapon, ObservableCollection<WeaponModel> weapons, ObservableCollection<SkillModel> skills)
        {
            if (weapon is null)
            {
                throw new System.ArgumentNullException(nameof(weapon));
            }

            if (weapons is null)
            {
                throw new System.ArgumentNullException(nameof(weapons));
            }

            if (skills is null)
            {
                throw new System.ArgumentNullException(nameof(skills));
            }

            this.gameMaster.DungeonLogic.Player.ChangeWeapon();
            weapon.CopyFrom(WeaponToModel(this.gameMaster.Player.Weapon));
            weapons.Clear();
            foreach (var otherweapon in this.gameMaster.Player.Weaponry)
            {
                weapons.Add(WeaponToModel(otherweapon));
            }

            skills.Clear();

            foreach (var gSkill in this.gameMaster.DungeonLogic.Skills.Available)
            {
                skills.Add(SkillToModel(gSkill));
            }
        }

        /// <inheritdoc/>
        public void ArmourChange(ArmourModel armour, ObservableCollection<ArmourModel> armours)
        {
            if (armour is null)
            {
                throw new System.ArgumentNullException(nameof(armour));
            }

            if (armours is null)
            {
                throw new System.ArgumentNullException(nameof(armours));
            }

            this.gameMaster.DungeonLogic.Player.ChangeArmour(this.armourIdx);
            armour.CopyFrom(ArmourToModel(this.gameMaster.Player.Armour));
            armours.Clear();
            foreach (var otherarmour in this.gameMaster.Player.Armoury)
            {
                armours.Add(ArmourToModel(otherarmour));
            }
        }

        /// <inheritdoc/>
        public void DisarmArmour(ArmourModel armour, ObservableCollection<ArmourModel> armours)
        {
            if (armour is null)
            {
                throw new System.ArgumentNullException(nameof(armour));
            }

            if (armours is null)
            {
                throw new System.ArgumentNullException(nameof(armours));
            }

            this.gameMaster.DungeonLogic.Player.ChangeArmour(-1);
            armour.CopyFrom(ArmourToModel(this.gameMaster.Player.Armour));
            armours.Clear();
            foreach (var otherarmour in this.gameMaster.Player.Armoury)
            {
                armours.Add(ArmourToModel(otherarmour));
            }
        }

        /// <inheritdoc/>
        public void UseSkill()
        {
            if (this.skillIdx >= 0)
            {
                this.gameMaster.DungeonLogic.Skills.Available[this.skillIdx].Skillact.Invoke();
                this.gameMaster.DungeonLogic.EndTurn();
            }
        }

        private static ArmourModel ArmourToModel(IArmour armour)
        {
            return new ArmourModel
            {
                Name = armour.Name,
                Icon = armour.Icon,
                Durability = armour.Durability,
                Maxdurability = armour.Maxdurability,
                Rarity = armour.Rarity,
                Tooltip = armour.Tooltip,
                Physical = armour.Defense.Find(a => a.Type == Damagetype.Physical)?.Value ?? 0,
                Fire = armour.Defense.Find(a => a.Type == Damagetype.Fire)?.Value ?? 0,
                Cold = armour.Defense.Find(a => a.Type == Damagetype.Cold)?.Value ?? 0,
                Lightning = armour.Defense.Find(a => a.Type == Damagetype.Lightning)?.Value ?? 0,
            };
        }

        private static ConsumableModel ConsumableToModel(IConsumable consumable)
        {
            return new ConsumableModel
            {
                Name = consumable.Name,
                Icon = consumable.Icon,
                Stack = consumable.Stack,
                Rarity = consumable.Rarity,
                Tooltip = consumable.Tooltip,
                Stamina = consumable.Stamina,
                Health = consumable.Health,
                Mana = consumable.Mana,
            };
        }

        private static WeaponModel WeaponToModel(Weapon weapon)
        {
            return new WeaponModel
            {
                Name = weapon.Name,
                Icon = weapon.Icon,
                Durability = weapon.Durability,
                Maxdurability = weapon.Maxdurability,
                Range = weapon.Range,
                Rarity = weapon.Rarity,
                Tooltip = weapon.Tooltip,
                Type = weapon.Type,
                Physical = weapon.Damage.Find(a => a.Type == Damagetype.Physical)?.Value ?? 0,
                Fire = weapon.Damage.Find(a => a.Type == Damagetype.Fire)?.Value ?? 0,
                Cold = weapon.Damage.Find(a => a.Type == Damagetype.Cold)?.Value ?? 0,
                Lightning = weapon.Damage.Find(a => a.Type == Damagetype.Lightning)?.Value ?? 0,
            };
        }

        private static SkillModel SkillToModel(ISkill skill)
        {
            return new SkillModel
            {
                Name = skill.Name,
                Icon = skill.Icon,
                Description = skill.Description,
            };
        }

        private void OnPlayerDied(object source, EventArgs args)
        {
            if (this.PlayerDied != null)
            {
                this.PlayerDied(source, args);
            }
        }

        private void OnSoundEffect(object source, SoundEffectEventArgs args)
        {
            Uri puri = null;
            Uri euri = null;

            Random r = new Random();

            if (source is Player)
            {
                Player p = (Player)source;

                switch (args.Effect)
                {
                    case "hurt":
                        int i = r.Next(3);
                        puri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\player\" + args.Effect + i + ".wav");
                        break;
                    case "dead":
                        puri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\player\" + args.Effect + ".wav");
                        break;
                    case "attack":
                        string wep = p.Weapon.Type.ToString();
                        puri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\player\" + wep + ".wav");
                        break;
                    case "consume":
                        puri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\player\" + args.Effect + ".wav");
                        break;
                    default:
                        break;
                }
            }
            else if (source is Enemy)
            {
                Enemy e = (Enemy)source;

                switch (args.Effect)
                {
                    case "hurt":
                        euri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\enemy\" + e.Sound + args.Effect + ".wav");
                        break;
                    case "dead":
                        euri = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"Resources\enemy\" + e.Sound + args.Effect + ".wav");
                        break;
                    default:
                        break;
                }
            }

            if (puri != null && puri.IsFile)
            {
                this.pplayer.Open(puri);
                this.pplayer.Volume = 0.1;
                this.pplayer.Play();
            }

            if (euri != null && euri.IsFile)
            {
                this.eplayer.Open(euri);
                this.eplayer.Volume = 0.1;
                this.eplayer.Play();
            }
        }
    }
}
