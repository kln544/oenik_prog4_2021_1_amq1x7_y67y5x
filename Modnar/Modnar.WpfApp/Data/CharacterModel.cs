﻿// <copyright file="CharacterModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Character moder class for display.
    /// </summary>
    public class CharacterModel : ObservableObject
    {
        private string name;

        private double health;
        private double mana;
        private double stamina;

        private int points;
        private int str;
        private int wis;
        private int dex;

        private int weaponr;
        private int armour;

        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        public string Name { get => this.name; set => this.name = value; }

        /// <summary>
        /// Gets or sets the maximum health of the entity.
        /// </summary>
        public double Health { get => this.health; set => this.health = value; }

        /// <summary>
        /// Gets or sets the player's maximum mana.
        /// </summary>
        public double Mana { get => this.mana; set => this.mana = value; }

        /// <summary>
        /// Gets or sets the player's maximum stamina.
        /// </summary>
        public double Stamina { get => this.stamina; set => this.stamina = value; }

        /// <summary>
        /// Gets or sets points.
        /// </summary>
        public int Points { get => this.points; set => this.points = value; }

        /// <summary>
        /// Gets or sets the player's strenght.
        /// </summary>
        public int Str { get => this.str; set => this.str = value; }

        /// <summary>
        /// Gets or sets the player's Wisdom.
        /// </summary>
        public int Wis { get => this.wis; set => this.wis = value; }

        /// <summary>
        /// Gets or sets the player's dexterity.
        /// </summary>
        public int Dex { get => this.dex; set => this.dex = value; }

        /// <summary>
        /// Gets or sets the player's equiqed weapon.
        /// </summary>
        public int Weaponr { get => this.weaponr; set => this.weaponr = value; }

        /// <summary>
        /// Gets or sets the player's equiqed weapon.
        /// </summary>
        public int Armour { get => this.armour; set => this.armour = value; }
    }
}
