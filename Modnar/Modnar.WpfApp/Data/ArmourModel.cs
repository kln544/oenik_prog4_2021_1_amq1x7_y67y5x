﻿// <copyright file="ArmourModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Modnar.GameModel;

    /// <summary>
    /// Armour model class for display.
    /// </summary>
    public class ArmourModel
        : ObservableObject
    {
        private int durability;

        private int maxdurability;

        private string name;

        private double physical;

        private double fire;

        private double cold;

        private double lightning;

        private string icon;

        private string tooltip;

        private Rarity rarity;

        /// <summary>
        /// Gets or sets the armours current durability.
        /// </summary>
        public int Durability
        {
            get { return this.durability; }
            set { this.Set(ref this.durability, value); }
        }

        /// <summary>
        /// Gets or sets the armours max durability.
        /// </summary>
        public int Maxdurability
        {
            get { return this.maxdurability; }
            set { this.Set(ref this.maxdurability, value); }
        }

        /// <summary>
        /// Gets or sets the armours name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the armours icon.
        /// </summary>
        public string Icon
        {
            get { return this.icon; }
            set { this.Set(ref this.icon, value); }
        }

        /// <summary>
        /// Gets or sets the armours description.
        /// </summary>
        public string Tooltip
        {
            get { return this.tooltip; }
            set { this.Set(ref this.tooltip, value); }
        }

        /// <summary>
        /// Gets or sets the armours rearity.
        /// </summary>
        public Rarity Rarity
        {
            get { return this.rarity; }
            set { this.Set(ref this.rarity, value); }
        }

        /// <summary>
        /// Gets or sets the armour's physical damage reduction.
        /// </summary>
        public double Physical
        {
            get { return this.physical; }
            set { this.Set(ref this.physical, value); }
        }

        /// <summary>
        /// Gets or sets the armour's fire damage reduction.
        /// </summary>
        public double Fire
        {
            get { return this.fire; }
            set { this.Set(ref this.fire, value); }
        }

        /// <summary>
        /// Gets or sets the armour's cold damage reduction.
        /// </summary>
        public double Cold
        {
            get { return this.cold; }
            set { this.Set(ref this.cold, value); }
        }

        /// <summary>
        /// Gets or sets the armour's Lightning damage reduction.
        /// </summary>
        public double Lightning
        {
            get { return this.lightning; }
            set { this.Set(ref this.lightning, value); }
        }

        /// <summary>
        /// Clone method.
        /// </summary>
        /// <param name="other">Other Armour.</param>
        public void CopyFrom(ArmourModel other)
        {
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
