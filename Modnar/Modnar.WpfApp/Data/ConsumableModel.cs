﻿// <copyright file="ConsumableModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Modnar.GameModel;

    /// <summary>
    /// Consumable item model class for display.
    /// </summary>
    public class ConsumableModel : ObservableObject
    {
        private Rarity rarity;
        private string name;
        private int stack;
        private string icon;
        private string tooltip;
        private double health;
        private double mana;
        private double stamina;

        /// <summary>
        /// Gets or sets the number of items in stack.
        /// </summary>
        public int Stack
        {
            get { return this.stack; }
            set { this.Set(ref this.stack, value); }
        }

        /// <summary>
        /// Gets or sets the items name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the items icon.
        /// </summary>
        public string Icon
        {
            get { return this.icon; }
            set { this.Set(ref this.icon, value); }
        }

        /// <summary>
        /// Gets or sets the items description.
        /// </summary>
        public string Tooltip
        {
            get { return this.tooltip; }
            set { this.Set(ref this.tooltip, value); }
        }

        /// <summary>
        /// Gets or sets the items rearity.
        /// </summary>
        public Rarity Rarity
        {
            get { return this.rarity; }
            set { this.Set(ref this.rarity, value); }
        }

        /// <summary>
        /// Gets or sets how much health the item restores.
        /// </summary>
        public double Health
        {
            get { return this.health; }
            set { this.Set(ref this.health, value); }
        }

        /// <summary>
        /// Gets or sets how much mana the item restores.
        /// </summary>
        public double Mana
        {
        get { return this.mana; }
        set { this.Set(ref this.mana, value); }
        }

        /// <summary>
        /// Gets or sets how much stamina the item restores.
        /// </summary>
        public double Stamina
        {
            get { return this.stamina; }
            set { this.Set(ref this.stamina, value); }
        }
    }
}
