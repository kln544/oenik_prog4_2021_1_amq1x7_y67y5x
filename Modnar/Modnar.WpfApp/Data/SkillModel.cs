﻿// <copyright file="SkillModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Skill model class for display.
    /// </summary>
    public class SkillModel
        : ObservableObject
    {
        private string name;

        private string icon;

        private string description;

        /// <summary>
        /// Gets or sets the skills name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the skills icon.
        /// </summary>
        public string Icon
        {
            get { return this.icon; }
            set { this.Set(ref this.icon, value); }
        }

        /// <summary>
        /// Gets or sets the skills description.
        /// </summary>
        public string Description
        {
            get { return this.description; }
            set { this.Set(ref this.description, value); }
        }
    }
}
