﻿// <copyright file="ScoreModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model of the leaderboard.
    /// </summary>
    public class ScoreModel
    {
        private int level;

        private string name;

        /// <summary>
        /// Gets or sets the highest level reached.
        /// </summary>
        public int Level { get => this.level; set => this.level = value; }

        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get => this.name; set => this.name = value; }
    }
}
