﻿// <copyright file="WeaponModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Modnar.GameModel;

    /// <summary>
    /// Weapon model class for display.
    /// </summary>
    public class WeaponModel : ObservableObject
    {
        private WeaponType type;

        private int durability;

        private int maxdurability;

        private int range;

        private string name;

        private string icon;

        private string tooltip;

        private double physical;

        private double fire;

        private double cold;

        private double lightning;

        private Rarity rarity;

        /// <summary>
        /// Gets or sets the weapons type.
        /// </summary>
        public WeaponType Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets the weapons current durability.
        /// </summary>
        public int Durability
        {
            get { return this.durability; }
            set { this.Set(ref this.durability, value); }
        }

        /// <summary>
        /// Gets or sets the weapons max durability.
        /// </summary>
        public int Maxdurability
        {
            get { return this.maxdurability; }
            set { this.Set(ref this.maxdurability, value); }
        }

        /// <summary>
        /// Gets or sets the weapons range.
        /// </summary>
        public int Range
        {
            get { return this.range; }
            set { this.Set(ref this.range, value); }
        }

        /// <summary>
        /// Gets or sets the weapons name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the weapons icon.
        /// </summary>
        public string Icon
        {
            get { return this.icon; }
            set { this.Set(ref this.icon, value); }
        }

        /// <summary>
        /// Gets or sets the weapons description.
        /// </summary>
        public string Tooltip
        {
            get { return this.tooltip; }
            set { this.Set(ref this.tooltip, value); }
        }

        /// <summary>
        /// Gets or sets the weapons reaeity.
        /// </summary>
        public Rarity Rarity
        {
            get { return this.rarity; }
            set { this.Set(ref this.rarity, value); }
        }

        /// <summary>
        /// Gets or sets weapon's physical damage.
        /// </summary>
        public double Physical
        {
            get { return this.physical; }
            set { this.Set(ref this.physical, value); }
        }

        /// <summary>
        /// Gets or sets weapon's fire damage.
        /// </summary>
        public double Fire
        {
            get { return this.fire; }
            set { this.Set(ref this.fire, value); }
        }

        /// <summary>
        /// Gets or sets weapon's cold damage.
        /// </summary>
        public double Cold
        {
            get { return this.cold; }
            set { this.Set(ref this.cold, value); }
        }

        /// <summary>
        /// Gets or sets weapon's lightning damage.
        /// </summary>
        public double Lightning
        {
            get { return this.lightning; }
            set { this.Set(ref this.lightning, value); }
        }

        /// <summary>
        /// Clone method.
        /// </summary>
        /// <param name="other">Other weapon.</param>
        public void CopyFrom(WeaponModel other)
        {
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
