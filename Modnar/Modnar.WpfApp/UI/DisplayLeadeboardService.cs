﻿// <copyright file="DisplayLeadeboardService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Implementation of <see cref="IDisplayLeadeboardService"/> interface.
    /// </summary>
    public class DisplayLeadeboardService : IDisplayLeadeboardService
    {
        /// <inheritdoc/>
        public void DisplayLeaderBoard()
        {
            HighScoreWindow win = new HighScoreWindow();
            win.ShowDialog();
        }
    }
}
