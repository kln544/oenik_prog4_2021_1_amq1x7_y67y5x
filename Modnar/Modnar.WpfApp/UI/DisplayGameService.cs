﻿// <copyright file="DisplayGameService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using Modnar.GameLogic;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Implementation of <see cref="IDisplayGameService"/> interface.
    /// </summary>
    public class DisplayGameService : IDisplayGameService
    {
        /// <inheritdoc/>
        public void DisplayGame()
        {
            GameWindow win = new GameWindow();
            win.ShowDialog();
        }
    }
}
