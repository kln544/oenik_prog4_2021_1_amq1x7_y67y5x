﻿// <copyright file="DisplayMenuService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.WpfApp.BL;
    using static Modnar.WpfApp.BL.IDisplayMenuService;

    /// <summary>
    /// Implementation of <see cref="IDisplayMenuService"/> interface.
    /// </summary>
    public class DisplayMenuService : IDisplayMenuService
    {
        /// <inheritdoc/>
        public event EventHandler<EventArgs> SkillUsed;

        /// <inheritdoc/>
        public void DisplayMenu()
        {
            MenuWindow win = new MenuWindow();
            win.ShowDialog();
        }

        /// <inheritdoc/>
        public void RefreshSkillList()
        {
            this.OnRefresSkillList();
        }

        /// <summary>
        /// Method to throw an event on a skill being used.
        /// </summary>
        protected virtual void OnRefresSkillList()
        {
            if (this.SkillUsed != null)
            {
                this.SkillUsed(this, EventArgs.Empty);
            }
        }
    }
}
