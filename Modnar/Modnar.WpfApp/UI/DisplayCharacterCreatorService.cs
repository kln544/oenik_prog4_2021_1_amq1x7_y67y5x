﻿// <copyright file="DisplayCharacterCreatorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Modnar.WpfApp.BL;

    /// <summary>
    /// Implementation of <see cref="IDisplayCharacterCreatorService"/> interface.
    /// </summary>
    public class DisplayCharacterCreatorService : IDisplayCharacterCreatorService
    {
        /// <inheritdoc/>
        public void DisplayCharacterCreator()
        {
            CharacterCreatorWindow win = new CharacterCreatorWindow();
            win.ShowDialog();
        }
    }
}
