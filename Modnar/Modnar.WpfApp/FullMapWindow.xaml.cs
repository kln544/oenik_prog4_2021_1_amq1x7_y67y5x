﻿// <copyright file="FullMapWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Modnar.WpfApp.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Modnar.WpfApp.VM;

    /// <summary>
    /// Interaction logic for FullMapWindow.xaml.
    /// </summary>
    public partial class FullMapWindow : Window
    {
        private MapViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="FullMapWindow"/> class.
        /// </summary>
        public FullMapWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.vm = this.FindResource("VM") as MapViewModel;
        }
    }
}
